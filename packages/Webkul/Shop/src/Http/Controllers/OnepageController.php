<?php

namespace Webkul\Shop\Http\Controllers;

// Requiring autoloader
$path = $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once "$path";

use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use DB;
use Session;
use Input;
use Webkul\Checkout\Facades\Cart;
use Webkul\Shipping\Facades\Shipping;
use Webkul\Payment\Facades\Payment;
use Webkul\Checkout\Http\Requests\CustomerAddressForm;
use Webkul\Sales\Repositories\OrderRepository;
use eGHL\MerchantAPI\core\APIFactory;

/**
 * Chekout controller for the customer and guest for placing order
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class OnepageController extends Controller {

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $orderRepository;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Attribute\Repositories\OrderRepository  $orderRepository
     * @return void
     */
    public function __construct(OrderRepository $orderRepository) {
        $this->orderRepository = $orderRepository;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if (!isset(auth()->guard('customer')->user()->id) && session('cart_data')) {
            $cart_details = session('cart_data');
            $prd_id = array();
            foreach ($cart_details as $get_cart):
                $prd_id[] = $get_cart['product_id'];
            endforeach;
            $cart_data = DB::table("products_grid")
                    ->join('product_inventories', 'product_inventories.product_id', '=', 'products_grid.product_id')
                    ->wherein('products_grid.product_id', $prd_id)
                    ->where('product_inventories.vendor_id', "!=", 0)
                    ->get();
            $currencies = DB::table('currencies')->where('id', 1)->first();
            $currency = $currencies->code;
            $customer_data = $customer_ship_data = array();
            return view($this->_config['view'], compact('cart_data', 'currency', 'customer_data', 'customer_ship_data'));
        } else if (isset(auth()->guard('customer')->user()->id)) {
            $customer_id = auth()->guard('customer')->user()->id;
            $cart_data = DB::table('cart_detail')
                            ->join('products_grid', 'products_grid.product_id', '=', 'cart_detail.prd_id')
                            ->where('customer_id', $customer_id)
                            ->get()->toArray();

            $count_cart_data = count($cart_data);
            if ($count_cart_data != 0) {
                $customer_data = DB::table('order_address')->where('customer_id', $customer_id)->where('address_type', "billing")->first();
                $customer_ship_data = DB::table('order_address')->where('customer_id', $customer_id)->where('address_type', "shipping")->first();

                $count_orde = count($customer_data);
                if ($count_orde == 0) {
                    $customer_data = DB::table('customers')->where('id', $customer_id)->get()->first();
                }
                $currencies = DB::table('currencies')->where('id', 1)->first();
                $currency = $currencies->code;
                return view($this->_config['view'], compact('cart_data', 'currency', 'customer_data', 'customer_ship_data'));
            } else {
                return redirect()->route('shop.checkout.cart.index');
            }
        } else {
            return redirect()->route('shop.checkout.cart.index');
        }
    }

    /**
     * Saves customer address.
     *
     * @param  \Webkul\Checkout\Http\Requests\CustomerAddressForm $request
     * @return \Illuminate\Http\Response
     */
    public function saveAddress(CustomerAddressForm $request) {
        $data = request()->all();

// dd($data);

        $data['billing']['address1'] = implode(PHP_EOL, array_filter($data['billing']['address1']));
        $data['shipping']['address1'] = implode(PHP_EOL, array_filter($data['shipping']['address1']));

        if (Cart::hasError() || !Cart::saveCustomerAddress($data) || !$rates = Shipping::collectRates())
            return response()->json(['redirect_url' => route('shop.checkout.cart.index')], 403);

        Cart::collectTotals();

        return response()->json($rates);
    }

    /**
     * Saves shipping method.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveShipping() {
        $shippingMethod = request()->get('shipping_method');

        if (Cart::hasError() || !$shippingMethod || !Cart::saveShippingMethod($shippingMethod))
            return response()->json(['redirect_url' => route('shop.checkout.cart.index')], 403);

        Cart::collectTotals();

        return response()->json(Payment::getSupportedPaymentMethods());
    }

    public function place_order(Request $request) {        
        $customer_id = auth()->guard('customer')->user()->id;
        //Address Fields
        $address_firstname = $request->input('address_firstname');
        $address_lastname = $request->input('address_lastname');
        $address_email = $request->input('address_email');
        $address_street = $request->input('address_street');
        $address_city = $request->input('address_city');
        $address_state = $request->input('address_state');
        $addresszip = $request->input('addresszip');
        $address_country = $request->input('address_country');
        $address_phone = $request->input('address_phone');
        

        if ($request->input('ship_type')) {
            $ship_firstname = $request->input('ship_firstname');
            $ship_lastname = $request->input('ship_lastname');
            $ship_email = $request->input('ship_email');
            $ship_adddress = $request->input('ship_adddress');
            $ship_city = $request->input('ship_city');
            $ship_state = $request->input('ship_state');
            $ship_zip = $request->input('ship_zip');
            $ship_country = $request->input('ship_country');
            $ship_phone = $request->input('ship_phone');
        }

        $item_name = $request->input('item_name');
        $item_qty = $request->input('item_qty');
        $item_price = $request->input('item_price');
        $item_currency = $request->input('item_currency');
        $item_type = $request->input('item_type');
        $item_shipchargename = $request->input('item_shipchargename');


        $item_shipchargevalue = $request->input('item_shipchargevalue');
        $item_payment = $request->input('payment_method');

        $marketplace_seller_id = $request->input('item_sellerid');
        $item_prdid = $request->input('item_prdid');
        $item_sku = $request->input('item_sku');
        $now = date('Y-m-d H:i:s', time());
        $nr = 0;
        
        $unique_seller_id= array_unique($marketplace_seller_id);
        

        $total_item_shipchargevalue = 0;
        foreach ($item_shipchargevalue as $all_shiping) {
            $total_item_shipchargevalue += $all_shiping;
        }

        $total_pricevalue = 0;
        foreach ($item_price as $all_price) {
            $total_pricevalue += $all_price;
        }

        $item_count = count($item_price);

        $total_qty = 0;
        foreach ($item_qty as $all_qty) {
            $total_qty += $all_qty;
        }

        $total_pricevalue = sprintf('%0.2f', $total_pricevalue);
        $total_item_shipchargevalue = sprintf('%0.2f', $total_item_shipchargevalue);
        $sub_total_price = $total_pricevalue - $total_item_shipchargevalue;
        $sub_total_price = sprintf("%0.2f", $sub_total_price);


        $orders_table = array(
            'increment_id' => 1,
            'status' => "pending",
            'channel_name' => 'Default',
            'is_guest' => 0,
            'customer_email' => $address_email,
            'customer_first_name' => $address_firstname,
            'customer_last_name' => $address_lastname,
            //'shipping_method' => $main_slug,
            //'shipping_title' => $item_shipchargename[$nr],
            'shipping_description' => $item_shipchargename[$nr],
            'is_gift' => 0,
            'total_item_count' => $item_count,
            'total_qty_ordered' => $total_qty,
            'base_currency_code' => $item_currency[0],
            'channel_currency_code' => $item_currency[0],
            'order_currency_code' => $item_currency[0],
            'grand_total' => $total_pricevalue,
            'base_grand_total' => $total_pricevalue,
            'sub_total' => $sub_total_price,
            'base_sub_total' => $sub_total_price,
            'shipping_amount' => $total_item_shipchargevalue,
            'base_shipping_amount' => $total_item_shipchargevalue,
            'customer_id' => $customer_id,
            'customer_type' => "Webkul\Customer\Models\Customer",
            'channel_id' => 1,
            'channel_type' => 'Webkul\Core\Models\Channel',
            'created_at' => $now,
            'updated_at' => $now
        );


        $table_inserted = DB::table('orders')->insert([$orders_table]);
        $order_last_id = DB::getPdo()->lastInsertId();

        if ($order_last_id != "") {
            $market_place_order = array(
                'status' => "pending",
                'grand_total' => $total_pricevalue,
                'base_grand_total' => $total_pricevalue,
                'sub_total' => $sub_total_price,
                'base_sub_total' => $sub_total_price,
                'shipping_amount' => $total_item_shipchargevalue,
                'base_shipping_amount' => $total_item_shipchargevalue,
                'marketplace_seller_id' => $marketplace_seller_id[$nr],
                'order_id' => $order_last_id,
                'created_at' => $now,
                'updated_at' => $now,
            );

            DB::table('marketplace_orders')->insert([$market_place_order]);


            //Save Address and Shiping Address
            $order_address = array(
                'first_name' => $address_firstname,
                'last_name' => $address_lastname,
                'email' => $address_email,
                'address1' => $address_street,
                'country' => $address_country,
                'state' => $address_state,
                'city' => $address_city,
                'postcode' => $addresszip,
                'phone' => $address_phone,
                'address_type' => 'billing',
                'order_id' => $order_last_id,
                'customer_id' => $customer_id,
                'created_at' => $now,
                'updated_at' => $now
            );
            DB::table('order_address')->insert([$order_address]);

            if ($request->input('ship_type')) {
                $ship_order_address = array(
                    'first_name' => $ship_firstname,
                    'last_name' => $ship_lastname,
                    'email' => $ship_email,
                    'address1' => $ship_adddress,
                    'country' => $ship_country,
                    'state' => $ship_state,
                    'city' => $ship_city,
                    'postcode' => $ship_zip,
                    'phone' => $ship_phone,
                    'address_type' => 'shipping',
                    'order_id' => $order_last_id,
                    'customer_id' => $customer_id,
                    'created_at' => $now,
                    'updated_at' => $now
                );
                DB::table('order_address')->insert([$ship_order_address]);
            }

            //Save Order Method
            $order_payment = array(
                'method' => $item_payment,
                'order_id' => $order_last_id
            );
            DB::table('order_payment')->insert([$order_payment]);


            foreach ($item_name as $total_items) {
                if ($marketplace_seller_id[$nr] == "") {
                    $marketplace_seller_id[$nr] = 1;
                }

                if (isset($item_shipchargename[$nr])) {
                    $item_ship_string = str_replace(' ', '-', $item_shipchargename[$nr]);
                    $new_upper_slug = preg_replace('/[^A-Za-z0-9\-]/', '', $item_ship_string);
                    $main_slug = strtolower($new_upper_slug);
                } else {
                    $main_slug = "";
                    $item_shipchargename = "";
                }
                $order_items = array(
                    'sku' => $item_sku[$nr],
                    'type' => $item_type[$nr],
                    'name' => $item_name[$nr],
                    'qty_ordered' => $item_qty[$nr],
                    'price' => $item_price[$nr],
                    'base_price' => $item_price[$nr],
                    'total' => $item_price[$nr],
                    'base_total' => $item_price[$nr],
                    'product_id' => $item_prdid[$nr],
                    'product_type' => "Webkul\Product\Models\Product",
                    'order_id' => $order_last_id,
                    'created_at' => $now,
                    'updated_at' => $now
                );
                DB::table('order_items')->insert([$order_items]);
                $nr++;
            }

            $orderid = (object)
                    array(
                        'id' => $order_last_id
            );

            $delete_cart_items = DB::table('cart_detail')->where('customer_id', $customer_id)->delete();

            $request->session()->put('order', $orderid);
            session()->flash('success', "Your Order has been placed successfully.");
            return redirect()->route('shop.checkout.success');
        } else {
            session()->flash('success', "Please try again latter");
            return redirect()->route('/checkout/onepage');
        }
    }

    /**
     * Saves payment method.
     *
     * @return \Illuminate\Http\Response
     */
    public function savePayment() {
        $payment = request()->get('payment');

        if (Cart::hasError() || !$payment || !Cart::savePaymentMethod($payment))
            return response()->json(['redirect_url' => route('shop.checkout.cart.index')], 403);

        $cart = Cart::getCart();

        return response()->json([
                    'jump_to_section' => 'review',
                    'html' => view('shop::checkout.onepage.review', compact('cart'))->render()
        ]);
    }

    /**
     * Saves order.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveOrder() {
        if (Cart::hasError())
            return response()->json(['redirect_url' => route('shop.checkout.cart.index')], 403);

        Cart::collectTotals();

        $this->validateOrder();

        $cart = Cart::getCart();

        if ($redirectUrl = Payment::getRedirectUrl($cart)) {
            return response()->json([
                        'success' => true,
                        'redirect_url' => $redirectUrl
            ]);
        }

        $order = $this->orderRepository->create(Cart::prepareDataForOrder());

        Cart::deActivateCart();

        session()->flash('order', $order);

        return response()->json([
                    'success' => true,
        ]);
    }

    /**
     * Order success page
     *
     * @return \Illuminate\Http\Response
     */
    public function success() {
        if (!$order = session('order'))
            return redirect()->route('shop.checkout.cart.index');

        return view($this->_config['view'], compact('order'));
    }

    public function eghlpayment() {
        $ServiceID = 'VRD';
        $merchantPass = 'Sw3sqfwQ';

        $params = array(
            'PymtMethod' => 'DD',
            'ServiceID' => $ServiceID,
            'PaymentID' => '8308110000',
            'Amount' => '123.10',
            'CurrencyCode' => 'RM'
        );

        $API = APIFactory::create('Query', $params, $merchantPass, true);
        $response = $API->sendRequest()->getResponse();
        echo "<pre>" . print_r($response, 1) . "</pre>";
    }

    /**
     * Validate order before creation
     *
     * @return mixed
     */
    public function validateOrder() {
        $cart = Cart::getCart();

        if (!$cart->shipping_address) {
            throw new \Exception(trans('Please check shipping address.'));
        }

        if (!$cart->billing_address) {
            throw new \Exception(trans('Please check billing address.'));
        }

        if (!$cart->selected_shipping_rate) {
            throw new \Exception(trans('Please specify shipping method.'));
        }

        if (!$cart->payment) {
            throw new \Exception(trans('Please specify payment method.'));
        }
    }

}
