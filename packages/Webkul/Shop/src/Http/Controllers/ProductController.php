<?php

namespace Webkul\Shop\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Webkul\Product\Repositories\ProductRepository as Product;

/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductController extends Controller {

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $product;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Product\Repositories\ProductRepository $product
     * @return void
     */
    public function __construct(Product $product) {
        $this->product = $product;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slug) {
        $currency = DB::table('currencies')->where('id', 1)->get()->toArray();
        $product_flat = DB::table('product_flat')->where('url_key', $slug)->get()->toArray();
        

        if ($currency[0]->code != "") {
            $currency = $currency[0]->code;
        } else {
            $currency = "$";
        }
        
        $product = $this->product->findBySlugOrFail($slug);
        
        $customer = auth()->guard('customer')->user();
        
        $product_review = DB::table('product_reviews')->where('product_id', $product['id'])->orderby('id', 'desc')->get()->toArray();
        $rating_average = DB::table('product_reviews')
                ->select(DB::raw('count(rating) as count_rating'), 'rating')
                ->where('product_id', $product['id'])
                ->groupby('rating')
                ->orderby('rating', 'desc')
                ->get()
                ->toArray();

        $cross_sell = DB::table('product_cross_sells')->where('parent_id', $product['id'])->get()->toArray();

        $attr_selected = $all_attr_option = "";

        $shipping_available = DB::table('shipping_price')->where('product_id', $product['id'])->get()->toArray();

        $productid = $product['id'];
        $seller_info = DB::table('customers')
                ->where('customers.id', function($query) use ($productid) {
                    $query->select('customer_id')
                    ->from('marketplace_sellers')
                    ->where('id', function($subquery) use ($productid) {
                        $subquery->select('marketplace_seller_id')
                        ->from('marketplace_products')
                        ->where('product_id', $productid);
                    });
                })
                ->join('marketplace_sellers', 'customers.id', '=', 'marketplace_sellers.customer_id')
                ->first();
                
                
        
                
        

        $whislist_product = array();

        if (isset(auth()->guard('customer')->user()->id)) {
            $customer_details = auth()->guard('customer')->user();
            $whislist_product = DB::table('wishlist')->where('product_id', $productid)->where('customer_id', auth()->guard('customer')->user()->id)->first();
        } else {
            $customer_details = array();
            $whislist_product = array();
        }

        
        if ($product['type'] == "configurable") {
            $attr_selected = DB::table('product_attr_data')->where('product_id', $product['id'])->get()->toarray();
            $prd_attr_selected = $attr_selected[0]->selected_attr_val;
            $explode_attr = explode(",", $prd_attr_selected);
            $all_attr_option = DB::table('attribute_options')->whereIn('attribute_id', $explode_attr)->get()->toarray();

            $prod_attr = array();
            foreach($attr_selected as $atr){
                $attr = explode(', ',$atr->product_attr_data_detail);
                foreach ($attr as $key) {
                    array_push($prod_attr,$key);
                }
            }
            $prod_attr = array_unique($prod_attr);

            $all_attr_option = DB::table('attribute_options')->whereIn('id', $prod_attr)->get()->toarray();

            return view($this->_config['view'], compact('product', 'customer', 'currency', 'product_flat', 'product_review', 'rating_average', 'cross_sell', 'attr_selected', 'all_attr_option', 'shipping_available', 'seller_info', 'whislist_product', 'customer_details'));
        } else {
            return view($this->_config['view'], compact('product', 'customer', 'currency', 'product_flat', 'product_review', 'rating_average', 'cross_sell', 'attr_selected', 'all_attr_option', 'shipping_available', 'seller_info', 'whislist_product', 'customer_details'));
        }
    }

    function variationprice() {
        $users_selected = $_POST['users_selected'];
        $cnfprd_id=$_POST['cnfprd_id'];
        $implode_array = implode(", ", $users_selected);
        $check_price = DB::table('product_attr_data')
                ->select('product_attr_data_price', 'product_attr_data_stock')
                    ->where('product_attr_data_detail', $implode_array)
                ->where('product_id', $cnfprd_id)
                ->get()->toarray();
        $count = count($check_price);
        if ($count != 0) {
            if ($check_price[0]->product_attr_data_stock > 0) {
                $price_data = $check_price[0]->product_attr_data_price;
            } else {
                $price_data = "stock_problem";
            }
        } else {
            $price_data = "error";
        }
        echo $price_data;
    }
    
    
    

    function productwishlist() {
        $prd_id = $_POST['prd_id'];
        if (isset(auth()->guard('customer')->user()->id)) {
            $customer_details = auth()->guard('customer')->user();
            $prd_wishlist = DB::table('wishlist')->where('product_id', $prd_id)->where('customer_id', auth()->guard('customer')->user()->id)->first();
            $count_prd_wishlist = count($prd_wishlist);
            if ($count_prd_wishlist == 1) {
                $mywishlist = DB::table('wishlist')->where('product_id', $prd_id)->where('customer_id', auth()->guard('customer')->user()->id)->delete();
                $result="removed";
            } else {
                $add_wishlist = array(
                    'product_id' => $prd_id,
                    'channel_id' => 1,
                    'customer_id' => auth()->guard('customer')->user()->id
                );
                $mywishlist = DB::table('wishlist')->insert([$add_wishlist]);
                $result="added";
            }
            echo $result; die();
        }
    }

}
