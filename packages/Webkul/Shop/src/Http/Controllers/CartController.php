<?php

namespace Webkul\Shop\Http\Controllers;

use DB;
use Session;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Checkout\Repositories\CartRepository;
use Webkul\Checkout\Repositories\CartItemRepository;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Product\Helpers\ProductImage;
use Webkul\Product\Helpers\View as ProductView;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Cart;

/**
 * Cart controller for the customer and guest users for adding and
 * removing the products in the cart.
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CartController extends Controller {

    /**
     * Protected Variables that holds instances of the repository classes.
     *
     * @param Array $_config
     * @param $cart
     * @param $cartItem
     * @param $customer
     * @param $product
     * @param $productImage
     * @param $productView
     */
    protected $_config;
    protected $cart;
    protected $cartItem;
    protected $customer;
    protected $product;
    protected $productView;

    public function __construct(
            CustomerRepository $customer,
            ProductRepository $product,
            ProductImage $productImage,
            ProductView $productView
    ) {

        $this->middleware('customer')->only(['moveToWishlist']);

        $this->customer = $customer;

        $this->product = $product;

        $this->productImage = $productImage;

        $this->productView = $productView;

        $this->_config = request('_config');
    }

    /**
     * Method to populate the cart page which will be populated before the checkout process.
     *
     * @return Mixed
     */
    public function index123() {
        return view($this->_config['view'])->with('cart', Cart::getCart());
    }

    public function index(Request $request) {
        if (isset(auth()->guard('customer')->user()->id)) {
            $cart_data = DB::table('cart_detail')
                    ->select(db::raw('DISTINCT `product_inventories`.*'), 'cart_detail.*', 'products_grid.*')
                    ->join('products_grid', 'products_grid.product_id', '=', 'cart_detail.prd_id')
                    ->join('product_inventories', 'product_inventories.product_id', '=', 'cart_detail.prd_id')
                    ->where('product_inventories.vendor_id', "!=", 0);

            if (isset(auth()->guard('customer')->user()->id)) {
                $customer_id = auth()->guard('customer')->user()->id;
                $cart_data->where('cart_detail.customer_id', $customer_id);
            }
            $cart_result = $cart_data->get();
        } else if (session('cart_data')) {
            $cart_details = session('cart_data');
//            Session::forget('cart_data');
//            Session::flush('cart_data');
//            Session::regenerate('cart_data');
//            echo "<pre>";
//            print_r(session('cart_data'));
//            die();
            $prd_id = array();
            foreach ($cart_details as $get_cart):
                $prd_id[] = $get_cart['product_id'];
            endforeach;

            $implodeids = implode(",", $prd_id);
            $cart_result = DB::table("products_grid")
                    ->join('product_inventories', 'product_inventories.product_id', '=', 'products_grid.product_id')
                    ->wherein('products_grid.product_id', $prd_id)
                    ->where('product_inventories.vendor_id', "!=", 0)
                    //->orderByRaw("FIELD(products_grid.product_id, " . $implodeids)
                    ->get();

            $cart_result = DB::select("select * from `products_grid` inner join `product_inventories` on `product_inventories`.`product_id` = `products_grid`.`product_id` where `products_grid`.`product_id` in ($implodeids) and `product_inventories`.`vendor_id` != 0 ORDER BY FIELD(`products_grid`.`product_id`, $implodeids)");
        } else {
            $cart_result = array();
        }


        //Get currencies
        $currencies = DB::table('currencies')->where('id', 1)->first();
        $currency = $currencies->code;
        return view($this->_config['view'], compact('cart_result', 'currency', 'duplicate_qty'));
    }

    /**
     * Function for guests user to add the product in the cart.
     *
     * @return Mixed
     */
    public function add($id, Request $request) {
        $customer_variationlabel = $customer_id = $customer_variationvalue = $customer_fname = $customer_lname = $customer_email = "";
        $customer_ip = $_SERVER['REMOTE_ADDR'] ?: ($_SERVER['HTTP_X_FORWARDED_FOR'] ?: $_SERVER['HTTP_CLIENT_IP']);
        $is_configurable = $_POST['is_configurable'];
        $product_id = $_POST['product'];
        $quantity = $_POST['quantity'];
        $product_name = $_POST['product_name'];
        $prd_price = $_POST['selected_prdprice'];
        $shipping_charge = $_POST['shipping_charge'];
        if ($shipping_charge == "") {
            $shipping_charge = 0;
        }
        $shipping_charge_id = $_POST['shipping_charge_id'];
        $shipping_charged_name = $_POST['shipping_charged_name'];
        $last_prdprice = $_POST['last_prdprice'];
        $store_currency = $_POST['store_currency'];
        $store_sellerid = $_POST['store_sellerid'];
        $prd_sku = $_POST['prd_sku'];
        $now = date('Y-m-d H:i:s', time());
        $allow_insert = 0;
        $update_status = 0;

        if (isset(auth()->guard('customer')->user()->id)) {
            $customer_id = auth()->guard('customer')->user()->id;
            $customer_fname = auth()->guard('customer')->user()->first_name;
            $customer_lname = auth()->guard('customer')->user()->last_name;
            $customer_email = auth()->guard('customer')->user()->email;

            if ($is_configurable == "configurable") {

                $customer_variationlabel = $_POST['select_textlabel'];
                $customer_variationvalue = $_POST['select_valuelabel'];

                $query_var = DB::table('cart_detail')->where('prd_type', $is_configurable)->where('variation_value', $customer_variationvalue)->where('prd_id', $product_id)->where('customer_address', $customer_ip);
                if (isset(auth()->guard('customer')->user()->id)) {
                    $query_var->where('customer_id', $customer_id);
                }
                $result_variable = $query_var->first();

                $count_variable = count($result_variable);

                if ($count_variable == 1) {
                    //Update Variation Product

                    $var_itemqty = $result_variable->items_qty;
                    $var_price = $result_variable->grand_total;
                    $var_sub_total = $result_variable->sub_total;
                    $var_prd_shipping_charge = $result_variable->prd_shipping_charge;

                    $var_itemqty = $var_itemqty + 1;
                    $var_price = ($var_sub_total * $var_itemqty) + $var_prd_shipping_charge;
                    $var_subt = $var_sub_total * $var_itemqty;
                    $update_var_price = number_format($var_price, 2);
                    $update_varsubt = number_format($var_subt, 2);
                    $update_query_variable = DB::table('cart_detail')->where('prd_type', $is_configurable)->where('variation_value', $customer_variationvalue)->where('prd_id', $product_id)->where('customer_address', $customer_ip);
                    if (isset(auth()->guard('customer')->user()->id)) {
                        $update_query_variable->where('customer_id', $customer_id);
                    }
                    $update_query_variable->update(['items_qty' => $var_itemqty, 'grand_total' => $update_var_price]);
                    $update_status = 1;
                    $allow_insert = 0;
                } else {
                    $allow_insert = 1;
                }
            } else {
                //Update Simple Product
                $query_sim = DB::table('cart_detail')->where('prd_type', $is_configurable)->where('prd_id', $product_id)->where('customer_address', $customer_ip);
                if (isset(auth()->guard('customer')->user()->id)) {
                    $query_sim->where('customer_id', $customer_id);
                }
                $result_simple = $query_sim->first();
                $count_simple = count($result_simple);
                if ($count_simple == 1) {
                    $simple_itemqty = $result_simple->items_qty;
                    $simple_price = $result_simple->grand_total;
                    $sim_sub_total = $result_simple->sub_total;
                    $simple_prd_shipping_charge = $result_simple->prd_shipping_charge;
                    $simple_itemqty = $simple_itemqty + 1;
                    $sim_price = ($sim_sub_total * $simple_itemqty) + $simple_prd_shipping_charge;
                    $sim_subt = $sim_sub_total * $simple_itemqty;

                    $update_simple_price = number_format($sim_price, 2);
                    $update_subt = number_format($sim_subt, 2);

                    $update_query = DB::table('cart_detail')->where('prd_id', $product_id)->where('customer_address', $customer_ip)->where('prd_type', $is_configurable);
                    if (isset(auth()->guard('customer')->user()->id)) {
                        $update_query->where('customer_id', $customer_id);
                    }
                    $update_query->update(['items_qty' => $simple_itemqty, 'grand_total' => $update_simple_price]);
                    $allow_insert = 0;
                    $update_status = 1;
                } else {
                    $allow_insert = 1;
                }
            }


            if (isset($product_id) && $prd_price != "" && $allow_insert == 1) {

                $save_cart = array(
                    'prd_name' => $product_name,
                    'customer_email' => $customer_email,
                    'customer_first_name' => $customer_fname,
                    'customer_last_name' => $customer_lname,
                    'items_qty' => $quantity,
                    'global_currency_code' => $store_currency,
                    'channel_currency_code' => $store_currency,
                    'cart_currency_code' => $store_currency,
                    'customer_id' => $customer_id,
                    'channel_id' => 1,
                    'prd_id' => $product_id,
                    'prd_type' => $is_configurable,
                    'variation_label' => $customer_variationlabel,
                    'variation_value' => $customer_variationvalue,
                    'prd_shipping_id' => $shipping_charge_id,
                    'prd_shipping_name' => $shipping_charged_name,
                    'customer_address' => $customer_ip,
                    'prd_shipping_charge' => $shipping_charge,
                    'sub_total' => $prd_price,
                    'seller_id' => $store_sellerid,
                    'prd_sku' => $prd_sku,
                    'grand_total' => $last_prdprice,
                    'created_at' => $now,
                    'updated_at' => $now
                );
                $insert_cart = DB::table('cart_detail')->insert([$save_cart]);
                if ($insert_cart == 1) {
                    $request->session()->put('cart_details', $customer_ip);
                    session()->flash('success', "Your Item added in cart successfully");
                    return redirect()->route($this->_config['redirect']);
                } else {
                    session()->flash('error', "Failed to add item in cart");
                    return redirect()->back();
                }
            } else if ($update_status == 1) {
                return redirect()->route($this->_config['redirect']);
            } else {
                session()->flash('error', "Please try after some time");
                return redirect()->back();
            }
        } else {
            if ($shipping_charge != "") {
                $shipping_charge = 0;
            }
            if ($is_configurable == "configurable") {
                $customer_variationlabel = $_POST['select_textlabel'];
                $customer_variationvalue = $_POST['select_valuelabel'];
                $prd_array = array(
                    array(
                        'prd_name' => $product_name,
                        'product_type' => $is_configurable,
                        'customer_ip' => $customer_ip,
                        'customer_variationlabel' => $customer_variationlabel,
                        'customer_variationvalue' => $customer_variationvalue,
                        'product_id' => $product_id,
                        'quantity' => $quantity,
                        'shipping_charge' => number_format($shipping_charge, 2),
                        'shipping_charge_id' => $shipping_charge_id,
                        'shipping_charged_name' => $shipping_charged_name,
                        'sub_total' => number_format($prd_price, 2),
                        'grand_total' => number_format($last_prdprice, 2),
                        'store_currency' => $store_currency,
                        'prd_sku' => $prd_sku,
                        'seller_id' => $store_sellerid,
                        'created_date' => $now
                ));
            } else {
                $prd_array = array(
                    array(
                        'prd_name' => $product_name,
                        'product_type' => $is_configurable,
                        'customer_ip' => $customer_ip,
                        'product_id' => $product_id,
                        'quantity' => $quantity,
                        'shipping_charge' => $shipping_charge,
                        'shipping_charge_id' => $shipping_charge_id,
                        'shipping_charged_name' => $shipping_charged_name,
                        'sub_total' => $prd_price,
                        'grand_total' => $last_prdprice,
                        'store_currency' => $store_currency,
                        'prd_sku' => $prd_sku,
                        'seller_id' => $store_sellerid,
                        'created_date' => $now
                ));
            }

            if (session('cart_data')) {
                $session_array = session('cart_data');
                $array_update_search = array_search($product_id, array_column($session_array, 'product_id'));
                if (strlen($array_update_search) != "") {
                    $count_lastsession = count($session_array);
                    $last_update_qty = $session_array[$array_update_search]['quantity'] + 1;
                    $grand_total = $session_array[$array_update_search]['grand_total'] * $last_update_qty;
                    $grand_total_format = sprintf('%0.2f', $grand_total);
                    $prd_array = array(
                        array(
                            'prd_name' => $session_array[$array_update_search]['prd_name'],
                            'product_type' => $session_array[$array_update_search]['product_type'],
                            'customer_ip' => $session_array[$array_update_search]['customer_ip'],
                            'product_id' => $session_array[$array_update_search]['product_id'],
                            'quantity' => $last_update_qty,
                            'shipping_charge' => $session_array[$array_update_search]['shipping_charge'],
                            'shipping_charge_id' => $session_array[$array_update_search]['shipping_charge_id'],
                            'shipping_charged_name' => $session_array[$array_update_search]['shipping_charged_name'],
                            'sub_total' => $session_array[$array_update_search]['sub_total'],
                            'grand_total' => $grand_total_format,
                            'store_currency' => $session_array[$array_update_search]['store_currency'],
                            'prd_sku' => $session_array[$array_update_search]['prd_sku'],
                            'seller_id' => $session_array[$array_update_search]['seller_id'],
                            'created_date' => $session_array[$array_update_search]['created_date']
                    ));

                    if ($session_array[$array_update_search]['product_type'] == "configurable") {
                        $variable_array = array(
                            array(
                                'customer_variationlabel' => $session_array[$array_update_search]['customer_variationlabel'],
                                'customer_variationvalue' => $session_array[$array_update_search]['customer_variationvalue'],
                            )
                        );
                        $prd_array = array_merge($prd_array, $variable_array);
                    }


                    if ($count_lastsession == 1) {
                        $request->session()->put('cart_data', $prd_array);
                    } else {
                        unset($session_array[$array_update_search]);     //Remove From Sesssion Array                        
                        $merge_array = array_merge($session_array, $prd_array);
                        $request->session()->put('cart_data', $merge_array);
                    }
                } else {
                    $merge_array = array_merge($session_array, $prd_array);
                    $request->session()->put('cart_data', $merge_array);
                }
            } else {
                $request->session()->put('cart_data', $prd_array);
            }



            $request->session()->put('cart_details', $customer_ip);
            session()->flash('success', "Your Item added in cart successfully");
            return redirect()->route($this->_config['redirect']);
        }
    }

    function cartupdate(Request $request) {
        $cart_itemsid = $_POST['cart_uid'];
        $cart_qty = $_POST['cart_qty'];
        $cart_price = $_POST['cart_price'];
        $n = 0;

        if (!isset(auth()->guard('customer')->user()->id)) {
            $session_data = session('cart_data');
            $cart_array = array();
            foreach ($session_data as $session_cart):
                $cart_array[] = array(
                    'prd_name' => $session_cart['prd_name'],
                    'product_type' => $session_cart['product_type'],
                    'customer_ip' => $session_cart['customer_ip'],
                    'product_id' => $session_cart['product_id'],
                    'quantity' => $cart_qty[$n],
                    'shipping_charge' => $session_cart['shipping_charge'],
                    'shipping_charge_id' => $session_cart['shipping_charge_id'],
                    'shipping_charged_name' => $session_cart['shipping_charge_id'],
                    'sub_total' => $session_cart['sub_total'],
                    'grand_total' => $cart_price[$n],
                    'store_currency' => $session_cart['store_currency'],
                    'prd_sku' => $session_cart['prd_sku'],
                    'seller_id' => $session_cart['seller_id'],
                    'created_date' => $session_cart['created_date']
                );
                $n++;
            endforeach;
            Session::forget('cart_data');
            Session::flush('cart_data');
            Session::regenerate('cart_data');
            Session::put('cart_data', $cart_array);
            return 1;
        } else {
            foreach ($cart_itemsid as $cartid):
                $getcart_qty = $cart_qty[$n];
                $getcart_price = $cart_price[$n];

                $update_array = array(
                    'items_qty' => $getcart_qty,
                    'grand_total' => $getcart_price
                );
                $update_query = DB::table('cart_detail')->where('cart_id', $cartid)->update($update_array);
                $n++;
            endforeach;
            echo 1;
        }
        die();
    }

    function cartremove() {
        $cart_removeid = $_POST['cart_removeid'];

        if (!isset(auth()->guard('customer')->user()->id)):
            $session_data = session('cart_data');
            $array_delete_search = array_search($cart_removeid, array_column($session_data, 'product_id'));
            unset($session_data[$array_delete_search]);
            Session::put('cart_data', $session_data);
            return 1;
        else:
            $delete_cartitem = DB::table('cart_detail')->where('cart_id', $cart_removeid)->delete();
            echo $delete_cartitem;
        endif;
        die();
    }

    public function addConfigurable($slug) {
        session()->flash('warning', trans('shop::app.checkout.cart.add-config-warning'));
        return redirect()->route('shop.products.index', $slug);
    }

    public function buyNow($id) {
        Event::fire('checkout.cart.add.before', $id);

        $result = Cart::proceedToBuyNow($id);

        Event::fire('checkout.cart.add.after', $result);

        Cart::collectTotals();

        if (!$result) {
            return redirect()->back();
        } else {
            return redirect()->route('shop.checkout.onepage.index');
        }
    }

    /**
     * Function to move a already added product to wishlist
     * will run only on customer authentication.
     *
     * @param instance cartItem $id
     */
    public function moveToWishlist($id) {
        $result = Cart::moveToWishlist($id);

        if (!$result) {
            Cart::collectTotals();

            session()->flash('success', trans('shop::app.wishlist.moved'));

            return redirect()->back();
        } else {
            session()->flash('warning', trans('shop::app.wishlist.move-error'));

            return redirect()->back();
        }
    }

}
