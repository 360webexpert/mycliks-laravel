<?php

namespace Webkul\Shop\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Core\Repositories\SliderRepository as Sliders;

/**
 * Home page controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class HomeController extends controller {

    protected $_config;
    protected $sliders;
    protected $current_channel;

    public function __construct(Sliders $s) {
        $this->_config = request('_config');
        $this->sliders = $s;
    }

    /**
     * loads the home page for the storefront
     */
//    public function index() {        
//        $current_channel = core()->getCurrentChannel();
//        $all_sliders = $this->sliders->findWhere(['channel_id' => $current_channel['id']]);
//        return view('shop::home.index')->with('sliderData', $all_sliders->toArray());
//    }

    public function index() {
 
        $cat_excludes='';
        $banner_slider = DB::table('sliders')
                ->orderby('id', 'asc')
                ->get()
                ->toArray();

        $cats = DB::table('categories')
                ->select('categories.image', 'category_translations.name', 'category_translations.slug')
                ->join('category_translations', 'categories.id', '=', 'category_translations.category_id')
                ->where('categories.status', '=', 1)
                //->WhereNull('categories.parent_id')
                ->orderby('position', 'asc')
                ->get()
                ->toArray();
        
        $currency=DB::table('currencies')->select('CODE')->where('id',1)->get()->toarray();
        
        
        
        $our_picks = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                
                ->join('product_categories','product_flat.product_id','=','product_categories.product_id')
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->join('marketplace_products', 'product_flat.product_id', '=', 'marketplace_products.product_id')
                ->where('product_categories.category_id','=',19)
                ->where('marketplace_products.is_approved','=',1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        $men_women = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')                
                ->where('product_flat.menswomens', 1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        $babies_info = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                                
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->where('product_flat.everythingbabies', 1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        $rocking_promotion = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->where('product_flat.rockingpromotions', 1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        $fav_electronic = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->where('product_flat.favelectronic', 1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        $maintain_beauty = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->where('product_flat.maintainbeauty', 1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        $createive_handcraft = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->where('product_flat.creativehandcraft', 1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        
        
        
        $special_attention = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                                
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->where('product_flat.new','=',1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();

        $fashion = DB::table('product_flat')
                ->select(DB::RAW('DISTINCT(product_images.product_id)'),'product_flat.*')                                
                ->join('product_images', 'product_flat.product_id', '=', 'product_images.product_id')
                ->join('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
                ->where('product_flat.featured','=',1)
                ->orderby('product_images.product_id','asc')
                ->get()
                ->toArray();
        
        
        return view('shop::home.home', ['currency'=>$currency,'page_title','Home','banner_slider' => $banner_slider, 'categories' => $cats, 'our_picks' => $our_picks,'special_attention'=>$special_attention,'fashion_prd'=>$fashion,'men_women'=>$men_women, 'rocking_promotion'=>$rocking_promotion, 'fav_electronic'=>$fav_electronic, 'maintain_beauty'=>$maintain_beauty, 'createive_handcraft'=>$createive_handcraft,'babies_info'=> $babies_info]);
    }
}
