<?php

namespace Webkul\Shop\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Webkul\Shop\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Core\Repositories\SliderRepository as Sliders;

/**
 * Home page controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class PagesController extends controller {
    protected $_config;
    protected $sliders;
    protected $current_channel;
    
    public function about(){
        $page_title="About";
        $page_desc="";
        $page_key="";
        return view("shop::pages.about", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }
    
    public function our_news(){
        $page_title="Our News";
        $page_desc="";
        $page_key="";
        return view("shop::pages.ournews", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }
    
    public function press_rooms(){
        $page_title="Press Rooms";
        $page_desc="";
        $page_key="";
        return view("shop::pages.press", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }
    
    public function partnership(){
        $page_title="Partnership";
        $page_desc="";
        $page_key="";
        return view("shop::pages.partnership", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }
    
    public function connectwithus(){
        $page_title="Connect With Us";
        $page_desc="";
        $page_key="";
        return view("shop::pages.connect", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }

    public function policy(){
        $page_title="Policy";
        $page_desc="";
        $page_key="";
        return view("shop::pages.policy", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }

    public function sell(){
        $page_title="Sell";
        $page_desc="";
        $page_key="";
        return view("shop::pages.sell", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }

    public function help(){
        $page_title="Help";
        $page_desc="";
        $page_key="";
        return view("shop::pages.help", ['page_title'=>$page_title, 'meta_description'=>$page_desc, 'meta_keywords'=>$page_key]);
    }
}