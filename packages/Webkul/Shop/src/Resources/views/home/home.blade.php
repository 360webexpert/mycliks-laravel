@extends('shop::layouts.main')
@section('page_title', 'Home') {{-- For different pages you can change your title here --}}    
@section('middle_content')


@php
$count=count($banner_slider);
@endphp

@if($count!=0)
<div class="banner_slider">
    @if($count==1)
    <div class="banner_image">
        <img src='{{ url()->to('/') }}/storage/{{$banner_slider[0]->path}}' >
    </div>
    @else
    <ul class="slick_slider">
        @foreach($banner_slider as $sliders)
        <li><img class="banner_slider_image" src='{{ url()->to('/') }}/storage/{{$sliders->path}}'></li>
        @endforeach
    </ul>
    @endif
</div>
@endif


<script>
    $(document).ready(function () {
        $('.slick_slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            dots: true,
            centerPadding: '10px',
            infinite: true,
        });

        $('.mypicks').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            infinite: true,
            prevArrow: "<div class='lft-btn'><i class='fa fa-angle-left'></i></div>",
            nextArrow: "<div class='rgt-btn'><i class='fa fa-angle-right'></div>",
            arrows: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script>

<div class="banner-logo">
    <div class="bottom-row-log page-main ct-main">
        <ul>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/fast_check_out.png" width="188" height="187"><span>Mycliks Guarantee</span></li>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/quality_guaranteed.png" width="188" height="187"><span>Quality Guaranteed</span></li>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/verified_seller.png" width="198" height="195"><span>Verified Sellers</span></li>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/special_deals.png" width="188" height="187"><span>Freemium Platform</span></li>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/secure_transaction.png" width="188" height="187"><span>Secure Transaction</span></li>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/dispute-02.png" width="188" height="188"><span>Dispute Resolution</span></li>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/free_shipping.png" width="188" height="187"><span>Free Shipping</span></li>
            <li><img src="{{ url()->to('/') }}/storage/banner_logo/handmades.png" width="188" height="187"><span>Handmades</span></li>
        </ul>
    </div>
</div>


<div class="all_categories top">  
    @php
    $count_cats=count($categories);
    @endphp
    @if($count_cats!=0)
    <div class="product_categories">
        <div class="featured-heading categories">Categories</div>
        <ul class="mycats">
            @foreach($categories as $cats)
            <li> 
                <a href="{{ url()->to('/') }}/categories/{{$cats->slug}}">
                    <div class="right">
                        <span><img src="{{ url()->to('/') }}/storage/{{$cats->image}}" width="428" height="226"></span>
                    </div>
                    <div class="left"><h4>{{$cats->name}}</h4></div>
                </a>
            </li> 
            @endforeach
        </ul> 
    </div>
    @endif
    <div class="all_categories_inner"> 
        <div class="all_categories_left">
            <!-- 1st -->
            @php
            $count_picks="";
            $count_picks="";
            $seller_products="";
            $count_picks=count($men_women);
            @endphp
            @if($count_picks!=0)
            <div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">All Men's And Women's Fashion</div>
                    <div class="mypicks">
                        @foreach($men_women as $mypicks)
                        @php
                        $get_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $seller_products = DB::table('marketplace_products')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        
                        if($mypicks->special_price!=0):
                        $sale_price=$diff_price=$original_price=$devide_price=$percentage_price=$get_percentage="";
                        $sale_price=$mypicks->price;
                        $original_price=$mypicks->special_price;
                        $diff_price=$sale_price-$original_price;
                        $devide_price=$diff_price/$sale_price;
                        $percentage_price=$devide_price*100;
                        $get_percentage=number_format($percentage_price);
                        endif;
                        @endphp

                        
                        @if(!empty($seller_products) && $seller_products[0]->is_approved==0): 
                        
                        @else:
                        
                        <div class="product-card" id="prd_{{$mypicks->product_id}}">
                            @if($mypicks->special_price!=0)
                            <span class="discount_offer">{{$get_percentage}}% OFF</span>
                            @endif
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                        <span>{{$mypicks->name}}</span>
                                    </a>
                                </div>
                                @if($get_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span><del>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</del></span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                                
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div> 
                        @endif;
                        @endforeach
                    </div>  
                </div>          
            </div>
            @endif


            <!-- 2nd -->
            @php
            $count_picks="";
            $mypicks="";
            $seller_products="";
            $count_picks=count($babies_info);
            @endphp
            @if($count_picks!=0)
            <div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">Everything For Babies</div>
                    <div class="mypicks">
                        @foreach($babies_info as $mypicks)
                        @php
                        $get_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $seller_products = DB::table('marketplace_products')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        
                        if($mypicks->special_price!=0):
                        $sale_price=$diff_price=$original_price=$devide_price=$percentage_price=$get_percentage="";
                        $sale_price=$mypicks->price;
                        $original_price=$mypicks->special_price;
                        $diff_price=$sale_price-$original_price;
                        $devide_price=$diff_price/$sale_price;
                        $percentage_price=$devide_price*100;
                        $get_percentage=number_format($percentage_price);
                        endif;
                        @endphp
                        
                        @if(!empty($seller_products) && $seller_products[0]->is_approved==0): 
                        
                        @else:
                        <div class="product-card"  id="prd_{{$mypicks->product_id}}">
                            @if($mypicks->special_price!=0)
                            <span class="discount_offer">{{$get_percentage}}% OFF</span>
                            @endif
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                        <span>{{$mypicks->name}}</span>
                                    </a>
                                </div>
                                @if($get_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span><del>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</del></span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                               
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div> 
                        @endif
                        @endforeach
                    </div>  
                </div>          
            </div>
            @endif

            <!-- 3rd -->
            @php
            $count_picks="";
            $mypicks="";
            $seller_products="";
            $count_picks=count($rocking_promotion);
            @endphp
            @if($count_picks!=0)
            <div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">Rocking Promotion</div>
                    <div class="mypicks">
                        @foreach($rocking_promotion as $mypicks)
                        @php
                        $get_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $seller_products = DB::table('marketplace_products')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        
                        
                        if($mypicks->special_price!=0):
                        $sale_price=$diff_price=$original_price=$devide_price=$percentage_price=$get_percentage="";
                        $sale_price=$mypicks->price;
                        $original_price=$mypicks->special_price;
                        $diff_price=$sale_price-$original_price;
                        $devide_price=$diff_price/$sale_price;
                        $percentage_price=$devide_price*100;
                        $get_percentage=number_format($percentage_price);
                        endif;
                        @endphp
                        
                        @if(!empty($seller_products) && $seller_products[0]->is_approved==0): 
                        
                        @else:
                        <div class="product-card"  id="prd_{{$mypicks->product_id}}">
                            @if($mypicks->special_price!=0)
                            <span class="discount_offer">{{$get_percentage}}% OFF</span>
                            @endif
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                        <span>{{$mypicks->name}}</span>
                                    </a>
                                </div>
                                @if($get_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span><del>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</del></span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                               
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>  
                </div>          
            </div>
            @endif



            <!-- 4th  -->
            @php            
            $count_picks="";
            $mypicks="";
            $seller_products="";
            $count_picks=count($fav_electronic);
            @endphp
            @if($count_picks!=0)
            <div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">Your Favourite Electronics</div>
                    <div class="mypicks">
                        @foreach($fav_electronic as $mypicks)
                        @php
                        $get_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $seller_products = DB::table('marketplace_products')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        
                        if($mypicks->special_price!=0):
                        $sale_price=$diff_price=$original_price=$devide_price=$percentage_price=$get_percentage="";
                        $sale_price=$mypicks->price;
                        $original_price=$mypicks->special_price;
                        $diff_price=$sale_price-$original_price;
                        $devide_price=$diff_price/$sale_price;
                        $percentage_price=$devide_price*100;
                        $get_percentage=number_format($percentage_price);
                        endif;
                        @endphp 
                        
                        @if(!empty($seller_products) && $seller_products[0]->is_approved==0): 
                        
                        @else:
                        <div class="product-card"  id="prd_{{$mypicks->product_id}}">
                            @if($mypicks->special_price!=0)
                            <span class="discount_offer">{{$get_percentage}}% OFF</span>
                            @endif
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                        <span>{{$mypicks->name}}</span>
                                    </a>
                                </div>
                                @if($get_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span><del>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</del></span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                               
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div> 
                        @endif
                        @endforeach
                    </div>  
                </div>          
            </div>
            @endif            


            <!-- 5th -->
            @php
            $count_picks="";
            $mypicks="";
            $seller_products="";
            $count_picks=count($maintain_beauty);
            @endphp
            @if($count_picks!=0)
            <div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">Maintain Your Beauty</div>
                    <div class="mypicks">
                        @foreach($maintain_beauty as $mypicks)
                        @php
                        $get_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $seller_products = DB::table('marketplace_products')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        
                        if($mypicks->special_price!=0):
                        $sale_price=$diff_price=$original_price=$devide_price=$percentage_price=$get_percentage="";
                        $sale_price=$mypicks->price;
                        $original_price=$mypicks->special_price;
                        $diff_price=$sale_price-$original_price;
                        $devide_price=$diff_price/$sale_price;
                        $percentage_price=$devide_price*100;
                        $get_percentage=number_format($percentage_price);
                        endif;
                        @endphp
                        
                        @if(!empty($seller_products) && $seller_products[0]->is_approved==0): 
                        
                        @else:
                        <div class="product-card" id="prd_{{$mypicks->product_id}}">
                            @if($mypicks->special_price!=0)
                            <span class="discount_offer">{{$get_percentage}}% OFF</span>
                            @endif
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                        <span>{{$mypicks->name}}</span>
                                    </a>
                                </div>
                                @if($get_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span><del>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</del></span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                                
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div> 
                        @endif
                        @endforeach
                    </div>  
                </div>          
            </div>
            @endif


            <!-- 6th -->
            @php
            $count_picks="";
            $mypicks="";
            $seller_products="";
            $count_picks=count($createive_handcraft);
            @endphp
            @if($count_picks!=0)
            <div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">Creative Handcrafts</div>
                    <div class="mypicks">
                        @foreach($createive_handcraft as $mypicks)
                        @php
                        $get_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        $seller_products = DB::table('marketplace_products')->where('product_id', '=', $mypicks->product_id)->get()->toArray();
                        
                        if($mypicks->special_price!=0):
                        $sale_price=$diff_price=$original_price=$devide_price=$percentage_price=$get_percentage="";
                        $sale_price=$mypicks->price;
                        $original_price=$mypicks->special_price;
                        $diff_price=$sale_price-$original_price;
                        $devide_price=$diff_price/$sale_price;
                        $percentage_price=$devide_price*100;
                        $get_percentage=number_format($percentage_price);
                        endif;
                        @endphp
                        
                        @if(!empty($seller_products) && $seller_products[0]->is_approved==0): 
                        
                        @else:
                        <div class="product-card" id="prd_{{$mypicks->product_id}}">
                            @if($mypicks->special_price!=0)
                            <span class="discount_offer">{{$get_percentage}}% OFF</span>
                            @endif
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$mypicks->url_key}}" title="{{$mypicks->name}}">
                                        <span>{{$mypicks->name}}</span>
                                    </a>
                                </div>
                                @if($get_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span><del>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</del></span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                                
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>  
                </div>          
            </div>
            @endif


            @php
            $count_special_attention=count($special_attention);
            @endphp
            @if($count_special_attention!=0)
            <!--<div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">Maintain Your Beauty</div>
                    <ul class="mypicks">
                        @foreach($special_attention as $special_att) 
                        @php
                        $get_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $special_att->product_id)->get()->toArray();
                        $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $special_att->product_id)->get()->toArray();
                        @endphp
                        <div class="product-card">
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$special_att->url_key}}" title="{{$special_att->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$special_att->url_key}}" title="{{$special_att->name}}">
                                        <span>{{$special_att->name}}</span>
                                    </a>
                                </div>
                                @if($get_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                                 
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div> 
                        @endforeach
                    </ul>  
                </div>          
            </div>-->
            @endif




            @php
            $count_fashion=count($fashion_prd);
            @endphp
            @if($count_fashion!=0)
            <!--<div class="all_categories box-design-product">
                <div class="all_categories_inner">
                    <div class="featured-heading categories">Creative Handcrafts</div>
                    <ul class="mypicks">
                        @foreach($fashion_prd as $fashion) 
                        @php
                        $fashion_inventroy = DB::table('product_inventories')->select('qty')->where('product_id', '=', $fashion->product_id)->get()->toArray();
                        $fashion_image = DB::table('product_images')->select('path')->where('product_id', '=', $fashion->product_id)->get()->toArray();
                        @endphp
                        <div class="product-card">
                            <div class="product-image">
                                <a href="{{ url()->to('/') }}/products/{{$fashion->url_key}}" title="{{$fashion->name}}">
                                    <img src="{{ url()->to('/') }}/storage/{{$fashion_image[0]->path}}"/>
                                </a>
                            </div>
                            <div class="product-information">
                                <div class="product-name">
                                    <a href="{{ url()->to('/') }}/products/{{$fashion->url_key}}" title="{{$fashion->name}}">
                                        <span>{{$fashion->name}}</span>
                                    </a>
                                </div>
                                @if($fashion_inventroy[0]->qty>0)
                                <div class="product-price">
                                    @if($mypicks->special_price!=0)
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->special_price,2)}}</span>
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @else
                                    <span>{{$currency[0]->CODE}}{{number_format($mypicks->price, 2)}}</span>
                                    @endif
                                </div>                                 
                                @else
                                <span class="red">Out of stock</span>
                                @endif
                            </div>
                        </div> 
                        @endforeach
                    </ul>  
                </div>          
            </div>-->
            @endif



        </div>  

        <div class="all_categories_right">
            <div class="image-one">
                <span style="background-image: url({{ url()->to('/') }}/storage/other_images/shoes.png)"></span>            
                <span style="background-image: url({{ url()->to('/') }}/storage/other_images/watch.png)"></span>
                <span style="background-image: url({{ url()->to('/') }}/storage/other_images/fashion1.png)"></span>         
                <span style="background-image: url({{ url()->to('/') }}/storage/other_images/sidebar_image1.jpg)"></span>
            </div>
        </div>
    </div>           
</div>






@endsection