@extends('shop::layouts.master')
@section('page_title')
{{ $page_title }}
@stop

@section('seo')
<meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
<meta name="keywords" content="{{ $meta_keywords }}"/>
@stop

<style>
    #header .main-container-wrapper {
    max-width: 1300px!important;
    padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }
</style>
@section('content-wrapper')
<div class="press press-rooms">
    <div class="div_banner">
        <div class="banner" style="background-image: url(<?php echo bagisto_asset('images/aboutusbanner-01.png'); ?>)">

        </div>
    </div>

    <div class="div_navigator">
        <ul class="banner-content">
            <li><a href="{{ URL::to('/about') }}">About us</a></li>
            <li><a href="{{ URL::to('/our-news') }}">Our News</a></li>
            <li class="active"><a href="javascript:void(0)">Press Room</a></li>
            <li><a href="{{ URL::to('/partnership') }}">Partnership</a></li>
            <li><a href="{{ URL::to('/connect-with-us') }}">Connect with us</a></li>
        </ul>
    </div>

    <h1 class="my_h1"> Press Room </h1>


    <div class="div_press_container">
        <div class="div_press_item">
            <div class="div_title">
                Mycliks Inc Unveils, Marketplace For Fashion and Handmade Products
            </div>
            <div class="div_auther">
                Posted by <p>Auther </p> on 1/1/1111
            </div>
            <div class="div_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat distinctio magnam culpa voluptates
                debitis non deleniti voluptatem optio accusamus tenetur laudantium quisquam provident fugit possimus
                cupiditate, a quis et praesentium.
            </div>
            <div class="div_actions">
                <a href="#">Read More</a>
            </div>

        </div>
        <div class="div_press_item">
            <div class="div_title">
                Mycliks Inc Unveils, Marketplace For Fashion and Handmade Products
            </div>
            <div class="div_auther">
                Posted by <p>Auther </p> on 1/1/1111
            </div>
            <div class="div_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat distinctio magnam culpa voluptates
                debitis non deleniti voluptatem optio accusamus tenetur laudantium quisquam provident fugit possimus
                cupiditate, a quis et praesentium.
            </div>
            <div class="div_actions">
                <a href="#">Read More</a>
            </div>

        </div>
        <div class="div_press_item">
            <div class="div_title">
                Mycliks Inc Unveils, Marketplace For Fashion and Handmade Products
            </div>
            <div class="div_auther">
                Posted by <p>Auther </p> on 1/1/1111
            </div>
            <div class="div_text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat distinctio magnam culpa voluptates
                debitis non deleniti voluptatem optio accusamus tenetur laudantium quisquam provident fugit possimus
                cupiditate, a quis et praesentium.
            </div>
            <div class="div_actions">
                <a href="#">Read More</a>
            </div>

        </div>

    </div>
</div>
@endsection