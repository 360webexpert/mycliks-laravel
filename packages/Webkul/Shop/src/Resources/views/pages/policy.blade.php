@extends('shop::layouts.master')
@section('page_title')
{{ $page_title }}
@stop

@section('seo')
<meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
<meta name="keywords" content="{{ $meta_keywords }}"/>
@stop
<style>
    #header .main-container-wrapper {
    max-width: 1300px!important;
    padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }

    /* policy styles */
    body {
            background-color: rgba(190, 190, 190, 0.5);
        }

        .div_categs {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            align-items: center;
            justify-content: center;
            width: 1000px;
            border: 0px solid red;
            margin-bottom: 20px;
            margin-left: auto;
            margin-right: auto;


        }

        .div_categs .div_item img {
            display: block;
            border: 0px solid violet;
            margin-top: 20px;
            margin-bottom: 5px;
        }

        .div_categs .div_item {
            border: 0.5px solid rgba(170, 170, 170, 0.5);
            width: 300px;
            height: 170px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: flex-start;
            text-align: center;
            background-color: white;
            cursor: pointer;
        }

        .div_categs .div_item:hover {
            filter: invert(10%);
        }

        .div_categs .div_item p {
            font-family: "HelveticaNeue", Arial, Helvetica, sans-serif;
            font-size: 18px;
            color: gray;
            margin-top: 5px;
        }

        .div_categs .div_data1,
        .div_categs .div_data2,
        .div_categs .div_data3 {
            border: 0px solid blue;
            width: 900px;
            padding: 4px;
            font-size: 1.2em;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .div_categs .div_data1,
        .div_categs .div_data2 {
            padding: 0px;
            height: 0px;
            overflow: hidden;
            display: inline-block;

            font-family: "HelveticaNeue", Arial, Helvetica, sans-serif;
            font-size: 14px;

        }

        /* uncomment for hover or click */
        .div_categs .div_item.fr:hover~.div_data1 {
          
            height: auto;

            padding: 25px;
        }

        .div_categs .div_item.sr:hover~.div_data2 {
      
            height: auto;
            padding: 25px;
        }

        /* policy banner */
        .div_banner {margin: 0;}

        .div_banner .banner {
            background-image: url('{{bagisto_asset("images/policybanner.png")}}');
            background-size: cover;
            border: 0px solid red;
            height: 300px;
            margin: 0;
            padding: 0;
        }

        .div_navigator {
            background-color: rgba(200, 200, 200, 1);
            padding: 0;
            margin: 0;
        }

        .div_navigator .banner-content {
            border: 0px solid red;
            width: 100%;
            display: flex;
            flex-direction: row;
            padding: 0;
            margin: 0;
            align-items: center;
            justify-content: center;


        }

        .div_navigator .banner-content li {
            padding-left: 25px;
            padding-right: 25px;
            text-decoration: none;
            list-style-type: none;
            border: 0px solid green;
            padding-top: 15px;
            padding-bottom: 15px;
            line-height: 120%;
        }

        .div_navigator .banner-content li a {

            text-decoration: none;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 20px;
            font-weight: 500%;
            color: gray;
            transition-duration: 500ms;
        }

        .div_navigator .banner-content li a:hover {

            color: darkred;
            transition-duration: 500ms;
        }

        .banner-content {
            display: flex;
            flex-flow: row;
            align-items: center;
            justify-content: center;
            flex-wrap: wrap;
        }
</style>
@section('content-wrapper')
<div class="div_banner"><div class="banner"></div></div></div>
<div class="container1" style="text-align: center;">
        <h2 class="my_h1">Learn all about our policies and agreements. </h2>
    </div>

    <div class="div_categs">
        <div class="div_item fr">
            <a href="#"><img src="{{bagisto_asset('images/terms.png')}}" /></a>
            <p>Terms of service</p>
        </div>
        <div class="div_item fr">
            <a href="#"><img src="{{bagisto_asset('images/privacy1.png')}}" /></a>
            <p>Privacy policy</p>
        </div>
        <div class="div_item fr">
            <a href="#"><img src="{{bagisto_asset('images/prohibited.png')}}" /></a>
            <p>Prohibited and Restricted items Policy</p>
        </div>

        <div class="div_data1">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nihil harum illo labore eligendi officiis,
            temporibus ipsum dolores placeat nostrum dicta, sequi incidunt aliquam repudiandae. Placeat esse accusamus
            natus sit quam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis tenetur quos, iure
            adipisci rerum
            blanditiis earum saepe nulla itaque facilis magnam exercitationem sit optio est maxime, eius minima
            praesentium! Quam.
        </div>

        <div class="div_item sr">
            <a href="#"><img src="{{bagisto_asset('images/return.png')}}" /></a>
            <p>Returns and Refund Policy</p>
        </div>
        <div class="div_item sr">
            <a href="#"><img src="{{bagisto_asset('images/agreement.png')}}" /></a>
            <p>User Agreement</p>
        </div>
        <div class="div_item sr">
            <a href="#"><img src="{{bagisto_asset('images/cookies.png')}}" /></a>
            <p>Cookies Handling</p>
        </div>
        <div class="div_data2">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nihil harum illo labore eligendi officiis,
            temporibus ipsum dolores placeat nostrum dicta, sequi incidunt aliquam repudiandae. Placeat esse accusamus
            natus sit quam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis tenetur quos, iure
            adipisci rerum
            blanditiis earum saepe nulla itaque facilis magnam exercitationem sit optio est maxime, eius minima
            praesentium! Quam.
        </div>
    </div>
@endsection