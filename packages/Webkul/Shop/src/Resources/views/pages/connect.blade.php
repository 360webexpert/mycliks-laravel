@extends('shop::layouts.master')
@section('page_title')
{{ $page_title }}
@stop

@section('seo')
<meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
<meta name="keywords" content="{{ $meta_keywords }}"/>
<style>
    #header .main-container-wrapper {
    max-width: 1300px!important;
    padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }

    .div_container_connectus {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items: center;
        justify-content: center;
        width: 100%;
        border: 0px solid red;
    }

    .div_container_connectus .div_connect_item {
        display: flex;
        flex-flow: row;
        align-items: center;
        justify-content: center;
        border: 0px solid green;
        margin: 10px;
        margin-right: 30px;
        text-decoration: none;
        color: black;
    }

    .div_container_connectus .div_text {
        font-size: 1.3em;
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
        color: rgb(102, 102, 102);
        margin: 15px;
    }
</style>
@section('content-wrapper')
<div class="our-news page-news">
    <div class="div_banner">
        <div class="banner" style="background-image: url(<?php echo bagisto_asset('images/aboutusbanner-01.png'); ?>)">

        </div>
    </div>

    <div class="div_navigator">
        <div class="div_navigator">
            <ul class="banner-content">
                <li><a href="{{ URL::to('/about') }}">About us</a></li>
                <li><a href="{{ URL::to('/our-news') }}">Our News</a></li>
                <li><a href="{{ URL::to('/press-rooms') }}">Press Room</a></li>
                <li><a href="{{ URL::to('/partnership') }}">Partnership</a></li>
                <li class="active"><a href="javascript:void(0)">Connect with us</a></li>
            </ul>
        </div>
    </div>

    <div class="div_container_connectus">
        <a href="." class="div_connect_item">
            <img src="https://www.mycliks.com/pub/media/wysiwyg/fb.png" />
            <div class="div_text">
                Connect with facebook
            </div>
        </a>
        <a href="." class="div_connect_item">
            <img src="https://www.mycliks.com/pub/media/wysiwyg/pin.png" />
            <div class="div_text">
                Connect with pinterest
            </div>
        </a> <a href="." class="div_connect_item">
            <img src="https://www.mycliks.com/pub/media/wysiwyg/twi.png" />
            <div class="div_text">
                Connect with twitter
            </div>
        </a> <a href="." class="div_connect_item">
            <img src="https://www.mycliks.com/pub/media/wysiwyg/gog.png" />
            <div class="div_text">
                Connect with google
            </div>
        </a> <a href="." class="div_connect_item">
            <img src="https://www.mycliks.com/pub/media/wysiwyg/link.png" />
            <div class="div_text">
                Connect with linkedin
            </div>
        </a> <a href="." class="div_connect_item">
            <img src="https://www.mycliks.com/pub/media/wysiwyg/insta.png" />
            <div class="div_text">
                Connect with Instagram
            </div>
        </a> <a href="." class="div_connect_item">
            <img src="https://www.mycliks.com/pub/media/wysiwyg/y-tube.png" />
            <div class="div_text">
                Connect with Youtube
            </div>
        </a>
    </div>

</div>
@endsection