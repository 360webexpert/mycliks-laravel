@extends('shop::layouts.master')
@section('page_title')
{{ $page_title }}
@stop

@section('seo')
<meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
<meta name="keywords" content="{{ $meta_keywords }}"/>
@stop

<style>
    #header .main-container-wrapper {
    max-width: 1300px!important;
    padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }
</style>
@section('content-wrapper')
<div class="about page-about">
    <div class="div_banner">
        <div class="banner" style="color:red; background-image: url(<?php echo bagisto_asset('images/aboutusbanner-01.png'); ?>)">

        </div>
    </div>
    <div class="div_navigator">
        <div class="div_navigator">
            <ul class="banner-content">
                <li class="active"><a href="javascript:void(0)">About us</a></li>
                <li><a href="{{ URL::to('/our-news') }}">Our News</a></li>
                <li><a href="{{ URL::to('/press-rooms') }}">Press Room</a></li>
                <li><a href="{{ URL::to('/partnership') }}">Partnership</a></li>
                <li><a href="{{ URL::to('/connect-with-us') }}">Connect with us</a></li>
            </ul>
        </div>
    </div>
    <div class="div_about_wrapper">
        <div class="div_left">
            <h1 class="my_h1">Mycliks.Com </h1>
            <p>
                Launched in 2018, Mycliks.com is an E-commerce marketplace in Malaysia connecting the buyers and sellers
                in wide range of categories and services.
            </p>
            <img src="{{ bagisto_asset('images/a1.png') }}" />
            <h1 class="my_h1">
                Our Vision
            </h1>
            <p>Envisioned to be a social-oriented marketplace for users. </p>
            <img src="{{ bagisto_asset('images/a2.png') }}" />
        </div>
        <div class="div_right">
            <img src="{{ bagisto_asset('images/a3.png') }}" />
            <h1 class="my_h1">Our Mission</h1>
            <p>MYCLIKS aim to be a complete online Eco-System for Internet Users
            </p>
            <img src="{{ bagisto_asset('images/a4.png') }}" />
            <h1 class="my_h1">For Buyers & Sellers </h1>
            <p>A BRIDGE between buyers and sellers. A HASSEL FREE digital shopping mall connecting millions of products
                as your choice and FREEMIUM platform for seller's & anyone with a product to SELL.
            </p>
        </div>
    </div>
</div>
@endsection