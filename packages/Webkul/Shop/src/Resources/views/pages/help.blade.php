@extends('shop::layouts.master')
@section('page_title')
{{ $page_title }}
@stop

@section('seo')
<meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
<meta name="keywords" content="{{ $meta_keywords }}"/>
@stop

<style>
    #header .main-container-wrapper {
    max-width: 1300px!important;
    padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }

    /* help styles */
    .div_help_wrapper {
            border: 0px solid red;
            display: flex;
            margin-left: auto;
            margin-right: auto;
            flex-direction: column;
            flex-wrap: wrap;
            padding: 15px;
            align-items: center;
            justify-content: center;
            width: 100%;
            box-sizing: border-box;
            min-width: 1000px;
    		max-width: 1200px;
            background-color: rgb(240, 240, 240);
        }

        .txt_search {
            background-image: url('https://static.takealot.com/images/search-icon.svg');
            background-repeat: no-repeat;
            background-position-x: 10px;
            background-position-y: 11px;
            background-origin: border-box;
            background-size: auto;
            padding: 10px;
            border: 1px solid gray;
            text-align: center;
            width: 100%;
            margin-bottom: 20px;
        }

        .txt_search::after {
            content: "";
            clear: both;
            display: block;
            border: 1px solid red;
        }

        .div_question_wrapper {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: center;
            border: 0px solid green;
            cursor: pointer;
            width: 100%;
        }

        .div_question {
            font-family: Verdana, Geneva, Tahoma, sans-serif;
            font-size: 1.1em;
            color: rgb(102, 102, 102);
            background-color: white;
            border: 0px solid rgb(180, 180, 180);
            width: 100%;
            padding: 10px;
            box-sizing: border-box;

        }

        .div_question p {
            position: relative;
            border: 0px solid black;
            float: right;
            font-size: 1.3em;
            padding: 0px;
            margin: 0px;

        }

        .div_answer {
            font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;
            font-size: 1.1em;
            color: white;
            background-color: rgb(50, 50, 50);
            border: 1px solid rgb(180, 180, 180);
            width: 100%;
            padding: 10px;
            line-height: 25px;
            padding-left: 80px;
            box-sizing: border-box;
            cursor: text;
        }

        .div_question_wrapper .div_answer {
            display: none;
        }

        .div_question_wrapper:hover .div_answer {
            display: inherit;
        }

        .div_banner {
		    margin: 0;
		}

		.div_banner .banner {
		    background-image: url('{{bagisto_asset("images/helpbanner.jpeg")}}');
		    background-size: cover;
		    border: 0px solid red;
		    height: 300px;
		    margin: 0;
		    padding: 0;
		}

		.div_navigator {
		    background-color: rgba(200, 200, 200, 1);
		    padding: 0;
		    margin: 0;
		}

		.div_navigator .banner-content {
		    border: 0px solid red;
		    width: 100%;
		    display: flex;
		    flex-direction: row;
		    padding: 0;
		    margin: 0;
		    align-items: center;
		    justify-content: center;


		}

		.div_navigator .banner-content li {
		    padding-left: 25px;
		    padding-right: 25px;
		    text-decoration: none;
		    list-style-type: none;
		    border: 0px solid green;
		    padding-top: 15px;
		    padding-bottom: 15px;
		    line-height: 120%;
		}

		.div_navigator .banner-content li a {

		    text-decoration: none;
		    font-family: Verdana, Geneva, Tahoma, sans-serif;
		    font-size: 20px;
		    font-weight: 500%;
		    color: gray;
		    transition-duration: 500ms;
		}

		.div_navigator .banner-content li a:hover {

		    color: darkred;
		    transition-duration: 500ms;
		}

		.banner-content {
		    display: flex;
		    flex-flow: row;
		    align-items: center;
		    justify-content: center;
		    flex-wrap: wrap;
		}


</style>
@section('content-wrapper')
<div class="div_banner"><div class="banner"></div></div>
<div class="div_navigator">
        <div class="div_navigator">
            <ul class="banner-content">
                <!-- <li class="active"><a href="about.html">About us</a></li>
                <li><a href="./company-news.html">Our News</a></li>
                <li><a href="./press-room.html">Press Room</a></li>
                <li><a href="./partnership.html">Partnership</a></li>
                <li><a href="./connect-with-us.html">Connect with us</a></li> -->
    	</ul>
	</div>
</div>
    
<div class="div_help_wrapper">
            <h1 class="my_h1">What can we help you with?</h1>
            <input id="txt_search" type="text" placeholder="Search here" class="txt_search" />

            <div class="div_question my_center">Question Category</div>
            <p class="my_para">And more special</p>

            <div class="div_question_wrapper">
                <div class="div_question">Question 1
                    <p>&times</p>
                </div>
                <div class="div_answer">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quod
                    nemo
                    optio
                    quae aliquam iusto explicabo, commodi illum voluptas quidem natus architecto porro voluptatem
                    vel.
                    Quidem esse nemo reprehenderit unde.</div>
            </div>

            <div class="div_question_wrapper">
                <div class="div_question">Question 1
                    <p>&times</p>
                </div>
                <div class="div_answer">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quod
                    nemo
                    optio
                    quae aliquam iusto explicabo, commodi illum voluptas quidem natus architecto porro voluptatem
                    vel.
                    Quidem esse nemo reprehenderit unde.</div>
            </div>
            <div class="div_question_wrapper">
                <div class="div_question">Question 1
                    <p>&times</p>
                </div>
                <div class="div_answer">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur quod
                    nemo
                    optio
                    quae aliquam iusto explicabo, commodi illum voluptas quidem natus architecto porro voluptatem
                    vel.
                    Quidem esse nemo reprehenderit unde.</div>
            </div>

        </div>
@endsection