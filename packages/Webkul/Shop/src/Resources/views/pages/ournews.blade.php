@extends('shop::layouts.master')
@section('page_title')
{{ $page_title }}
@stop

@section('seo')
<meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
<meta name="keywords" content="{{ $meta_keywords }}"/>
@stop

<style>
    #header .main-container-wrapper {
    max-width: 1300px!important;
    padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }
    .page-news .div_news_container .div_news_item {
        width: 570px !important;
    }
</style>
@section('content-wrapper')
<div class="our-news page-news">
    <div class="div_banner">
        <div class="banner" style="color:red; background-image: url(<?php echo bagisto_asset('images/aboutusbanner-01.png'); ?>)">

        </div>
    </div>
    <div class="div_navigator">
        <ul class="banner-content">
            <li><a href="{{ URL::to('/about') }}">About us</a></li>
            <li  class="active"><a href="javascript:void(0)">Our News</a></li>
            <li><a href="{{ URL::to('/press-rooms') }}">Press Room</a></li>
            <li><a href="{{ URL::to('/partnership') }}">Partnership</a></li>
            <li><a href="{{ URL::to('/connect-with-us') }}">Connect with us</a></li>
        </ul>
    </div>

    <h1 style="font-family:'Roboto';  color: rgb(102,102,102);"> Latest News </h1>

    <!-- news -->
    <div class="div_news_container">
        <!-- items -->
        <div class="div_news_item">
            <div class="div_news_image"
                 style="background-image:url('https://www.mycliks.com/pub/media/wysiwyg/post-1.jpg');background-size: cover; filter: brightness(70%)  ">
            </div>

            <div class="div_news_data">
                <div class="div_line1">
                    <p class="s1">Seller</p> 1/1/2019
                </div>
                <div class="div_line2">
                    Mycliks to Participate at Upcoming Investor Conferences Mycliks to at Upcoming Investor
                    Conferences
                </div>
            </div>

        </div>

        <div class="div_news_item">
            <div class="div_news_image"
                 style="background-image:url('https://www.mycliks.com/pub/media/wysiwyg/post-1.jpg');background-size: cover; filter: brightness(50%)  ">
            </div>

            <div class="div_news_data">
                <div class="div_line1">
                    <p class="s2">Seller</p> 1/1/2019
                </div>
                <div class="div_line2">
                    Mycliks to Participate at Upcoming Investor Conferences Mycliks Conferences
                </div>
            </div>

        </div>

        <div class="div_news_item">
            <div class="div_news_image"
                 style="background-image:url('https://www.mycliks.com/pub/media/wysiwyg/post-1.jpg');background-size: cover; filter: brightness(40%)  ">
            </div>

            <div class="div_news_data">
                <div class="div_line1">
                    <p class="s2">Seller</p> 1/1/2019
                </div>
                <div class="div_line2">
                    Mycliks to Participate at Upcoming Investor Conferences Mycliks to
                </div>
            </div>

        </div>

        <div class="div_news_item">
            <div class="div_news_image"
                 style="background-image:url('https://www.mycliks.com/pub/media/wysiwyg/post-1.jpg');background-size: cover; filter: brightness(40%)  ">
            </div>

            <div class="div_news_data">
                <div class="div_line1">
                    <p class="s2">Seller</p> 1/1/2019
                </div>
                <div class="div_line2">
                    Mycliks to Participate at Upcoming Investor Conferences Mycliks to Conferences
                </div>
            </div>

        </div>

        <div class="div_news_item">
            <div class="div_news_image"
                 style="background-image:url('https://www.mycliks.com/pub/media/wysiwyg/post-1.jpg');background-size: cover; filter: brightness(40%)  ">
            </div>

            <div class="div_news_data">
                <div class="div_line1">
                    <p class="s2">Seller</p> 1/1/2019
                </div>
                <div class="div_line2">
                    Mycliks to Participate at Upcoming Investor Conferences Mycliks to Conferences
                </div>
            </div>

        </div>

        <div class="div_news_item">
            <div class="div_news_image"
                 style="background-image:url('https://www.mycliks.com/pub/media/wysiwyg/post-1.jpg');background-size: cover; filter: brightness(40%)  ">
            </div>

            <div class="div_news_data">
                <div class="div_line1">
                    <p class="s2">Seller</p> 1/1/2019
                </div>
                <div class="div_line2">
                    Mycliks to Participate at Upcoming Investor Conferences Mycliks to Conferences
                </div>
            </div>

        </div>


    </div>
</div>
@endsection
