@extends('shop::layouts.master')
@section('page_title')
{{ $page_title }}
@stop

@section('seo')
<meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
<meta name="keywords" content="{{ $meta_keywords }}"/>
@stop

<style>
    #header .main-container-wrapper {
    max-width: 1300px!important;
    padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }
</style>
@section('content-wrapper')
<div class="our-news page-news">
    <div class="div_banner">
        <div class="banner" style="background-image: url(<?php echo bagisto_asset('images/aboutusbanner-01.png'); ?>)">

        </div>
    </div>
    <div class="div_navigator">
        <div class="div_navigator">
            <ul class="banner-content">
                <li><a href="{{ URL::to('/about') }}">About us</a></li>
                <li><a href="{{ URL::to('/our-news') }}">Our News</a></li>
                <li><a href="{{ URL::to('/press-rooms') }}">Press Room</a></li>
                <li class="active"><a href="javascript:void(0)">Partnership</a></li>
                <li><a href="{{ URL::to('/connect-with-us') }}">Connect with us</a></li>
            </ul>
        </div>
    </div>



    <div class="div_partnership_container">

        <div class="div_complex_form">
            <div>
                <h1>
                    Let's be Friends<br>
                    <h2>Interested in partnering with us?</h2>
                </h1>
            </div>
            <div class="div_form_partnership">
                <input type="text" id="txt_name" placeholder="your name"><br>
                <input type="email" id="txt_email" placeholder="your email">
                <input type="text" id="txt_companyName" placeholder="your company-name">
                <textarea id="txt_details" placeholder="your details"></textarea>
                <a href=".">Send</a>
            </div>
        </div>
    </div>
</div>
@endsection