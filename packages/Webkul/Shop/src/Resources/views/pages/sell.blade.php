@extends('shop::layouts.master')
@section('page_title')
    {{ $page_title }}
@stop

@section('seo')
    <meta name="description" content="{{ str_limit(strip_tags($meta_description), 120, '') }}"/>
    <meta name="keywords" content="{{ $meta_keywords }}"/>
@stop

<style>
    #header .main-container-wrapper {
        max-width: 1300px !important;
        padding: 0 !important;
    }

    .main-container-wrapper {
        padding: 0 !important;
        max-width: 100% !important;
    }

    /* sell styles */
    .div_video_banner {
        position: relative;
        display: block;
        width: 100%;
        height: 500px;
        border: 0px solid red;
        overflow: hidden;
    }

    .div_video_banner_data {
        border: 1px solid black;
        width: 100%;
        height: 100%;
        display: flex;
        flex-flow: column;
        align-items: center;
        justify-content: flex-end;
        z-index: 10;
        background-color: rgba(100, 100, 100, 0.6);
    }

    .div_video_banner_data * {
        border: 0 solid red;
    }

    .div_video_banner .div_video {
        position: absolute;
        width: 100%;
        height: 100%;
        border: 1px solid fuchsia;
        z-index: -10;
    }

    .div_video_banner h1 {
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 40px;
        font-weight: 500;
        line-height: 40px;
        color: black;
        /* text-shadow: 1px 1px 3px black; */
        width: 800px;
        margin: 15px;
    }

    .div_video_banner p {
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 20px;
        font-weight: 500;
        line-height: 40px;
        color: black;
        /* text-shadow: 1px 1px 3px black; */
        width: 800px;
        text-align: center;
        margin: 15px;
    }

    .div_video_banner .btn {
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 20px;
        font-weight: 500;
        line-height: 40px;
        color: white;
        text-decoration: none;
        background-color: dodgerblue;
        margin: 15px;
        padding: 5px 30px;
        text-shadow: 1px 1px 3px black;
        border-radius: 4px;
    }

    .div_video_banner .btn:hover {
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 20px;
        font-weight: 500;
        line-height: 40px;
        color: white;
        text-decoration: none;
        background-color: steelblue;
        margin: 15px;
        padding: 5px 30px;
        text-shadow: 2px 2px 3px black;
    }

    .div_video_banner .div_video_hero_stat {
        border: 0px solid green;
        width: 100%;
        background-color: rgba(150, 150, 150, 0.5);
    }

    .div_video_banner .div_video_hero_stat .cf {
        display: flex;
        flex-flow: row;
        flex-wrap: wrap;
        align-items: stretch;
        justify-content: space-evenly;
        border: 0px dashed red;
        margin: 0;
    }

    .div_video_banner .div_video_hero_stat .cf li {
        padding: 10px;
        margin: 10px;
        text-decoration: none;
        list-style-type: none;
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.4em;
        font-weight: 500;
        line-height: 40px;
        color: black;
        width: 300px;
    }

    /* container1 - styles */
    .div_categs {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items: center;
        justify-content: center;
        width: 100%;
        border: 0px solid red;
        margin-bottom: 20px;
    }

    .div_categs .div_item {
        border: 0 solid red;
        margin: 15px;
        padding: 5px;
    }

    /* how it works - styles */

    .div_howitworks {
        background-color: #8bc34a;
    }

    .div_howitworks>h1, .div_howitworks h3, .div_howitworks p {
        color: white!important;
    }

    .div_howitworks ul {

        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items: flex-start;
        justify-content: center;

        border: 0px solid red;
        padding: 0;
        margin: 0 0 20px;
    }

    .div_howitworks li {
        list-style-type: none;
        border: 0px solid red;
        margin: 5px 75px;
        padding: 5px;
        max-width: 300px;
        text-align: center;
    }

    .div_howitworks li h3 {
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.4em;
        font-weight: 700;
        line-height: 40px;
        color: black;
        text-align: center;
        margin: 8px;
    }

    .div_howitworks li p {
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 1.2em;
        color: black;
        text-align: center;
    }

    /* what it can sell - styles */
    .whaticansell_container {
        background-color: #9E9E9E;
        padding-top: 2em;
        padding-bottom: 2em;
        height: 18em;
    }

    .div_whaticansell {
        width: 60em;
        margin: 0 auto;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        align-items: flex-start;
    }

    .whaticansell_row{
        display: flex;
        flex-direction: column;
    }

    .div_whaticansell .lp {
        width: 28em;
        height: 4em;
        margin: 5px 10px;
        background: #ffffff;
        padding: 1.5em 1em 1.5em 1em;
    }

    .div_whaticansell .lp .panel{
        margin-top: 10px;
        display: none;
        position: absolute;
    }

    .div_whaticansell .lp:hover {
        cursor: pointer;
        border-left: 5px solid #8BC34A;
        z-index: 1;
    }

    .div_whaticansell .lp:hover .panel{
        display: block;
        padding: 10px;
        width: 28em;
        border-radius: 10px;
        line-height: 29px;
        font-weight: 100;
        box-shadow: 5px 10px 8px 10px #3333339c;
    }








    .div_whaticansell ul {
        display: flex;
        flex-direction: column;
        max-width: 1800px;
        align-items: flex-start;
        justify-content: center;
        list-style-type: none;
        margin: 20px;
        padding: 0;
        border: 0px solid red;
        min-width: 400px;
    }

    .div_whaticansell ul li {
        width: 90%;
        margin: 5px;;
        padding: 25px;
        background-color: white;
        border: 0px solid black;
    }

    .div_whaticansell ul li {
        font-size: 1.2em;
        color: black;
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        cursor: pointer;
        box-sizing: content-box;
    }

    .div_whaticansell ul li:hover {
        border-left: 3px solid green;
    }

    .div_whaticansell ul li .panel {
        display: none;
        color: red;
    }
    .div_whaticansell ul li:hover .panel {
        border-left: 3px solid green;
        display: block;
    }
</style>
@section('content-wrapper')
    <div class="div_video_banner">
        <div class="div_video">
            <video src="https://sg.fiverrcdn.com/packages_lp/cover_video.mp4"
                   poster="//assetsv2.fiverrcdn.com/assets/v2_photos/packages-lp/bg-first-hero-663d877e05b0814ef0b07c05910aa645.jpg"
                   autoplay="" loop muted preload="auto">
                <source src="https://sg.fiverrcdn.com/packages_lp/cover_video.mp4" type="video/mp4">
                <source src="https://sg.fiverrcdn.com/packages_lp/cover_video.webm" type="video/webm">
                <source src="https://sg.fiverrcdn.com/packages_lp/cover_video.ogv" type="video/ogv">
            </video>
        </div>
        <div class="div_video_banner_data">
            <h1 class="fadeable fade-in">Become an Entrepreneur and Make Money With Us Now <br></h1>

            <p class="fadeable fade-in">Bring your products, Business and Service here, Absolutely FREE OF FEES </p>

            <a href="{{ URL::to('/customer/register') }}" class="btn" data-seller="header">Become a Seller</a>

            <div class="div_video_hero_stat">
                <ul class="cf">
                    <li>2Million Monthly Users</li>
                    <li>Entrepreneurship Supports</li>
                    <li>100% FREEMIUM Platform</li>
                    <li>Expand Your Network</li>
                </ul>

            </div>
        </div>
    </div>

    <div class="container1"
         style=" border:0px solid red;  font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
        <h2>What You Can SELL in MYCLIKS </h2>
    </div>

    <div class="div_categs">
        <div class="div_item">
            <a href="#"><img src="{{ bagisto_asset('images/i1.jpg') }}"/></a>
        </div>
        <div class="div_item">
            <a href="#"><img src="{{ bagisto_asset('images/i2.jpg') }}"/></a>
        </div>
        <div class="div_item">
            <a href="#"><img src="{{ bagisto_asset('images/i3.jpg') }}"/></a>
        </div>
        <div class="div_item">
            <a href="#"><img src="{{ bagisto_asset('images/i4.jpg') }}"/></a>
        </div>
    </div>

    <div class="div_howitworks">
        <h1 style="font-family: proximanova, helvetica neue, Helvetica, Arial, sans-serif;margin: 5px;padding-top: 15px;">How It Works</h1>
        <ul>
            <li>
                <h3 class="icn-create-gig locale-no-transform">1. Create an Account</h3>
                <p>Sign up for free and begin to offer your products to the world </p>
            </li>
            <li>
                <h3 class="icn-deliver locale-no-transform">2. Deliver The Parcel </h3>
                <p>Get notified immediately upon a purchase of your product and ship it promptly </p>
            </li>
            <li>
                <h3 class="icn-get-paid locale-no-transform">3. Get Paid</h3>
                <p>Get paid on time, once your buyer receive the product.</p>
            </li>
        </ul>

    </div>

    <div class="whaticansell_container">
        <div class="div_whaticansell">
            <div class="whaticansell_row">
                <div class="lp">
                    <header>What can I sell?</header>
                    <div class="panel">You can sell anything and everything from man-made to machine made product within approved categories of products and avoid prohibited and illegal items.
                    </div>
                </div>
                <div class="lp">
                    <header>How much money can I make?</header>
                    <div class="panel">It depends on the product you sell here, remember, service after sales and special promotion does help a lot to retain your customer and to increase your sales. We will guide and help you the best we can.
                    </div>
                </div>
                <div class="lp">
                    <header>How much do I need to pay Mycliks</header>
                    <div class="panel">It’s free to join Mycliks. Currently, we don’t charge you anything nor before or after sales. It’s 100% FREEMIUM for you.
                    </div>
                </div>
            </div>
            <div class="whaticansell_row">
                <div class="lp">
                    <header>How easy to handle my account?</header>
                    <div class="panel">We provide you an easily manageable seller to manage your account, include an immediate notification upon a purchase, sales report. It’s very easy.
                    </div>
                </div>
                <div class="lp">
                    <header>What else mycliks can do to boost my sales?</header>
                    <div class="panel">Mycliks will help you with free marketing, sales tools, networking and there are more features and benefit in the future waiting for you.
                    </div>
                </div>
                <div class="lp">
                    <header>How do I get paid?</header>
                    <div class="panel">Once you complete a buyer’s transaction by delivering the package, you will get paid within 14days.
                    </div>
                </div>
            </div>




            {{--<div class="lp">--}}
                {{--<header>How easy to handle my account?</header>--}}
                {{--<div class="panel">We provide you an easily manageable seller to manage your account, include an--}}
                    {{--immediate--}}
                    {{--notification upon a purchase, sales report. It’s very easy.--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="lp">--}}
                {{--<header>What else mycliks can do to boost my sales?</header>--}}
                {{--<div class="panel">Mycliks will help you with free marketing, sales tools, networking and there--}}
                    {{--are more features and benefit in the future waiting for you.--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="lp">--}}
                {{--<header>How do I get paid?</header>--}}
                {{--<div class="panel">Once you complete a buyer’s transaction by delivering the package, you will get--}}
                    {{--paid within 14days.--}}
                    {{--</div>--}}
            {{--</div>--}}


        </div>
    </div>
@endsection
