@extends('shop::layouts.master')

@section('page_title')
{{ __('shop::app.checkout.onepage.title') }}
@stop

@section('content-wrapper') 

<section class="checkout">   

    <div id="checkout" class="checkout-process">

        <form method="post" action="{{url()->to('/checkout/place_order')}}" id="checkout_formdetails">
            {{ csrf_field() }}
            <div class="full-column usership_address">
                <div class="col-main">
                    @if (!isset(auth()->guard('customer')->user()->id))
                    <div class="payble-amount">
                        <p style="color: red;font-size: 12px;"><i class="fa fa-hand-o-right"></i> Please <a href="{{ url()->to('/customer/login') }}">Login</a> to place order.</p>
                    </div>
                    @endif
                    @guest('customer')
                    <!--<div class="control-group">
                        <a href="{{ url()->to('/customer/login') }}" id="login_users" class="btn btn-lg btn-primary">Sign In</a>
                    </div>-->
                    @endguest

                    <div id="address-section" class="step-content information">                
                        <!----> 
                        <div class="form-container">
                            <div class="form-header">
                                <h1>Billing Address</h1>                                
                            </div>

                            <div class="control-group">
                                <label for="address_firstname">First Name</label> 
                                <input type="text" id="address_firstname" name="address_firstname" class="control" value="@if($customer_data){{$customer_data->first_name}} @endif">
                            </div>

                            <div class="control-group">
                                <label for="address_lastname">Last Name</label> 
                                <input type="text" id="address_lastname" name="address_lastname" class="control" value="@if($customer_data){{$customer_data->last_name}}@endif">
                            </div>

                            <div class="control-group">
                                <label for="address_email">Email</label> 
                                <input type="text" id="address_email" name="address_email" class="control" value="@if($customer_data){{$customer_data->email}}@endif" readonly="readonly">
                            </div>

                            <div class="control-group">
                                <label for="address_lastname">Street Address</label> 
                                <input type="text" id="address_useraddress" name="address_street" class="control" value="@if(isset($customer_data->address1)){{$customer_data->address1}} @endif">
                            </div>

                            <div class="control-group">
                                <label for="address_city">City</label> 
                                <input type="text" id="address_city" name="address_city" class="control" value="@if(isset($customer_data->city)){{$customer_data->city}}@endif">
                            </div>


                            <div class="control-group">
                                <label for="address_state">State</label> 
                                <input type="text" id="address_state" name="address_state" class="control" value="@if(isset($customer_data->state)){{$customer_data->state}}@endif">
                            </div>


                            <div class="control-group">
                                <label for="addresszip">Zip/Postcode</label> 
                                <input type="text" id="addresszip" name="addresszip" class="control" value="@if(isset($customer_data->postcode)){{$customer_data->postcode}}@endif">
                            </div>

                            <div class="control-group">
                                <label for="billing[country]">Country</label> 
                                <select type="text" id="address_country" name="address_country" class="control">
                                    <option value=""></option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AX">Aland Islands</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua &amp; Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AC">Ascension Island</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia</option>
                                    <option value="BA">Bosnia &amp; Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="VG">British Virgin Islands</option>
                                    <option value="BN">Brunei</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="IC">Canary Islands</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="BQ">Caribbean Netherlands</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="EA">Ceuta &amp; Melilla</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo - Brazzaville</option>
                                    <option value="CD">Congo - Kinshasa</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CI">C�te d�Ivoire</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CW">Cura�ao</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czechia</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DG">Diego Garcia</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="EZ">Eurozone</option>
                                    <option value="FK">Falkland Islands</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong SAR China</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IM">Isle of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JE">Jersey</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="XK">Kosovo</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Laos</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macau SAR China</option>
                                    <option value="MK">Macedonia</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia</option>
                                    <option value="MD">Moldova</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar (Burma)</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="KP">North Korea</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PS">Palestinian Territories</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn Islands</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">R�union</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russia</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">S�o Tom� &amp; Pr�ncipe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SX">Sint Maarten</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia &amp; South Sandwich Islands</option>
                                    <option value="KR">South Korea</option>
                                    <option value="SS">South Sudan</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="BL">St. Barth�lemy</option>
                                    <option value="SH">St. Helena</option>
                                    <option value="KN">St. Kitts &amp; Nevis</option>
                                    <option value="LC">St. Lucia</option>
                                    <option value="MF">St. Martin</option>
                                    <option value="PM">St. Pierre &amp; Miquelon</option>
                                    <option value="VC">St. Vincent &amp; Grenadines</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard &amp; Jan Mayen</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syria</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TL">Timor-Leste</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad &amp; Tobago</option>
                                    <option value="TA">Tristan da Cunha</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks &amp; Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UM">U.S. Outlying Islands</option>
                                    <option value="VI">U.S. Virgin Islands</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="UN">United Nations</option>
                                    <option value="US">United States</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VA">Vatican City</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Vietnam</option>
                                    <option value="WF">Wallis &amp; Futuna</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                                <!---->
                            </div>
                            <div class="control-group">
                                <label for="address_phone">Telephone</label> 
                                <input type="text" id="address_phone" name="address_phone" class="control" value="@if(isset($customer_data->phone)){{$customer_data->phone}}@endif">
                            </div>                        
                        </div>              
                    </div>
                </div>

                <div class="col-right">
                    <div class="control-group">
                        <span class="checkbox"><input type="checkbox" id="ship_type" name="ship_type" value="shipping">
                            <label for="ship_type" class="checkbox-view"></label>Ship to this address</span>
                    </div>

                    <div id="address-shipsection" class="step-content information user_shipsection" style="display: none;">                
                        <!----> 
                        <div class="form-container">
                            <div class="form-header">
                                <h1>Shiping Address</h1>                                
                            </div>
                            <div class="control-group">
                                <label for="billing[first_name]">First Name</label> 
                                <input type="text" id="ship_firstname" name="ship_firstname" class="control" value="@if(isset($customer_ship_data->first_name)){{$customer_ship_data->first_name}}@endif">
                            </div>
                            <div class="control-group">
                                <label for="ship_lastname">Last Name</label> 
                                <input type="text" id="billing[last_name]" name="ship_lastname" class="control" value="@if(isset($customer_ship_data->last_name)){{$customer_ship_data->last_name}}@endif">
                            </div>
                            <div class="control-group">
                                <label for="ship_email">Email</label> 
                                <input type="text" id="billing[email]" name="ship_email" class="control" value="@if(isset($customer_ship_data->email)){{$customer_ship_data->email}}@endif">
                            </div>
                            <div class="control-group">
                                <label for="ship_adddress">Street Address</label> 
                                <input type="text" id="ship_adddress" name="ship_adddress" class="control" value="@if(isset($customer_ship_data->address1)){{$customer_ship_data->address1}}@endif">
                            </div>


                            <div class="control-group">
                                <label for="ship_city">City</label> 
                                <input type="text" id="billing[city]" name="ship_city" class="control" value="@if(isset($customer_ship_data->city)){{$customer_ship_data->city}}@endif">
                            </div>

                            <div class="control-group">
                                <label for="ship_state">State</label> 
                                <input type="text" id="ship_state" name="ship_state" class="control" value="@if(isset($customer_ship_data->state)) {{$customer_ship_data->state}} @endif">
                            </div>

                            <div class="control-group">
                                <label for="ship_zip">
                                    Zip/Postcode
                                </label> <input type="text" id="ship_zip" name="ship_zip" class="control" value="@if(isset($customer_ship_data->postcode)){{$customer_ship_data->postcode}}@endif">
                            </div>

                            <div class="control-group">
                                <label for="ship_country">Country</label> 
                                <select type="text" id="ship_country" name="ship_country" class="control">
                                    <option value=""></option>
                                    <option value="AF">Afghanistan</option>
                                    <option value="AX">Aland Islands</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua &amp; Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AC">Ascension Island</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia</option>
                                    <option value="BA">Bosnia &amp; Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="VG">British Virgin Islands</option>
                                    <option value="BN">Brunei</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="IC">Canary Islands</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="BQ">Caribbean Netherlands</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="EA">Ceuta &amp; Melilla</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo - Brazzaville</option>
                                    <option value="CD">Congo - Kinshasa</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CI">C�te d�Ivoire</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CW">Cura�ao</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czechia</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DG">Diego Garcia</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="EZ">Eurozone</option>
                                    <option value="FK">Falkland Islands</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong SAR China</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IM">Isle of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JE">Jersey</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="XK">Kosovo</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Laos</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macau SAR China</option>
                                    <option value="MK">Macedonia</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia</option>
                                    <option value="MD">Moldova</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar (Burma)</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="KP">North Korea</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PS">Palestinian Territories</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn Islands</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">R�union</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russia</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome & Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SX">Sint Maarten</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia &amp; South Sandwich Islands</option>
                                    <option value="KR">South Korea</option>
                                    <option value="SS">South Sudan</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="BL">St. Barthelemy</option>
                                    <option value="SH">St. Helena</option>
                                    <option value="KN">St. Kitts &amp; Nevis</option>
                                    <option value="LC">St. Lucia</option>
                                    <option value="MF">St. Martin</option>
                                    <option value="PM">St. Pierre &amp; Miquelon</option>
                                    <option value="VC">St. Vincent &amp; Grenadines</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard &amp; Jan Mayen</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syria</option>
                                    <option value="TW">Taiwan</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania</option>
                                    <option value="TH">Thailand</option>
                                    <option value="TL">Timor-Leste</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad &amp; Tobago</option>
                                    <option value="TA">Tristan da Cunha</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks &amp; Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UM">U.S. Outlying Islands</option>
                                    <option value="VI">U.S. Virgin Islands</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="UN">United Nations</option>
                                    <option value="US">United States</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VA">Vatican City</option>
                                    <option value="VE">Venezuela</option>
                                    <option value="VN">Vietnam</option>
                                    <option value="WF">Wallis &amp; Futuna</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
                                <!---->
                            </div>
                            <div class="control-group">
                                <label for="ship_phone">Telephone</label> 
                                <input type="text" id="ship_phone" name="ship_phone" class="control" value="@if(isset($customer_ship_data->phone)){{$customer_ship_data->phone}}@endif">
                            </div>                        
                        </div>              
                    </div>
                </div>
            </div>


            <div class="full-column price_checkout">
                <div>
                    <div class="order-summary">
                        <h3>Order Summary</h3>
                        @php
                        $checkout_price=0;
                        $total_shiping_price=0;
                        $c=0;
                        @endphp

                        @if (isset(auth()->guard('customer')->user()->id))
                        @foreach($cart_data as $cart_items)
                        @php
                        $checkout_price+=$cart_items->grand_total;
                        $total_shiping_price+=$cart_items->prd_shipping_charge;
                        @endphp
                        <div class="item-detail">
                            <label>{{$cart_items->name}} X {{$cart_items->items_qty}}</label> 
                            <label class="right">{{$currency}}{{$cart_items->grand_total}}</label>
                            <input type="hidden" name="item_name[]" value="{{$cart_items->name}}"/>
                            <input type="hidden" name="item_qty[]" value="{{$cart_items->items_qty}}"/>
                            <input type="hidden" name="item_price[]" value="{{$cart_items->sub_total}}"/>
                            <input type="hidden" name="item_prdid[]" value="{{$cart_items->prd_id}}"/>
                            <input type="hidden" name="item_sellerid[]" value="{{$cart_items->seller_id}}"/>
                            <input type="hidden" name="item_sku[]" value="{{$cart_items->prd_sku}}"/>
                            <input type="hidden" name="item_currency[]" value="{{$currency}}"/>
                            <input type="hidden" name="item_type[]" value="{{$cart_items->prd_type}}"/>                            
                            <input type="hidden" name="item_shipchargename[]" value="{{$cart_items->prd_shipping_name}}"/>
                            <input type="hidden" name="item_shipchargevalue[]" value="{{$cart_items->prd_shipping_id}}"/>                            
                            @if($cart_items->prd_type=="configurable")
                            <input type="hidden" name="item_var_label[]" value="{{$cart_items->variation_label}}"/>
                            <input type="hidden" name="item_var_value[]" value="{{$cart_items->variation_value}}"/>
                            @endif

                        </div>                        
                        @endforeach

                        @elseif (!isset(auth()->guard('customer')->user()->id) && session('cart_data'))

                        @php
                        $cart_details = session('cart_data');                        
                        @endphp

                        @foreach($cart_details as $session_cart)
                        @php
                        $checkout_price+=$session_cart['grand_total'];                       

                        $total_shiping_price+=$session_cart['shipping_charge'];;

                        @endphp
                        <div class="item-detail">
                            <label>{{$cart_data[$c]->name}} X {{$session_cart['grand_total']}}</label> 
                            <label class="right">{{$currency}}{{$session_cart['grand_total']}}</label>
                            <input type="hidden" name="item_name[]" value="{{$cart_data[$c]->name}}"/>
                            <input type="hidden" name="item_qty[]" value="{{$session_cart['quantity']}}"/>
                            <input type="hidden" name="item_price[]" value="{{$session_cart['sub_total']}}"/>
                            <input type="hidden" name="item_prdid[]" value="{{$session_cart['product_id']}}"/>
                            <input type="hidden" name="item_sellerid[]" value="{{$session_cart['seller_id']}}"/>
                            <input type="hidden" name="item_sku[]" value="{{$session_cart['prd_sku']}}"/>
                            <input type="hidden" name="item_currency[]" value="{{$currency}}"/>
                            <input type="hidden" name="item_type[]" value="{{$session_cart['product_type']}}"/>                            
                            <input type="hidden" name="item_shipchargename[]" value="{{$session_cart['shipping_charged_name']}}"/>
                            <input type="hidden" name="item_shipchargevalue[]" value="{{$session_cart['shipping_charge_id']}}"/>                            
                            @if($session_cart['product_type']=="configurable")
                            <input type="hidden" name="item_var_label[]" value="{{$session_cart['customer_variationlabel']}}"/>
                            <input type="hidden" name="item_var_value[]" value="{{$session_cart['customer_variationvalue']}}"/>
                            @endif

                        </div>
                        @php
                        $c++;
                        @endphp
                        @endforeach

                        @endif
                        <div class="item-detail">
                            <label>Shipping Price Included</label>
                            <label class="right">{{$currency}}{{$total_shiping_price}}</label>
                        </div>

                        <div class="payble-amount"><label>Grand Total</label> <label class="right">{{$currency}}{{$checkout_price}}</label></div>
                    </div>
                </div>
            </div>

            <div class="form-container" payment_type>
                <div class="form-header">
                    <h1>Payment Information</h1>
                </div>
                <div class="payment-methods">
                    <div class="control-group">
                        <span class="radio">
                            <input type="radio" class="paymenttype" id="cashondelivery" name="payment_method" value="cashondelivery" checked="checked"> 
                            <label for="cashondelivery" class="radio-view"></label>
                            Cash On Delivery
                        </span>                         
                    </div>
                    <div class="control-group">
                        <span class="radio">
                            <input type="radio" class="paymenttype" id="eghl" name="payment_method" value="eghl"> 
                            <label for="cashondelivery" class="radio-view"></label>
                            Payment By EGHL
                        </span>                         
                    </div>
                </div>
            </div>
            @if (isset(auth()->guard('customer')->user()->id))            
            <div class="control global-btn">
                <!--<input class="btn_style submit_order" type="submit" name="submit" value="Place Order"/>-->
                <input type="submit" class="btn_style submit_order" value="Place Order"/>
            </div>
            @endif
        </form>

    </div>
</section>

<div id="myModal_light" class="modal-video">  
    <div class="modal-video-body">
        <span class="close_box" class="modal-video-close-btn js-modal-video-dismiss-btn">&times;</span>
        <div id="text" class="auth-content"></div>
    </div>
</div>

@endsection

@push('scripts')
<script>
    $(function () {
        $("#ship_type").click(function () {
            if ($(this).prop("checked") == true) {
                $("#address-shipsection").show();
            } else {
                $("#address-shipsection").hide();
            }
        });



        $("#checkout_formdetails").validate({
            errorElement: "span",
            errorClass: 'error control-error',
            highlight: function (element) {
                $(element).parent().addClass("has-error");
                $(element).addClass('teset');
            },
            unhighlight: function (element) {
                $(element).parent().removeClass("has-error");
            },
            rules: {
                //Delivery Address Validate
                address_firstname: "required",
                address_lastname: "required",
                address_email: {
                    required: true,
                    email: true
                },
                address_street: "required",
                address_city: "required",
                address_state: "required",

                addresszip: {
                    required: true,
                    number: true
                },
                address_phone: {
                    required: true,
                    number: true
                },

                //Shipping Address Validate
                ship_firstname: "required",
                ship_lastname: "required",
                ship_email: {
                    required: true,
                    email: true
                },
                ship_adddress: "required",
                ship_city: "required",
                ship_state: "required",
                ship_zip: {
                    required: true,
                    number: true
                },
                ship_phone: {
                    required: true,
                    number: true
                }


            },

            messages: {
                address_firstname: 'First Name field is required.',
                address_lastname: 'Last Name field is required.',
                address_email: {
                    required: 'Email field is required.',
                    email: 'Please enter valid email address'
                },
                address_street: 'Address field is required.',
                address_city: 'City field is required.',
                address_state: 'State field is required.',

                addresszip: {
                    required: 'Postal Code field is required.',
                    number: 'Postal Code field accept only Numbers',
                },
                address_phone: {
                    required: 'Phone field is required.',
                    number: 'Phone Number field accept only Numbers',
                },

                ship_firstname: 'First Name field is required.',
                ship_lastname: 'Last Name field is required.',
                ship_email: {
                    required: 'Email field is required.',
                    email: 'Please enter valid email address'
                },
                ship_adddress: 'Address field is required.',
                ship_city: 'City field is required.',
                ship_state: 'State field is required.',

                ship_zip: {
                    required: 'Postal Code field is required.',
                    number: 'Postal Code field accept only Numbers',
                },
                ship_phone: {
                    required: 'Phone field is required.',
                    number: 'Phone Number field accept only Numbers',
                }

            },
            submitHandler: function (form) {
                var pay_type = $(".paymenttype:checked").val();
                if (pay_type != "cashondelivery") {
                    
                } else {
                    form.submit();
                }
            }
        });
    });
</script>
@endpush