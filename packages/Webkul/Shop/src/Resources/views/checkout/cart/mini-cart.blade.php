@if (!isset(auth()->guard('customer')->user()->id))


@elseif(isset(auth()->guard('customer')->user()->id))
@php
$session_ip=session('cart_details');
$cart_data = DB::table('cart_detail')
->select(db::raw('DISTINCT `product_inventories`.*'), 'cart_detail.*', 'products_grid.*')
->join('products_grid', 'products_grid.product_id', '=', 'cart_detail.prd_id')
->join('product_inventories', 'product_inventories.product_id', '=', 'cart_detail.prd_id')
->where('product_inventories.vendor_id', "!=", 0);

if (isset(auth()->guard('customer')->user()->id)) {
$customer_id = auth()->guard('customer')->user()->id;
$cart_data->where('cart_detail.customer_id', $customer_id);
}
$cart_result = $cart_data->get();


$currencies = DB::table('currencies')->where('id', 1)->first();
$currency = $currencies->code;
$count_cart=count($cart_result);
$total_sub_price=0;
@endphp

<div class="dropdown-toggle">
    <a class="cart-link" href="{{ route('shop.checkout.cart.index') }}">
        <span class="icon cart-icon"></span>
    </a>

    <span class="name">
        {{ __('shop::app.header.cart') }}
        <span class="count">{{$count_cart}}</span>
    </span>

    <i class="icon arrow-down-icon"></i>
</div>

<div class="dropdown-list" style="display: none; top: 50px; right: 0px;">
    <div class="dropdown-container">
        <div class="dropdown-cart">
            <div class="dropdown-header mt-5">
                <p class="heading">

                </p>
            </div>

            <div class="dropdown-content">



                @foreach($cart_result as $cart_items)
                @php
                $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $cart_items->product_id)->get()->toArray();
                $total_sub_price+=$cart_items->grand_total;
                @endphp
                <div class="item">
                    <div class="item-image">
                        @if($get_image[0]->path!="")
                        <img style="height: 150px; width: 150px;" src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/>
                        @else
                        <img style="height: 150px; width: 150px;" src="{{ url()->to('/') }}/storage/other_images/meduim-product-placeholder.png"/>
                        @endif                         
                    </div>

                    <div class="item-details">
                        @if($cart_items->prd_type=="simple")
                        <div class="item-name">{{$cart_items->name}}</div>
                        @else
                        <div class="item-name">{{$cart_items->name}}-<span class="item-option">{{$cart_items->variation_label}}</span></div>
                        @endif

                        <div class="item-price">{{$currency}}{{$cart_items->grand_total}}</div>
                        <div class="item-qty">Quantity - {{$cart_items->items_qty}}</div>
                    </div>
                </div>


                @endforeach
            </div>

            <div class="dropdown-header mt-5"><p class="heading">Cart Subtotal -<b>{{$currency}} {{$total_sub_price}}</b></p></div>

            <div class="dropdown-footer global-btn">
                <a href="{{ url()->to('/checkout/cart') }}">View Cart</a>

                <a class="btn_style btn-mincart" style="color: white;" href="{{ url()->to('/checkout/onepage') }}">Checkout</a>
            </div>
        </div>
    </div>
</div>

@else

<div class="dropdown-toggle">
    <div style="display: inline-block; cursor: pointer;">
        <span class="icon cart-icon"></span>
        <span class="name">Cart <span class="count"> 0</span></span>
    </div>
</div>
@endif