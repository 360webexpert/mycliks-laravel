@extends('shop::layouts.master')

@section('page_title')
{{ __('shop::app.checkout.cart.title') }}
@stop

@section('content-wrapper')    
<section class="cart">        
    <div class="title">
        {{ __('shop::app.checkout.cart.title') }}
    </div>

    @php
    $cart_total=0;
    $ship_price=0;
    $i=1;
    $p=0;
    $count_cartitems=count($cart_result);
    $out_stock=0;
    @endphp



    @if($count_cartitems==0)
    <div class="cart-content">
        <p>Your cart has been empty. <a href="{{url('/')}}">Shop Now</a></p>
    </div>
    @else
        <div class="cart-content" id="cart_content">
            
        @if(!isset(auth()->guard('customer')->user()->id))
            
            @php        
            $session_data=session('cart_data');
            //echo "<pre>";
            //print_r(session('cart_data'));
            //echo "</pre>";
            $n=0;
            @endphp
            <div class="cart-details">
                <div class="left-side">
                    <table border="1">
                        <thead>
                            <tr>                        
                                <th style="width: 16%;">Image</th>
                                <th style="width: 16%;">Product</th>
                                <th style="width: 16%;">Price</th>
                                <th style="width: 16%;">Shipping</th>
                                <th style="width: 16%;">Qty</th>
                                <th style="width: 16%;">Total</th>
                                <th>Remove</th>
                            </tr>
                        </thead>

                        <tbody>                        
                            @foreach($cart_result as $cart_items)
                            @php
                            $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $cart_items->product_id)->get()->toArray();
                            $get_slug = DB::table('product_flat')->select('url_key')->where('product_id', '=', $cart_items->product_id)->get()->toArray();

                            if($session_data[$n]['product_type']!="simple"):
                            $variation_value=$cart_items->variation_value;                        
                            $explode_value=explode(',',$variation_value);
                            $search_implode=implode(', ', $explode_value);
                            $get_variableprd_stock = DB::table('product_attr_data')->select('product_attr_data_stock')->where('product_id', '=', $cart_items->product_id)->where('product_attr_data_detail', '=', $search_implode)->first();                        
                            endif;
                            @endphp
                            <tr class="product_row" id="tablerow_{{$session_data[$n]['product_id']}}">  
                                @if($get_image[0]->path!="")
                                <td><img style="height: 150px; width: 150px;" src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/></td>
                                @else
                                <td><img style="height: 150px; width: 150px;" src="{{ url()->to('/') }}/storage/other_images/meduim-product-placeholder.png"/></td>
                                @endif                            

                                @if($session_data[$n]['product_type']=="simple")
                                <td><a href="{{ url()->to('/') }}/products/{{$get_slug[0]->url_key}}">{{$session_data[$n]['prd_name']}}</a> @if($cart_items->qty!=0)<span class="green_stock">IN Stock</span>@else @php $out_stock=1; @endphp <span class="red_stock">Out Of Stock</span> @endif</td>
                                @else
                                <td><a href="{{ url()->to('/') }}/products/{{$get_slug[0]->url_key}}">{{$cart_items->name}}- {{$cart_items->variation_label}}</a> @if($get_variableprd_stock->product_attr_data_stock!=0)<span class="green_stock">IN Stock</span>@else @php $out_stock=1; @endphp <span class="red_stock">Out Of Stock</span> @endif</td>
                                @endif
                                <td style="text-align: center;"><span class="currency">{{$currency}}</span><span class="price">{{$session_data[$n]['sub_total']}}</span></td>
                                <td style="text-align: center;"><span class="currency">{{$currency}}</span><span class="price">@if($session_data[$n]['shipping_charge']=="")0.00 @else {{$session_data[$n]['shipping_charge']}} @endif</span></td>
                                <td style="text-align: center;">
                                    <input type="number" class="qty_expect" id="{{$session_data[$n]['product_id']}}" value="{{$session_data[$n]['quantity']}}" min="1" max="20"/>
                                    <input type="hidden" class="prddata_type" id="prdtype_{{$session_data[$n]['product_id']}}" value="{{$session_data[$n]['product_type']}}"/>
                                    <input type="hidden" class="prddata_shipprice" id="prd_shipprice_{{$session_data[$n]['product_id']}}" value="{{$session_data[$n]['shipping_charge']}}"/>
                                    <input type="hidden" class="prddata_price" id="prd_price_{{$session_data[$n]['product_id']}}" value="{{$session_data[$n]['sub_total']}}"/>

                                </td>
                                <td style="text-align: center;"><span class="currency">{{$currency}}</span><span id="finalprc_{{$session_data[$n]['product_id']}}" class="price">{{$session_data[$n]['grand_total']}}</span></td>
                                <td style="text-align: center;"><a class="remove_items" id="{{$session_data[$n]['product_id']}}" href="javascript:void(0)"><i class="fa fa-times"></i></a></td>
                            </tr>       
                            @php
                            $i++;
                            $n++;
                            @endphp
                            @endforeach
                            <tr style="text-align: right;">
                                <td colspan="7" class="actions" style="padding: 10px;">
                                    <div class="div_buy_add"><a href="javascript:void(0)" class="btn_buy cart-btn update_cart">Update cart</a></div>
                                </td>
                            </tr>                        
                        </tbody>
                    </table>
                </div>


                <div class="right-side">
                    <div class="order-summary">
                        <h3>Order Summary</h3>
                        @php
                        $p=0;
                        $ship_price=0;
                        $cart_total=0;
                        @endphp
                        
                        @foreach($cart_result as $cart_details)
                        @php 
                        $product_title= substr($cart_details->name, 0, 12);
                        $prd_title_length=strlen($cart_details->name);
                        
                        if($session_data[$p]['shipping_charge']!=0){
                            $ship_price+=$session_data[$p]['shipping_charge'];
                        }
                        //$ship_price+=0.00;
                        $cart_total+=$session_data[$p]['grand_total'];
                        
                        @endphp
                        <div class="item-detail" id="card_items_{{$session_data[$p]['product_id']}}">
                            <label><span id="rightprdqty_{{$session_data[$p]['product_id']}}">{{$session_data[$p]['quantity']}}</span> <span>X</span> @if($prd_title_length>12){{$product_title}}...@else {{$product_title}} @endif</label> 
                            <label class="right" id="product_{{$session_data[$p]['product_id']}}"><span class="currency">{{$currency}}</span><span class="prd_final_price" id="rightfinalprc_{{$session_data[$p]['product_id']}}">{{$session_data[$p]['grand_total']}}</span></label>
                        </div>

                        @php
                        $p++;
                        @endphp
                        @endforeach

                        <div class="item-detail">
                            <label>Total shipping Included</label> 
                            <label class="right">{{$currency}}{{$ship_price}}</label>
                        </div>

                        <div class="payble-amount">
                            <label>Grand Total</label> 
                            <label class="right" id="grand_final_result"><span class="currency">{{$currency}}</span><span class="last_result">{{sprintf('%0.2f', $cart_total)}}</span></label>
                        </div>

                        @if($out_stock==1)
                        <div class="payble-amount">
                            <p style="color: red;font-size: 12px;"><i class="fa fa-hand-o-right"></i> Please Remove out of stock items from cart items.</p>
                        </div>
                        @endif

                        @if($out_stock!=1)              
                        <div class="proceed_checkout global-btn">
                            <a class="btn_style proceed-checkout"  href="javascript:void(0)">Proceed To Checkout</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            
        @else  
        
            <div class="cart-details">
                <div class="left-side">
                    <table border="1">
                        <thead>
                            <tr>                        
                                <th style="width: 16%;">Image</th>
                                <th style="width: 16%;">Product</th>
                                <th style="width: 16%;">Price</th>
                                <th style="width: 16%;">Shipping</th>
                                <th style="width: 16%;">Qty</th>
                                <th style="width: 16%;">Total</th>
                                <th>Remove</th>
                            </tr>
                        </thead>

                        <tbody>                      
                            @foreach($cart_result as $cart_items)
                            @php
                            $get_image = DB::table('product_images')->select('path')->where('product_id', '=', $cart_items->product_id)->get()->toArray();
                            $get_slug = DB::table('product_flat')->select('url_key')->where('product_id', '=', $cart_items->product_id)->get()->toArray();
                            if($cart_items->prd_type!="simple"):
                            $variation_value=$cart_items->variation_value;                        
                            $explode_value=explode(',',$variation_value);
                            $search_implode=implode(', ', $explode_value);
                            $get_variableprd_stock = DB::table('product_attr_data')->select('product_attr_data_stock')->where('product_id', '=', $cart_items->product_id)->where('product_attr_data_detail', '=', $search_implode)->first();                        
                            endif;
                            @endphp
                            <tr class="product_row" id="tablerow_{{$cart_items->cart_id}}">  
                                @if($get_image[0]->path!="")
                                <td><img style="height: 150px; width: 150px;" src="{{ url()->to('/') }}/storage/{{$get_image[0]->path}}"/></td>
                                @else
                                <td><img style="height: 150px; width: 150px;" src="{{ url()->to('/') }}/storage/other_images/meduim-product-placeholder.png"/></td>
                                @endif                            

                                @if($cart_items->prd_type=="simple")
                                <td><a href="{{ url()->to('/') }}/products/{{$get_slug[0]->url_key}}">{{$cart_items->name}}</a> @if($cart_items->qty!=0)<span class="green_stock">IN Stock</span>@else @php $out_stock=1; @endphp <span class="red_stock">Out Of Stock</span> @endif</td>
                                @else
                                <td><a href="{{ url()->to('/') }}/products/{{$get_slug[0]->url_key}}">{{$cart_items->name}}- {{$cart_items->variation_label}}</a> @if($get_variableprd_stock->product_attr_data_stock!=0)<span class="green_stock">IN Stock</span>@else @php $out_stock=1; @endphp <span class="red_stock">Out Of Stock</span> @endif</td>
                                @endif
                                <td style="text-align: center;"><span class="currency">{{$currency}}</span><span class="price">{{$cart_items->sub_total}}</span></td>
                                <td style="text-align: center;"><span class="currency">{{$currency}}</span><span class="price">{{$cart_items->prd_shipping_charge}}</span></td>
                                <td style="text-align: center;">
                                    <input type="number" class="qty_expect" id="{{$cart_items->cart_id}}" value="{{$cart_items->items_qty}}" min="1" max="20"/>
                                    <input type="hidden" class="prddata_type" id="prdtype_{{$cart_items->cart_id}}" value="{{$cart_items->prd_type}}"/>
                                    <input type="hidden" class="prddata_shipprice" id="prd_shipprice_{{$cart_items->cart_id}}" value="{{$cart_items->prd_shipping_charge}}"/>
                                    <input type="hidden" class="prddata_price" id="prd_price_{{$cart_items->cart_id}}" value="{{$cart_items->sub_total}}"/>

                                </td>
                                <td style="text-align: center;"><span class="currency">{{$currency}}</span><span id="finalprc_{{$cart_items->cart_id}}" class="price">{{$cart_items->grand_total}}</span></td>
                                <td style="text-align: center;"><a class="remove_items" id="{{$cart_items->cart_id}}" href="javascript:void(0)"><i class="fa fa-times"></i></a></td>
                            </tr>       
                            @php
                            $i++;
                            @endphp
                            @endforeach
                            <tr style="text-align: right;">
                                <td colspan="7" class="actions" style="padding: 10px;">
                                    <div class="div_buy_add"><a href="javascript:void(0)" class="btn_buy cart-btn update_cart">Update cart</a></div>
                                </td>
                            </tr>
                        </tbody>                
                    </table>
                </div>

                <div class="right-side">
                    <div class="order-summary">
                        <h3>Order Summary</h3>

                        @foreach($cart_result as $cart_details)
                        @php 
                        $product_title= substr($cart_details->name, 0, 12);
                        $prd_title_length=strlen($cart_details->name);
                        $ship_price+=$cart_details->prd_shipping_charge;
                        $cart_total+=$cart_details->grand_total;
                        @endphp
                        <div class="item-detail" id="card_items_{{$cart_details->cart_id}}">
                            <label><span id="rightprdqty_{{$cart_details->cart_id}}">{{$cart_details->items_qty}}</span> <span>X</span> @if($prd_title_length>12){{$product_title}}...@else {{$product_title}} @endif</label> 
                            <label class="right" id="product_{{$cart_details->cart_id}}"><span class="currency">{{$currency}}</span><span class="prd_final_price" id="rightfinalprc_{{$cart_details->cart_id}}">{{$cart_details->grand_total}}</span></label>
                        </div>
                        @endforeach

                        <div class="item-detail">
                            <label>Total shipping Included</label> 
                            <label class="right">{{$currency}}{{$ship_price}}</label>
                        </div>

                        <div class="payble-amount">
                            <label>Grand Total</label> 
                            <label class="right" id="grand_final_result"><span class="currency">{{$currency}}</span><span class="last_result">{{$cart_total}}</span></label>
                        </div>

                        @if($out_stock==1)
                        <div class="payble-amount">
                            <p style="color: red;font-size: 12px;"><i class="fa fa-hand-o-right"></i> Please Remove out of stock items from cart items.</p>
                        </div>
                        @endif

                        @if($out_stock!=1)              
                        <div class="proceed_checkout global-btn">
                            <a class="btn_style proceed-checkout"  href="javascript:void(0)">Proceed To Checkout</a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>            
        @endif
        <div class="cart_spinner" style="display: none;">
                <i class="fa fa-spinner fa-spin" style="font-size:50px"></i>
            </div>
        </div>
    @endif    
</section>

@endsection

@push('scripts')
<script>
    $(function () {
        $("body").on("click", ".left-side table a.update_cart", function () {
            $("#cart_content").addClass("cart_processing");
            $(".cart_spinner").show();
            $(".qty_expect").each(function () {
                var getqty = $(this).val();
                var get_attrid = $(this).attr('id');
                var get_price = $("#prd_price_" + get_attrid).val();
                var get_shipprc = $("#prd_shipprice_" + get_attrid).val();
                if (get_shipprc == "") {
                    get_shipprc = 0;
                }

                var subtotal = get_price * getqty;
                var grand_total = parseFloat(subtotal) + parseFloat(get_shipprc);
                var final_result = grand_total.toFixed(2);
                $("#finalprc_" + get_attrid).html(final_result);
                $("#rightfinalprc_" + get_attrid).html(final_result);
                $("#rightprdqty_" + get_attrid).html(getqty);
            });

            var grand_sub_price = 0;
            $(".prd_final_price").each(function () {
                var getprdprc = $(this).text();
                grand_sub_price += parseFloat(getprdprc);
            });
            var grand_price = grand_sub_price.toFixed(2);
            $("#grand_final_result .last_result").html(grand_price);

            setTimeout(function () {
                $("#cart_content").removeClass("cart_processing");
                $(".cart_spinner").hide();
            }, 400);
        });

        $(".proceed-checkout").click(function () {
            var total_prd = $(".product_row").length;
            if (total_prd != 0) {
                $(this).addClass("disabled").html('Wait <i class="fa fa-refresh fa-spin" style="font-size: 11px;"></i>');
                $(this).prop("disabled", true);
                var qty_arr = [];
                var cartid = [];
                var cartprc = [];
                var ship_price = [];
                $(".qty_expect").each(function () {
                    var get_qty = $(this).val();
                    var getcartid = $(this).attr('id');
                    qty_arr.push(get_qty);
                    cartid.push(getcartid);
                });

                $(".prd_final_price").each(function () {
                    var get_prc = $(this).text();
                    cartprc.push(get_prc);
                });

                $.ajax({
                    url: "<?php echo url('/checkout/updatecart'); ?>",
                    type: 'POST',
                    data: {"_token": "{{ csrf_token() }}", "cart_uid": cartid, "cart_qty": qty_arr, "cart_price": cartprc},
                    success: function (resp) {                        
                        $(".proceed-checkout").removeClass("disabled").html('Proceed To Checkout');
                        $(".proceed-checkout").removeAttr("disabled");
                        window.location.href = "{{ url()->to('/checkout/onepage') }}";
                    }, error: function () {
                        $(".proceed-checkout").removeClass("disabled").html('Proceed To Checkout');
                        $(".proceed-checkout").removeAttr("disabled");
                        alert("Something Went wrong, Please try again after some time");
                    }
                });
            } else {
                alert("Something went wrong");
            }
        });


        //Remove cart Item through Ajax
        $(".remove_items").click(function () {
            var get_remove_id = $(this).attr('id');
            if (get_remove_id != "") {
                if (confirm('Are you sure you want to delete this item')) {
                    $.ajax({
                        url: "<?php echo url('/checkout/removecart'); ?>",
                        type: 'POST',
                        data: {"_token": "{{ csrf_token() }}", "cart_removeid": get_remove_id},
                        success: function (resp) {
                            console.log(resp);
                            $(".left-side tr#tablerow_" + get_remove_id).remove();
                            $(".order-summary div#card_items_" + get_remove_id).remove();
                            $(".left-side table a.update_cart").trigger("click");
                        }, error: function () {
                            alert("Please try again latter");
                        }
                    });
                }
            }
        });

    });
</script>
@endpush