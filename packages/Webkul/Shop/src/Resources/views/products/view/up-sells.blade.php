{!! view_render_event('bagisto.shop.products.view.up-sells.after', ['product' => $product]) !!}



@if ($product->up_sells()->count())
    <div class="attached-products-wrapper common_slider_class">

        <div class="title">Customers Who Viewd This Also Viewed<span class="border-bottom"></span></div>

        <div class="product-grid-rel up-sells product_slider" id="relative_prd">

            @foreach ($product->up_sells()->paginate(12) as $up_sell_product)

                @include ('shop::products.list.card', ['product' => $up_sell_product])

            @endforeach

        </div>

    </div>
@endif

{!! view_render_event('bagisto.shop.products.view.up-sells.after', ['product' => $product]) !!}