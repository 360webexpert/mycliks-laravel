@if ($product->related_products()->count())
    <div class="attached-products-wrapper common_slider_class">

        <div class="title">Sponsored Products Related To This Item<span class="border-bottom"></span></div>

        <div class="product-grid-rel rel-product product_slider" id="relative_prd">

            @foreach ($product->related_products()->paginate(12) as $related_product)

                @include ('shop::products.list.card', ['product' => $related_product])

            @endforeach

        </div>

    </div>
@endif