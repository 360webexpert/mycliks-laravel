@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
<?php 
$images = $productImageHelper->getGalleryImages($product); 
?>

{!! view_render_event('bagisto.shop.products.view.gallery.before', ['product' => $product]) !!}

<div class="product-image-group" style="width: 100%; position: relative;">
    <div id="loader" class="cp-spinner cp-round" style="display: none; z-index:9;"></div>
    <div class="product_image_detail">
        <div class="thumb-list">         
            @foreach($images as $prdimg)
            <div class="thumb-frame active">
                <img src="{{$prdimg['original_image_url']}}">
            </div>
            @endforeach        
        </div>
        <div id="product-hero-image" class="product-hero-image">
            <img src="<?php echo $images[0]['original_image_url']; ?>" id="pro-img" class="block__pic">
        </div>
    </div>
</div>

{!! view_render_event('bagisto.shop.products.view.gallery.after', ['product' => $product]) !!}

@push('scripts')
<script>
    jQuery(function () {
        var get_hero_img = $("#product-hero-image img").attr("src");
        $("body").on("mouseover", ".product_image_detail .slick-slide img", function () {
            var get_src = $(this).attr("src");
            console.log(get_src);
            $("#product-hero-image img").attr('src', get_src);
        });

//        $("body").on("mouseout", ".product_image_detail .slick-slide img", function () {            
//            $("#product-hero-image img").attr('src', get_hero_img);
//        });


        $(window).onbeforeload(function () {
            $("#loader").show();
        });
        $(window).load(function(){
            
            $("#loader").hide();
        });
    });
</script>


@endpush