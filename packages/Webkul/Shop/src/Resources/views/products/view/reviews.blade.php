@inject ('reviewHelper', 'Webkul\Product\Helpers\Review')

{!! view_render_event('bagisto.shop.products.view.reviews.after', ['product' => $product]) !!}

@php
$total = $reviewHelper->getTotalReviews($product);
@endphp

@if ($total)
<div class="rating-reviews">
    <div class="rating-header">
        {{ __('shop::app.products.reviews-title') }}
    </div>

    <div class="overall">
        <div class="review-info">

            <span class="rating_number">
                <p>
                    @for($i=0; $i<5; $i++)
                    @if($i<$get_avgrating)
                    <i  class="fa fa-star" style="color: rgb(241, 141, 24);"></i>
                    @else
                    <i  class="fa fa-star-o" style="color: rgb(241, 141, 24);"></i>
                    @endif
                    @endfor                            
                </p>
                Rating {{$get_avgrating}} out of 5
            </span>

            <span class="stars">
                @for ($i = 1; $i <= round($reviewHelper->getAverageRating($product)); $i++)
                    <i  class="fa fa-star" style="color: rgb(241, 141, 24);"></i>
                @endfor
            </span>

            <div class="total-reviews">
                {{ __('shop::app.products.total-reviews', ['total' => $total]) }}
            </div>

        </div>

        @if (core()->getConfigData('catalog.products.review.guest_review') || auth()->guard('customer')->check())
        <a href="{{ route('shop.reviews.create', $product->url_key) }}" class="btn btn-lg btn-primary">
            {{ __('shop::app.products.write-review-btn') }}
        </a>
        @endif

    </div>

    <div class="reviews">

        @foreach ($reviewHelper->getReviews($product)->paginate(10) as $review)
        <div class="review">
            <div class="title">
                {{ $review->title }}
            </div>

            <span class="stars">
                @for ($i = 1; $i <= $review->rating; $i++)

                <i  class="fa fa-star" style="color: rgb(241, 141, 24);"></i>

                @endfor
            </span>

            <div class="message">
                {{ $review->comment }}
            </div>

            <div class="reviewer-details">
                <span class="by">
                    {{ __('shop::app.products.by', ['name' => $review->name]) }},
                </span>

                <span class="when">
                    {{ core()->formatDate($review->created_at, 'F d, Y') }}
                </span>
            </div>
        </div>
        @endforeach

        @if ($total>10)
        <a href="{{ route('shop.reviews.index', $product->url_key) }}" class="view-all">
            {{ __('shop::app.products.view-all') }}
        </a>
        @endif

    </div>
</div>
@else
@if (core()->getConfigData('catalog.products.review.guest_review') || auth()->guard('customer')->check())
<div class="rating-reviews">
    <p>No Review of this product. Please <a href="{{ route('shop.reviews.create', $product->url_key) }}" class="write-review">{{ __('shop::app.products.write-review-btn') }}</a> of this product.</p>
</div>
@endif
@endif

{!! view_render_event('bagisto.shop.products.view.reviews.after', ['product' => $product]) !!}