@if ($product->cross_sells()->count())
    <div class="attached-products-wrapper common_slider_class">

        <div class="title">Customers Who Bought This Also Bought</div>

        <div class="product-grid-rel cross-sells product_slider" id="relative_prd">

            @foreach ($product->cross_sells()->paginate(12) as $up_sell_product)

                @include ('shop::products.list.card', ['product' => $up_sell_product])

            @endforeach

        </div>

    </div>
@endif
