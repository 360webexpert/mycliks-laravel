@extends('shop::layouts.master')
<style type="text/css">
    .category{
        margin-left: 2%;
    }

    .price_text{
        font-size: large;
    }

    .cut{
        font-size: smaller;
    }

    .shipping-div{
        background: #efebeb;
        padding: 3%;
        width: 60%;
    }

    .cart-div{
        margin-top: 25px;
        margin-bottom: 25px;
    }



    .buynow{
        border-style: double;
        background-color: orangered;
        border-width: 2px;
        border-color: orangered;
        padding: 8px 20px 8px 20px;
        border-radius: 5px;
        color: white !important;
    }

    .wish{
        color: black !important;
    }

    .tab_border{
        border-style: none !important;
        width: 165px !important;
    }

    .chat-now{
        padding: 8px 23px 8px 23px;
        background-color: white;
        border-width: 2px !important;
        border-style: double;
        border-color: #0a8cca !important;
        border-radius: 8px;
        color: #0a8cca;
    }
    .visit_store{
        background: blue;
        color: white !important;
        padding: 8px 23px 8px 23px;
        border-width: 2px !important;
        border-style: double;
        border-color: #0a8cca !important;
        border-radius: 8px;
    }

    .follow{
        background: transparent;
        /* color: white !important; */
        padding: 8px 23px 8px 23px;
        border-width: 2px !important;
        border-style: double;
        border-color: #000000 !important;
        border-radius: 8px;
    }

    .share_img{
        width: 25px;
    }
    .thumblist{
        border-style: solid;
        border-width: 1px;
        border-color: #b2b6b9;
    }

    .main-image{
        border-style: solid;
        border-width: 1px;
        border-color: #b2b6b9;
        margin-left: 10px;
    }
</style>
@section('page_title')
{{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
<meta name="description" content="{{ trim($product->meta_description) != "" ? $product->meta_description : str_limit(strip_tags($product->description), 120, '') }}"/>
<meta name="keywords" content="{{ $product->meta_keywords }}"/>
@stop

@section('content-wrapper')

{!! view_render_event('bagisto.shop.products.view.before', ['product' => $product]) !!}


<section class="product-detail">
    <div class="div_content">
        <div class="col-md-12" >
            <div class="category">

            </div>
        </div>
        <div class="div_product_images col-5 col-s-12">
            @include ('shop::products.view.gallery')
        </div>
        <div class="div_product_desc col-4 col-s-12">
            <div style="padding-left:5px">
                <div class="div_title">

                    <h2 class="p1 m1 ">{{$product['name']}}</h2>

                    <div class="div_title_stat">
                        <!--<p style="color:rgb(223,70,80);">6 product last sold</p>-->
                        @php
                        $count=count($product_review);
                        $total_rat=0;
                        $prd_rat=0;
                        @endphp
                        @if($count!=0)

                        @php
                        foreach($rating_average as $avg_rating):
                        $total_rat += $avg_rating->count_rating*$avg_rating->rating;
                        $prd_rat += $avg_rating->rating;
                        endforeach;

                        $get_avgrating= number_format($total_rat/$prd_rat);

                        @endphp
                        <span class="rating_number">
                            <p>
                                @for($i=0; $i<5; $i++)
                                @if($i<$get_avgrating)
                                <i  class="fa fa-star" style="color: rgb(241, 141, 24);"></i>
                                @else
                                <i  class="fa fa-star-o" style="color: rgb(241, 141, 24);"></i>
                                @endif
                                @endfor
                            </p>
                            Rating {{$get_avgrating}} out of 5
                        </span>
                        <p style="color:dodgerblue;font-size: 12px;font-weight: 1;"><a href="#review">{{$count}} Reviews</a></p>
                        @endif
                    </div>
                    <!--                     <hr style="width: 90%; text-align: center;" color="rgb(204,204,204)"> -->
                </div>

                @if($product['type']=="simple")
                <div class="div_save_details">
                    @if($product['special_price']!="")
                    @php

                    $sale_price=$product['price'];
                    $original_price=$product['special_price'];

                    $diff_price=$sale_price-$original_price;
                    $devide_price=$diff_price/$sale_price;
                    $percentage_price=$devide_price*100;
                    $get_percentage=number_format($percentage_price);
                    $per = $product['price']/$product['special_price'];
                    $per  = $per * 100;
                    $per=number_format($per,2);

                    @endphp

<!--                     <p>List Price: <span><s><span>{{$currency}}</span>{{$product['price']}}</s></span></p>

                    <p> Price: <span style="color:firebrick;"><span>{{$currency}}</span><span class="prd_price" id="{{$product['special_price']}}">{{$product['special_price']}}</span></span></p>


                    <p>You Save: <span style="color:firebrick;"><span>{{$currency}}</span>{{$diff_price}} ({{$get_percentage}}%)</span></p> -->

                    <p><span class="price_text"><span>{{$currency}}</span><span class="prd_price" id="{{$product['price']}}">{{$product['price']}}</span></span></p>

                    <p><del><span class="cut"><span>{{$currency}}</span><span class="prd_price" id="{{$product['special_price']}}">{{$product['special_price']}}</span></span></del></p>

                    <p><span style="color:firebrick;">{{$per}}% Off</span></p>
                    @else
                    <p><span class="price_text"><span>{{$currency}}</span><span class="prd_price" id="{{$product['price']}}">{{$product['price']}}</span></span></p>
                    @endif

                    <ul>
                        @if($product_flat[0]->color_label!="")
                        <li>
                            <p>Color: {{$product_flat[0]->color_label}}</p>
                        </li>
                        @endif

                        @if($product_flat[0]->size_label!="")
                        <li>
                            <p>Size: {{$product_flat[0]->size_label}}</p>
                        </li>
                        @endif
                    </ul>

                    @if($product['inventories'][0]->qty!="0")
                    <input class="check_simple_stock" type="hidden" value="{{$product['inventories'][0]->qty}}"/>
                    @else
                    <input class="check_simple_stock" type="hidden" value="nostock"/>
                    @endif
                </div>

                @elseif($click_prdtype=="admin_cnf")
                <?php
                $ko = 0;
                ?>

                @foreach($prd_attr_data as $boxes)
                @php

                $admin_attr_option = DB::table('attribute_options')
                ->leftjoin('product_attribute_values', 'product_attribute_values.integer_value','=','attribute_options.id')
                ->where('product_attribute_values.attribute_id', $boxes->id)
                ->whereIn('product_attribute_values.product_id', $ad_prdattrid)
                ->get()
                ->toarray();
                @endphp

                <select id="{{strtolower($boxes->admin_name)}} {{strtolower($boxes->admin_name)}}_{{$ko}}" class="form-control form-select">
                    <option value="-1">{{ucfirst($boxes->admin_name)}}</option>
                    @foreach($admin_attr_option as $admin_variation)
                    <option value="-1">{{$admin_variation->admin_name}}</option>
                    @endforeach
                </select>

                @php
                $ko++;

                @endphp
                @endforeach

                @else

                <div class="div_save_details configurable_product">
                    <?php
                    $prd_attr_text = $attr_selected[0]->selected_attr_text;
                    $prd_attr_val = $attr_selected[0]->selected_attr_val;
                    $explode_attr_text = explode(',', $prd_attr_text);
                    $explode_attr_value = explode(',', $prd_attr_val);
                    $count_select = count($explode_attr_value);
                    $i = 0;
                    ?>

                    @foreach($explode_attr_text as $total_selectbox)
                    <select id="{{strtolower($total_selectbox)}} {{strtolower($total_selectbox)}}_{{$explode_attr_value[$i]}}" class="form-control form-select">
                        <option value="-1">{{ucfirst($total_selectbox)}}</option>
                        @foreach($all_attr_option as $get_option)
                        @if($get_option->attribute_id==$explode_attr_value[$i])
                        <option value="{{$get_option->id}}">{{$get_option->admin_name}}</option>
                        @endif
                        @endforeach
                    </select>
                    @php
                    $i++;
                    @endphp
                    @endforeach
                    <input type="hidden" value="{{$count_select}}" id="count_selected"/>
                    <input type="hidden" value="{{$product['id']}}" id="product_conf_id"/>
                    <p id="variation_price" style="display: none;"></p>
                </div>
                @endif
            </div>

            <div class="col-md-12">
                <h5>Quantity : 1</h5>
                <h5>Limit 1 Unit per user</h5>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12">

                  @if($product_flat[0]->delivery_from!="")
                    <div class="shipping-div">
                      <p><i class="fa fa-truck"></i> Delivery From {{$product_flat[0]->delivery_from}}</p>
                    </div>
                  @endif

                  @if($product_flat[0]->estimated_arrival!="")
                    <div class="shipping-div">
                      <p><i class="fa fa-truck"></i> Estimated Arrival : <b>{{$product_flat[0]->estimated_arrival}} working days</b></p>
                    </div>
                  @endif

            </div>

            <div class="clearfix"></div>

            <!--<div class="col-md-12">
                <div class="cart-div">
                    <a href="#" class="addtocart">ADD TO CART</a>
                    <a href="#" class="buynow">BUY NOW</a>
                </div>
            </div>-->


            @php
            $count_shipment=count($shipping_available);
            $j=1;
            @endphp
            <div class="user_shipping"><!--
                <h3>Select Shipping Method: </h3>
                @if($count_shipment!=0)
                <ul class="available_shipment">
                    @foreach($shipping_available as $all_shipment)
                    <li>
                        <label for="mycats_{{$j}}">
                            <input id="mycats_{{$j}}" type="radio" name="ship_selected" value="{{$all_shipment->shipping_id}}" data-title="{{$all_shipment->prd_ship_name}}" data-value="{{$all_shipment->prd_ship_price}}" class="cats_selected">{{$all_shipment->prd_ship_name}}[<span class="ship_price"><span>{{$currency}}</span>{{$all_shipment->prd_ship_price}}</span>]
                        </label></li>
                    @php $j++; @endphp
                    @endforeach
                </ul>
                @else
                <p class="green_message">No Shipping Fee.</p>
                @endif
                --></div>

            <div class="final_result" style="display: none">
                <p id="shipping_prd_price"></p>
                <p id="shipping_price"></p>
                <p id="final_price"></p>
            </div>

            @if($product['type']=="simple")
            @if($product['inventories'][0]->qty==0)
            <div class="out_stock">
                <p class="red no_stock">Out of stock</p>
            </div>
            @endif
            @elseif($product['type']=="configurable" && $click_prdtype=="vendor_cnf")
            <div class="out_stock" id="configurable_stock" style="display: none;">
                <p class="red no_stock">Out of stock</p>
            </div>
            @endif



            <div class="div_buy_add col-12" style="display: none;">
                <div class="col-md-12">
                    <div class="cart-div">
                        <form action="{{URL::to('/')}}/checkout/cart/add/{{$product['id']}}" method="POST">
                            @csrf
                            <input type="hidden" name="product" value="{{$product['id']}}">
                            <input type="hidden" name="product_name" value="{{$product['name']}}-@if($product_flat[0]->color_label!=''){{$product_flat[0]->color_label}}@endif,@if($product_flat[0]->size_label!=''){{$product_flat[0]->size_label}}@endif">
                            <input type="hidden" name="quantity" value="1">

                            @if($product['type']=="configurable")
                            <div class="configurable_boxes">
                                <input type="hidden" name="select_textlabel" id="select_textlabel"/>
                                <input type="hidden" name="select_valuelabel" id="select_valuelabel"/>
                            </div>
                            @endif

                            <input type="hidden" value="{{$product['type']}}" name="is_configurable">
                            <input type="hidden" name="selected_prdprice" id="prdprice"/>
                            <input type="hidden" name="shipping_charge" id="shipping_charge"/>
                            <input type="hidden" name="shipping_charge_id" id="shipping_charge_id"/>
                            <input type="hidden" name="shipping_charged_name" id="shipping_charged_name"/>
                            <input type="hidden" name="last_prdprice" id="last_prdprice"/>
                            <input type="hidden" name="store_currency" value="{{$currency}}"/>
                            <input type="hidden" name="store_sellerid" value="@if(isset($seller_info->id)){{$seller_info->id}}@else 1 @endif"/>
                            <input type="hidden" name="prd_sku" value="{{$product['sku']}}"/>
                            <button class="btn_buy cart-btn addtocart"><i class="fa fa-shopping-cart"></i> Add To Cart</button>
                        </form>

                        <span>
                          @php
                          $count_wishlist=count($whislist_product);
                          @endphp

                          <!-- 1 For Add and 2 for remove wishlist -->
                          @if(isset($customer_details->id))
                            @if($count_wishlist!=0)
                            <button id="mywishlist" class="btn_buy cart-btn addtowishlist" onclick="addToWishlist(2, {{$product['id']}})"><i class="fa fa-shopping-cart"></i> Remove to wishlist</button>
                            @else
                            <button id="mywishlist" class="btn_buy cart-btn addtowishlist" onclick="addToWishlist(1, {{$product['id']}})"><i class="fa fa-shopping-cart"></i> Add to wishlist</button>
                            @endif
                          @else
                            <button id="mywishlist" class="btn_buy cart-btn addtowishlist" onclick="addToWishlist(-1, 0)"><i class="fa fa-shopping-cart"></i> Add to wishlist</button>
                          @endif
                        </span>
                    </div>
                </div>
            </div>

            <?php /*
            <div class="wishlist">
                @php
                $count_wishlist=count($whislist_product);
                @endphp

                <!-- 1 For Add and 2 for remove wishlist -->
                @if(isset($customer_details->id))
                @if($count_wishlist!=0)
                <a href='javascript:void(0)' class="mywishlist wish" data-id="2" data-prd-id="{{$product['id']}}" title="Remove to wishlist"><i class="fa fa-heart"></i> Remove to wishlist</a>
                @else
                <a href='javascript:void(0)' class="mywishlist wish" data-id="1" data-prd-id="{{$product['id']}}" title="Add to wishlist"><i class="fa fa-heart-o"></i> Add to wishlist</a>
                @endif
                @else
                <a href='javascript:void(0)' class="mywishlist wish" data-id="-1" title="Add to wishlist"><i class="fa fa-heart-o"></i> Add to wishlist</a>
                @endif
            </div>
            */ ?>


            <div class=div_container_social><!--
                <p style="color:indigo; font-size: 20px;">Fullfilled by myclicks</p>
                <div class="div_social" style="display:flex;">
                    <a href="" class="ssk ssk-facebook">
                        <p class="btn_blue"> <i class="fa fa-facebook"></i>Share</p>
                    </a>
                    <a href="" class="ssk ssk-tumblr">
                        <p class="btn_blue"> <i class="fa fa-tumblr"></i>Share</p>
                    </a>
                    <a href="" class="ssk ssk-facebook">
                        <p class="btn_blue"> <i class="ssk ssk-twitter"></i>Tweet</p>
                    </a>
                    <a href="" class="ssk ssk-pinterest">
                        <p class="btn_blue"> <i class="fa fa-pinterest"></i>Save</p>
                    </a>

                </div>
                --></div>
            <div class="div_shipping"><!--
                <?php echo html_entity_decode($product['short_description']); ?>
                <table class="tbl">
                    <tbody>
                        <tr>
                            <td>
                                <p>Payments</p>
                            </td>
                            <td>
                                <img src="https://ir.ebaystatic.com/rs/v/adpmys5sve3vzjkvw4zbdptxqio.png" />

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>Returns</p>
                            </td>
                            <td>
                                <p> 30 days Returns, buyers pay for return shipping <span><a href="#"><u>see
                                                details</u></a></span></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                --></div>
        </div>

        <div class="div_product_extra col-3">
            <div class="div_shop_confidence">
                <div class="div_card" style="background-color: #e4e4e4">
                    <p class="center">
                      <img src="{{ bagisto_asset('images/default-store-logo.png') }}">
                    </p>
                    {{-- <p class="center" style="font-size: 11px;">
                      Since 21 Sep 2014
                    </p>
                    <p class="center" style="font-size: 11px;">
                      Last active 17 hours ago
                    </p> --}}




                    <div class="div_item">
                        <!--                         <div class="div_left_side">
                                                    <img src="{{ bagisto_asset('images/bullet.jpg') }}" class="img" />
                                                </div> -->
                        <!-- <div class="div_right_side">
                            <p class="m1">Top Rated Plus</p>
                            <p class="m1 noweight" style="color:rgb(82,82,82);">Trusted seller fast shipping rusted
                                seller fast shipping <span><a href="#"><u>Learn more</u></a></span></p>
                        </div> -->
                        <div class="chat_buttons">
                            <button class="chat-now"><i class="fa fa-comments"></i> Chat Now</button>
                            <button class="visit_store">Visit Store</button>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                    <div class="div_item">
                        <button class="follow center"><i style="color: orangered" class="fa fa-user" aria-hidden="true"></i> Follow (0)</button>
                        <!-- <div class="div_left_side">
                            <img src="{{ bagisto_asset('images/bullet.jpg') }}" class="img" />
                        </div> -->
                        <!-- <div class="div_right_side">
                            <p class="m1">Mycliks Money Back Guarantee</p>
                            <p class="m1 noweight" style="color:rgb(82,82,82);">Trusted seller fast shipping rusted
                                seller fast shipping <span><a href="#"><u>Learn more</u></a></span></p>
                        </div> -->
                    </div>

                    <div class="div_item">
                        <h5 style="margin-bottom: 0;">We Accept</h5>
                    </div>
                    <div class="div_item">
                      <img src="{{ bagisto_asset('images/Payment_Channel.png') }}" width="100%">
                    </div>
                    <div class="div_item">
                      <img src="{{ bagisto_asset('images/buyer_protection.png') }}" >
                    </div>
                    <div class="div_item">
                        <input type="checkbox" name="refund"><span style="font-size: 12px;">Full Refund if you don't Receive your order</span>
                    </div>


                </div>

                @php
                $count_seller_info=count($seller_info);
                @endphp
                @if($count_seller_info!=0)
                <!-- <div class="div_card">
                    <div class="div_header">
                        <p class="center">Seller Information</p>
                        <p>Name: <span>{{$seller_info->first_name}} {{$seller_info->last_name}}</span></p>
                        @if($seller_info->email!="")

                        @endif
                        @if($seller_info->phone!="")

                        @endif
                    </div>

                    @if($seller_info->url!="" && $seller_info->shop_title!="")
                    <ul>
                        <li>
                            <p>Visit Store: <span><a href="{{ url()->to('/') }}/marketplace/seller/profile/{{$seller_info->url}}">{{$seller_info->shop_title}}</a></span></p>
                        </li>
                    </ul>
                    @endif

                    <ul class="social_link">
                        @if($seller_info->facebook!="")
                        <li><a target="_blank" href="{{$seller_info->facebook}}"><i class="fa fa-facebook"></i></a></li>
                        @endif

                        @if($seller_info->twitter!="")
                        <li><a target="_blank" href="{{$seller_info->twitter}}"><i class="fa fa-twitter"></i></a></li>
                        @endif

                        @if($seller_info->youtube!="")
                        <li><a target="_blank" href="{{$seller_info->facebook}}"><i class="fa fa-youtube"></i></a></li>
                        @endif

                        @if($seller_info->instagram!="")
                        <li><a target="_blank" href="{{$seller_info->instagram}}"><i class="fa fa-instagram"></i></a></li>
                        @endif

                        @if($seller_info->skype!="")
                        <li><a target="_blank" href="{{$seller_info->skype}}"></a><i class="fa fa-skype"></i></li>
                        @endif

                        @if($seller_info->linked_in!="")
                        <li><a target="_blank" href="{{$seller_info->linked_in}}"><i class="fa fa-linkedin"></i></a></li>
                        @endif

                        @if($seller_info->pinterest!="")
                        <li><a target="_blank" href="{{$seller_info->pinterest}}"><i class="fa fa-pinterest"></i></a></li>
                        @endif
                    </ul>
                </div> -->
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="share">
            <p style="display: inline;">Share To</p>
            <div class="share_btns" style="display: inline;"><a href=""><img class="share_img" src="{{url('storage/facebook.png')}}"></a>
                <a href=""><img class="share_img" src="{{url('storage/whatsapp.png')}}"></a>
                <a href=""><img class="share_img" src="{{url('storage/twitter.png')}}"></a>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="col-md-12">

        <a href="javascript:void(0)" class="single-tabs active-tab" data-id="desc">
            <p class="btn_tab tab_border">Product Description</p>
        </a>

        <a href="javascript:void(0)" class="single-tabs" id="review" data-id="review">
            <p class="btn_tab tab_border">Reviews (0)</p>
        </a>

        <a href="javascript:void(0)" class="single-tabs" data-id="ship">
            <p class="btn_tab tab_border">Q&A</p>
        </a>

        <a href="javascript:void(0)" class="single-tabs" data-id="policy">
            <p class="btn_tab tab_border">Policy</p>
        </a>

    </div>

    <div class="div_2 col-12">
        <div class="div_content tab_content" id="tab_desc">
            <?php echo html_entity_decode($product['description']); ?>
        </div>

        <div class="div_content tab_content" id="tab_ship" style="display: none">
            <h3>Available Shipment Price</h3>
            @if($count_shipment!=0)
            <ul class="available_shipment">
                @foreach($shipping_available as $all_shipment)
                <li>{{$all_shipment->prd_ship_name}}: <span class="ship_price"><span>{{$currency}}</span>{{$all_shipment->prd_ship_price}}</span></li>
                @endforeach
            </ul>
            @else
            <p>No Shipping Fee.</p>
            @endif
        </div>

        <div class="div_content tab_content" id="tab_review" style="display: none">
            @guest('customer')
            <p style="color:red;">Please <a href="{{URL::to('/')}}/customer/login">Login</a> and give rating us</p>
            @endguest
            @include ('shop::products.view.reviews')
        </div>
    </div>


    <div class="div_content">
        <div class="div_products_row" style="width: 100%; display: block;">
            @include ('shop::products.view.related-products')
        </div>

        <div class="div_products_row" style="width: 100%; display: block;">
            @include ('shop::products.view.up-sells')
        </div>

        <div class="div_products_row" style="width: 100%; display: block;">
            @include ('shop::products.view.cross-sells-single')
        </div>
    </div>
</section>

{!! view_render_event('bagisto.shop.products.view.after', ['product' => $product]) !!}
@endsection



@push('scripts')
<script>
    jQuery(function ($) {
        $(".single-tabs").click(function () {
            $(".single-tabs").removeClass("active-tab");
            $(this).addClass("active-tab");
            $(".tab_content").hide();
            var get_id = $(this).attr('data-id');
            $("#tab_" + get_id).show();
        });

        $(".block__pic").imagezoomsl({
            zoomrange: [3, 3]
        });

        addToWishlist(check_login_oraction, product_id){
          if (check_login_oraction == -1) {
              alert("Please login to add wishlist");
          } else {
              var product_id = product_id;
              $.ajax({
                  url: "<?php echo url('/products/wishlist'); ?>",
                  type: 'POST',
                  data: {"_token": "{{ csrf_token() }}", "prd_id": product_id, 'user_action': check_login_oraction},
                  success: function (resp) {
                      if (resp == "added") {
                          $("#mywishlist").html('<i class="fa fa-heart"></i> Remove to wishlist');
                      } else {
                          $("#mywishlist").html('<i class="fa fa-heart-o"></i> Add to wishlist');
                      }
                  }, error: function () {
                      alert("Server connectivity failed");
                  }
              });
          }
        }

        // $(".wishlist a.mywishlist").click(function () {
        //     var check_login_oraction = $(this).attr('data-id');
        //     if (check_login_oraction == -1) {
        //         alert("Please login to add wishlist");
        //     } else {
        //         var product_id = $(this).attr('data-prd-id');
        //         $.ajax({
        //             url: "<?php echo url('/products/wishlist'); ?>",
        //             type: 'POST',
        //             data: {"_token": "{{ csrf_token() }}", "prd_id": product_id, 'user_action': check_login_oraction},
        //             success: function (resp) {
        //                 if (resp == "added") {
        //                     $(".wishlist a.mywishlist").html('<i class="fa fa-heart"></i> Remove to wishlist');
        //                 } else {
        //                     $(".wishlist a.mywishlist").html('<i class="fa fa-heart-o"></i> Add to wishlist');
        //                 }
        //             }, error: function () {
        //                 alert("Server connectivity failed");
        //             }
        //         });
        //     }
        // });
    });
</script>


@if($product['type']=="simple" && $count_shipment!=0)
<script>
    $(function () {
        $(".available_shipment label input").click(function () {
            var prdprice = $(".prd_price").attr('id');
            var get_shiping_charge = $(this).attr('data-value');
            var get_shipping_name = $(this).attr('data-title');
            var get_shiping_id = $(this).val();
            var final_price = parseFloat(prdprice) + parseFloat(get_shiping_charge);
            var check_stock = $(".check_simple_stock").val();
            $(".prd_price").text(final_price.toFixed(2));

            $(".final_result").show();
            $("#shipping_prd_price").html("Product Price: <span>{{$currency}}</span><span class='prd_price'>" + prdprice + "</span>");
            $("#shipping_price").html("Product Price: <span>{{$currency}}</span><span class='prd_price'>" + get_shiping_charge + "</span>");
            $("#final_price").html("Cart Price: <span>{{$currency}}</span><span class='prd_price'>" + final_price + "</span>");
            $("#prdprice").val(prdprice);
            $("#shipping_charge").val(get_shiping_charge);
            $("#shipping_charged_name").val(get_shipping_name);
            $("#shipping_charge_id").val(get_shiping_id);
            $("#last_prdprice").val(final_price);
            if (check_stock != "nostock") {
                $(".div_buy_add").show();
            } else {
                $(".div_buy_add").hide();
            }
        });
    });
</script>

@elseif($product['type']=="simple" && $count_shipment==0)
<script>
    $(function () {
        var get_prdprice = $(".prd_price").attr('id');
        var check_stock = $(".check_simple_stock").val();
        $("#last_prdprice").val(get_prdprice);
        $("#prdprice").val(get_prdprice);
        if (check_stock != "nostock") {
            $(".div_buy_add").show();
        } else {
            $(".div_buy_add").hide();
        }
    });
</script>
@elseif($product['type']=="configurable" && $count_shipment!=0)
<script>
    $(function () {
        var all_select = $("#count_selected").val();

        $(".configurable_product select").change(function () {
            var cnfprd_id = $("#product_conf_id").val();
            var myarray = [];
            var myreturn = 0;
            $(".configurable_product select").each(function () {
                var get_val = $(this).val();
                if (get_val != -1) {
                    myarray.push(get_val);
                }
            });
            if (myarray.length == all_select) {
                $.ajax({
                    url: "<?php echo url('/products/variationprice'); ?>",
                    type: 'POST',
                    data: {"_token": "{{ csrf_token() }}", "users_selected": myarray, "cnfprd_id": cnfprd_id},
                    success: function (resp) {
                        if (resp == "error") {
                            $(".div_buy_add").hide();
                            $("#variation_price").removeClass("success_msg").addClass("error_msg").show().css("color", "red").html("Sorry, no products matched your selection. Please choose a different combination!");
                        } else if (resp == "stock_problem") {
                            $(".div_buy_add").hide();
                            $("#configurable_stock").show();
                            $("#variation_price").removeClass("success_msg").addClass("error_stock").show().css("color", "red").html("Out of stock");
                        } else {
                            $("#configurable_stock").hide();
                            $("#variation_price").removeClass("success_msg").addClass("success_msg").show().css("color", "green").html("<span>{{$currency}}</span><span class='prd_price' id='" + resp + "'>" + resp + "</span>");

                            if ($(".available_shipment label input").is(":checked")) {
                                var prdprice = resp;
                                var get_shiping_charge = $(".available_shipment label input:checked").attr('data-value');
                                var get_shipping_name = $(".available_shipment label input:checked").attr('data-title');
                                var get_shiping_id = $(".available_shipment label input:checked").val();
                                cart_price(prdprice, get_shiping_charge, get_shipping_name, get_shiping_id);
                            }
                        }
                    }, error: function () {
                        alert("Failed");
                    }
                });
            } else {
                $(".div_buy_add").hide();
                $("#variation_price").hide();
            }
        });


        $(".available_shipment label input").click(function () {
            if ($("#variation_price").hasClass("success_msg")) {
                var prdprice = $("#variation_price > .prd_price").attr('id');
                var get_shiping_charge = $(this).attr('data-value');
                var get_shipping_name = $(this).attr('data-title');
                var get_shiping_id = $(this).val();
                cart_price(prdprice, get_shiping_charge, get_shipping_name, get_shiping_id);
            }
        });

        function cart_price(prdprice, get_shiping_charge, get_shipping_name, get_shiping_id) {
            var final_price = parseFloat(prdprice) + parseFloat(get_shiping_charge);
            $("#variation_price .prd_price").attr('id', final_price.toFixed(2));
            $("#variation_price .prd_price").text(final_price.toFixed(2));
            $(".final_result").show();
            $("#shipping_prd_price").html("Product Price: <span>{{$currency}}</span><span class='prd_price'>" + prdprice + "</span>");
            $("#shipping_price").html("Product Price: <span>{{$currency}}</span><span class='prd_price'>" + get_shiping_charge + "</span>");
            $("#final_price").html("Cart Price: <span>{{$currency}}</span><span class='prd_price'>" + final_price.toFixed(2) + "</span>");
            $("#prdprice").val(prdprice);
            $("#shipping_charge").val(get_shiping_charge);
            $("#shipping_charged_name").val(get_shipping_name);
            $("#shipping_charge_id").val(get_shiping_id);
            $("#last_prdprice").val(final_price);
            $(".div_buy_add").show();
        }


        $(".div_buy_add form button.cart-btn").click(function (event) {
            event.preventDefault();
            var myarray_val = [];
            var myarray_text = [];
            $(".configurable_product select option:selected").each(function () {
                var get_val = $(this).val();
                var get_text = $(this).text();
                myarray_val.push(get_val);
                myarray_text.push(get_text);
            });
            $("#select_textlabel").val(myarray_text);
            $("#select_valuelabel").val(myarray_val);
            $(".div_buy_add form").submit();
        });
    });
</script>


@else

<script>
    $(function () {
        var all_select = $("#count_selected").val();

        $(".configurable_product select").change(function () {
            var cnfprd_id = $("#product_conf_id").val();
            var myarray = [];
            var myreturn = 0;
            $(".configurable_product select").each(function () {
                var get_val = $(this).val();
                if (get_val != -1) {
                    myarray.push(get_val);
                }
            });
            if (myarray.length == all_select) {
                $.ajax({
                    url: "<?php echo url('/products/variationprice'); ?>",
                    type: 'POST',
                    data: {"_token": "{{ csrf_token() }}", "users_selected": myarray, "cnfprd_id": cnfprd_id},
                    success: function (resp) {
                        if (resp == "error") {
                            $(".div_buy_add").hide();
                            $("#variation_price").removeClass("success_msg").addClass("error_msg").show().css("color", "red").html("Sorry, no products matched your selection. Please choose a different combination!");
                        } else if (resp == "stock_problem") {
                            $(".div_buy_add").hide();
                            $("#configurable_stock").show();
                            $("#variation_price").removeClass("success_msg").addClass("error_stock").show().css("color", "red").html("Out of stock");
                        } else {
                            $("#configurable_stock").hide();
                            $("#variation_price").removeClass("success_msg").addClass("success_msg").show().css("color", "green").html("<span>{{$currency}}</span><span class='prd_price' id='" + resp + "'>" + resp + "</span>");
                            $(".div_buy_add").show();
                        }
                    }, error: function () {
                        alert("Failed");
                    }
                });
            } else {
                $(".div_buy_add").hide();
                $("#variation_price").hide();
            }
        });

        $(".div_buy_add form button.cart-btn").click(function (event) {
            event.preventDefault();
            var prdprice = $(".prd_price").attr('id');
            var myarray_val = [];
            var myarray_text = [];
            $(".configurable_product select option:selected").each(function () {
                var get_val = $(this).val();
                var get_text = $(this).text();
                myarray_val.push(get_val);
                myarray_text.push(get_text);
            });
            $("#select_textlabel").val(myarray_text);
            $("#select_valuelabel").val(myarray_val);
            $("#last_prdprice").val(prdprice);
            $("#prdprice").val(prdprice);
            $(".div_buy_add form").submit();
        });
    });
</script>
@endif

@endpush
