<div class="footer">
    <!-- 
            <div class="list-container">
              
                    <ul class="social-footer">
<li><a href="https://www.facebook.com/mycliks.official/" target="_blank"><i class="icon-facebook">test</i></a></li>
<li><a href="https://twitter.com/mycliks_com" target="_blank"><i class="icon-twitter"></i></a></li>
<li><a href="https://www.instagram.com/mycliks_official/" target="_blank"><i class="icon-instagram">test</i></a></li>
<li><a href="https://www.youtube.com/channel/UCmQdtm4rv4zmhWPr8fmFAEg" target="_blank"><i class="icon-youtube">test</i></a></li>
<li><a href="https://in.pinterest.com/myclikscom/" target="_blank"><i class="icon-pinterest">test</i></a></li>
<li><a href="#" target="_blank"><i class="icon-google-plus"></i></a></li>
<li><a href="https://www.linkedin.com/company/mycliks-com/" target="_blank"><i class="icon-linkedin">test</i></a></li>
</ul>
                
            </div> -->
    <div class="footer-content">
        <div class="footer-list-container">
            <div class="list-container footer-logo">
                <a href="{{url()->to('/')}}">
                    <img src="{{url()->to('/public/themes/default/assets/images/logo.png')}}" class="logo">
                </a>
            </div>
            @if (count($categories))
            <div class="list-container">
                <span class="list-heading">Categories</span>
                <ul class="list-group">
                    @foreach ($categories as $key => $category)
                    <li>
                        <a href="{{ route('shop.categories.index', $category->slug) }}">{{ $category->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif

            {!! DbView::make(core()->getCurrentChannel())->field('footer_content')->render() !!}
        </div>
    </div>
    <!--     <div class="list-container">
                <span class="list-heading">{{ __('shop::app.footer.subscribe-newsletter') }}</span>
                <div class="form-container">
                    <form action="{{ route('shop.subscribe') }}">
                        <div class="control-group" :class="[errors.has('subscriber_email') ? 'has-error' : '']">
                            <input type="email" class="control subscribe-field" name="subscriber_email" placeholder="Email Address" required><br/>

                            <button class="btn btn-md btn-primary">{{ __('shop::app.subscription.subscribe') }}</button>
                        </div>
                    </form>
                </div>

                <span class="list-heading">{{ __('shop::app.footer.locale') }}</span>
                <div class="form-container">
                    <div class="control-group">
                        <select class="control locale-switcher" onchange="window.location.href = this.value">

                            @foreach (core()->getCurrentChannel()->locales as $locale)
                                <option value="?locale={{ $locale->code }}" {{ $locale->code == app()->getLocale() ? 'selected' : '' }}>{{ $locale->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="currency">
                    <span class="list-heading">{{ __('shop::app.footer.currency') }}</span>
                    <div class="form-container">
                        <div class="control-group">
                            <select class="control locale-switcher" onchange="window.location.href = this.value">

                                @foreach (core()->getCurrentChannel()->currencies as $currency)
                                    <option value="?currency={{ $currency->code }}" {{ $currency->code == core()->getCurrentCurrencyCode() ? 'selected' : '' }}>{{ $currency->code }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>

            </div> -->
</div>
@push('scripts')
<script src="{{ bagisto_asset('js/slick.min.js') }}"></script>
<script src="{{ bagisto_asset('js/custom.js') }}"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{ bagisto_asset('js/zoomsl.js') }}"></script>
<script src="{{ bagisto_asset('js/social-share-kit.js') }}"></script>

<script>
function initSlider() {
    $('.thumb-list').slick({
        vertical: true,
        verticalSwiping: true,
        infinite: true,
        slidesToShow: 5,
        arrows: true,
        slidesToScroll: 1
    });
    
    $('.product_slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        prevArrow: "<div class='lft-btn'><i class='fa fa-angle-left'></i></div>",
        nextArrow: "<div class='rgt-btn'><i class='fa fa-angle-right'></div>",
        arrows: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}
jQuery(function () {
    initSlider();
    jQuery('#saller_table').DataTable();
});

</script>


<script>

    // Init Social Share Kit
    SocialShareKit.init({
        url: '{{url()->to("/products/test-configurations")}}',
        twitter: {
            title: 'Sparx Womens Red Flip-Flops and House Slippers - 5',
            via: 'MyCliks'
        }
    });
    SocialShareKit.init({
        reinitialize: true,
        url: '{{url()->to("/products/test-configurations")}}',
        twitter: {
            title: 'Sparx Womens Red Flip-Flops and House Slippers - 5',
            via: 'socialsharekit'
        }
    });

    $(function () {

        // Just to disable href for other example icons
        $('.ssk').on('click', function (e) {
            e.preventDefault();
        });

        // Navigation collapse on click
        $('.navbar-collapse ul li a:not(.dropdown-toggle)').bind('click', function () {
            $('.navbar-toggle:visible').click();
        });

        // Email protection
        $('.author-email').each(function () {
            var a = '@', em = 'support' + a + 'social' + 'sharekit' + '.com', t = $(this);
            t.attr('href', 'mai' + 'lt' + 'o' + ':' + em);
            !t.text() && t.text(em);
        });

        // Sticky icons changer
        $('.sticky-changer').click(function (e) {
            e.preventDefault();
            var $link = $(this);
            $('.ssk-sticky').removeClass($link.parent().children().map(function () {
                return $(this).data('cls');
            }).get().join(' ')).addClass($link.data('cls'));
            $link.parent().find('.active').removeClass('active');
            $link.addClass('active').blur();
        });
    });

    // This code is required if you want to use Twitter callbacks
    window.twttr = (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function (f) {
            t._e.push(f);
        };

        return t;
    }(document, "script", "twitter-wjs"));

    // Demo callback
    function twitterDemoCallback(e) {
        $('#twitterEvents').append(e.type + ' ');
    }

    // Bind to Twitter events
    twttr.ready(function (tw) {
        tw.events.bind('click', twitterDemoCallback);
        tw.events.bind('tweet', twitterDemoCallback);
    });


</script>
@endpush