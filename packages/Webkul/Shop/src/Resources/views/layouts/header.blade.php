<div class="wrapper">
    <div class="header" id="header">

        <div class="top-header">
            <div class="main-container-wrapper">
                <div class="top-header-inner">
                    <ul class="logo-container">
                        <li>
                            <a href="{{ route('shop.home.index') }}">
                                @if ($logo = core()->getCurrentChannel()->logo_url)
                                <img class="logo" src="{{ $logo }}" />
                                @else
                                <img class="logo" src="{{ bagisto_asset('images/logo.png') }}" />
                                @endif
                            </a>
                        </li>

                        <li><a href="{{ URL::to('/help') }}">Help</a></li>
                        <li><a href="{{ URL::to('/sell') }}" style="color: rgb(36, 36, 36);">Sell</a></li>
                        <!-- {!! view_render_event('bagisto.shop.layout.header.currency-item.before') !!} -->

                        @if (core()->getCurrentChannel()->currencies->count() > 1)
                        <li class="currency-switcher">
                            <span class="dropdown-toggle">
                                {{ core()->getCurrentCurrencyCode() }}

                                <i class="icon arrow-down-icon"></i>
                            </span>

                            <ul class="dropdown-list currency">
                                @foreach (core()->getCurrentChannel()->currencies as $currency)
                                <li>
                                    <a href="?currency={{ $currency->code }}">{{ $currency->code }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @endif
                        {!! view_render_event('bagisto.shop.layout.header.currency-item.after') !!}
                        {!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}

                    </ul>

                    <div class="left-navigation-top">
                        <ul>

                        </ul>
                    </div>
                    <div class="right-navigation-top">                        
                        <ul>
                            @guest('customer')
                            <li><a class="btn-sm" href="{{ route('customer.session.index') }}">Login</a></li>
                            <li><a class="btn-sm" href="{{ route('customer.register.index') }}">{{ __('shop::app.header.sign-up') }}</a></li>
                            @endguest


                            @auth('customer')
                            <li>
                                <a href="{{ route('customer.profile.index') }}">{{ __('shop::app.header.profile') }}</a>
                            </li>
                            <li>
                                <a href="{{ route('customer.session.destroy') }}">{{ __('shop::app.header.logout') }}</a>
                            </li>
                            {!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}
                            {!! view_render_event('bagisto.shop.layout.header.cart-item.before') !!}
                            @endauth
                            <li class="cart-wishlist">
                                <a href="{{ route('customer.wishlist.index') }}" title="{{ __('shop::app.header.wishlist') }}"><i class="fa fa-heart"></i></a>
                            </li>
                            <li class="cart-dropdown-container dropdown-open">
                                @include('shop::checkout.cart.mini-cart')
                            </li>
                            @auth('customer')
                            <li><a class="customer_name" href="javascript:void(0)">Welcome: {{ auth()->guard('customer')->user()->first_name }}</a></li>
                            @endauth
                            {!! view_render_event('bagisto.shop.layout.header.cart-item.after') !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="header-top">
            <div class="main-container-wrapper">
                <div class="header-top-inner">                    
                    <div class="left-content">
                        <div class="search-offer">
                            <ul class="search-container">
                                <li class="search-group">
                                    <form role="search" action="{{ route('shop.search.index') }}" method="GET" style="display: inherit;">
                                        <input type="search" name="term" class="search-field" placeholder="{{ __('shop::app.header.search-text') }}" required>
                                        @php
                                        $count_cats=count($categories);
                                        @endphp
                                        @if($count_cats!=0)
                                        <!--<select name="search_cats" class="search_cats">
                                            <option>All Departments</option>
                                            @foreach($categories as $cats)
                                            <option value="{{$cats->slug}}">{{$cats->name}}</option> 
                                            @endforeach
                                        </select> -->
                                        @endif
                                        <div class="search-icon-wrapper">
                                            <button class="" class="background: none;">Search</button>
                                        </div>
                                    </form>
                                </li>
                            </ul>

                            <ul class="offer-links">
                                <li>Just Landed</li>
                                <li>Best Takelot</li>
                                <li>Easter</li>
                                <li>New Electronics</li>
                                <li>Shop By Brand</li>
                                <li>Clearance</li>
                                <li>Daily deals</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
