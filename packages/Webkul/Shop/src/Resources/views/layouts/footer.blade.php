<div class="footer">
    <!-- 
            <div class="list-container">
              
                    <ul class="social-footer">
<li><a href="https://www.facebook.com/mycliks.official/" target="_blank"><i class="icon-facebook">test</i></a></li>
<li><a href="https://twitter.com/mycliks_com" target="_blank"><i class="icon-twitter"></i></a></li>
<li><a href="https://www.instagram.com/mycliks_official/" target="_blank"><i class="icon-instagram">test</i></a></li>
<li><a href="https://www.youtube.com/channel/UCmQdtm4rv4zmhWPr8fmFAEg" target="_blank"><i class="icon-youtube">test</i></a></li>
<li><a href="https://in.pinterest.com/myclikscom/" target="_blank"><i class="icon-pinterest">test</i></a></li>
<li><a href="#" target="_blank"><i class="icon-google-plus"></i></a></li>
<li><a href="https://www.linkedin.com/company/mycliks-com/" target="_blank"><i class="icon-linkedin">test</i></a></li>
</ul>
                
            </div> -->
    <div class="footer-content">
        <div class="footer-list-container">
            <div class="list-container footer-logo">
                <a href="{{ route('shop.home.index') }}">
                    <img src="{{ bagisto_asset('images/logo.png') }}" class="logo">
                </a>
            </div>
            @if (count($categories))
            <div class="list-container">
                <span class="list-heading">Categories</span>

                <ul class="list-group">
                    @foreach ($categories as $key => $category)
                    <li>
                        <a href="{{ route('shop.categories.index', $category->slug) }}">{{ $category->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            {!! DbView::make(core()->getCurrentChannel())->field('footer_content')->render() !!}
        </div>
    </div>
    <!--     <div class="list-container">
                <span class="list-heading">{{ __('shop::app.footer.subscribe-newsletter') }}</span>
                <div class="form-container">
                    <form action="{{ route('shop.subscribe') }}">
                        <div class="control-group" :class="[errors.has('subscriber_email') ? 'has-error' : '']">
                            <input type="email" class="control subscribe-field" name="subscriber_email" placeholder="Email Address" required><br/>

                            <button class="btn btn-md btn-primary">{{ __('shop::app.subscription.subscribe') }}</button>
                        </div>
                    </form>
                </div>

                <span class="list-heading">{{ __('shop::app.footer.locale') }}</span>
                <div class="form-container">
                    <div class="control-group">
                        <select class="control locale-switcher" onchange="window.location.href = this.value">

                            @foreach (core()->getCurrentChannel()->locales as $locale)
                                <option value="?locale={{ $locale->code }}" {{ $locale->code == app()->getLocale() ? 'selected' : '' }}>{{ $locale->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="currency">
                    <span class="list-heading">{{ __('shop::app.footer.currency') }}</span>
                    <div class="form-container">
                        <div class="control-group">
                            <select class="control locale-switcher" onchange="window.location.href = this.value">

                                @foreach (core()->getCurrentChannel()->currencies as $currency)
                                    <option value="?currency={{ $currency->code }}" {{ $currency->code == core()->getCurrentCurrencyCode() ? 'selected' : '' }}>{{ $currency->code }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>

            </div> -->
</div>
<div class="footer-bottom">
    <p>@ 2019 MYCLIKS & IT’s AFFILIATE</p>
</div>