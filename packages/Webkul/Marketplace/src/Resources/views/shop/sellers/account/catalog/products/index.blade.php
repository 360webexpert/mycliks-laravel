@extends('marketplace::shop.layouts.account')

@section('page_title')
{{ __('marketplace::app.shop.sellers.account.catalog.products.title') }}
@endsection

@section('content')

<div class="account-layout">

    <div class="account-head mb-10">
        <span class="account-heading">
            {{ __('marketplace::app.shop.sellers.account.catalog.products.title') }}
        </span>

        <div class="account-action">
            <a href="{{ route('marketplace.account.products.search') }}" class="btn btn-primary btn-lg">
                {{ __('marketplace::app.shop.sellers.account.catalog.products.create') }}
            </a>
        </div>

        <div class="horizontal-rule"></div>
    </div>

    {!! view_render_event('marketplace.sellers.account.catalog.products.list.before') !!}        

    <div class="account-items-list">            
        <table class="table" id="saller_table">
            <thead>
                <tr>
                    <th class="grid_head">Id</th>
                    <th class="grid_head">Name</th>
                    <th class="grid_head">Is Approved</th>
                    <th>Actions</th>
                </tr>
            </thead> 
            <tbody>

                @foreach($saller_product as $sal_prd)
                <tr>
                    <td data-value="Id" style="text-align: center;">{{$sal_prd->id}}</td> 
                    <td data-value="Name" style="text-align: center;">{{$sal_prd->name}}</td> 
                    <td data-value="Is Approved" style="text-align: center;">@if($sal_prd->is_approved==0) NO @else Yes @endif</td> 
                    <td data-value=" Actions" class="actions" style="width: 100px; text-align: center;"><div>
                            <a href="{{URL::to('/')}}/marketplace/account/catalog/products/edit/{{$sal_prd->id}}">
                                <span onclick="return confirm('Do you really want to perform this action?')" class="icon pencil-lg-icon"></span>
                            </a> 
                            <a href="{{URL::to('/')}}/marketplace/account/catalog/products/delete/{{$sal_prd->id}}">
                                <span onclick="return confirm('Do you really want to perform this action?')" class="icon trash-icon"></span>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    {!! view_render_event('marketplace.sellers.account.catalog.products.list.after') !!}

</div>

@endsection