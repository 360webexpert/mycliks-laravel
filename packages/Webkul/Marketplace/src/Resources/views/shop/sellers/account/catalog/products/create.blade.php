@extends('marketplace::shop.layouts.account')

@section('page_title')
{{ __('marketplace::app.shop.sellers.account.catalog.products.create-title') }}
@endsection

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css" />
<div class="account-layout">
    <form id="product_form" action="" enctype="multipart/form-data" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="wizard1">
            <div class="wizard-header">
                <h3>Add Product</h3><span class="inner-headeing">Please choose the right category for your product.</span>
            </div>
            <div class="wizard-content">    
                <div class="control-group text">
                    <label for="prdname" class="required">Product Name</label>                               
                    <input type="text" id="prdname" class="control" placeholder="Product Name" value="{{old('name')}}">
                </div>

                <div class="cats_content">
                    <div class="control-group text">
                        <input class="control" id="myInput" type="text" placeholder="Categories Name" autocomplete="off">
                    </div>

                    <ul id="myTable">
                        @php                
                        $count_cats=count($cats);
                        $i=1;
                        @endphp
                        @if($count_cats!=0)

                        @foreach($cats as $cates)

                        <li>
                            <label for="mycats_{{$i}}">
                                <input id="mycats_{{$i}}" type="checkbox" name="cats_selected" class="cats_selected" value="{{$cates->id}}" data-title="{{$cates->name}}"/>{{$cates->name}}

                                @php
                                $cat_id=$cates->id;
                                $subcats = DB::table('categories')
                                ->select('categories.id', 'category_translations.name', 'category_translations.slug')
                                ->join('category_translations', 'categories.id', '=', 'category_translations.category_id')
                                ->where('categories.status', '=', 1)
                                ->where('categories.parent_id','=',$cat_id)
                                ->orderby('position', 'asc')
                                ->get()
                                ->toArray();
                                $count=count($subcats);
                                if($count!=0):
                                @endphp
                                <span class="arrow"><a href="javascript:void(0)"><i class="fa fa-angle-right"></i></a></span>
                            </label>
                            @php
                            foreach($subcats as $scates):
                            @endphp
                            <ul class="subcats" style="display: none;">
                                <li>
                                    <label for="mycats_{{$i}}">
                                        <input id="mycats_{{$i}}" type="checkbox" name="cats_selected" class="cats_selected" value="{{$scates->id}}" data-title="{{$scates->name}}"/>{{$scates->name}}
                                    </label>
                                </li>
                            </ul>
                            @php                            
                            endforeach;
                            else:
                            @endphp
                            </label>
                            @php
                            endif;
                            @endphp

                        </li>
                        @php                
                        $i++;
                        @endphp
                        @endforeach
                        @endif
                    </ul>
                    <input type="hidden" name="selected_cats" id="selected_cats"/>
                    <div class="current_selected" style="display: none;">
                        <h4>The Currently Selected: </h4><span class="show_cats"></span>                
                    </div>
                    <div class="next_info"><a href="javascript:void(0)">Next</a></div>
                </div>
            </div>
        </div>

        <div class="other_info" style="display: none;">
            <div class="wizard2">
                <div class="wizard-header"><h4>Basic Information</h4></div>
                <div class="wizard-content">

                    <div class="control-group text">
                        <label for="other_prdname" class="required">Product Name</label>                               
                        <input type="text" id="other_prdname" name="name" class="control" data-vv-id="2" placeholder="Product Name" value="{{old('name')}}">
                    </div>


                    <div class="current_selected">
                        <h4>Category: </h4><span class="show_cats"></span> <a class="edit_cats" href="javascript:void(0)">Edit</a>                
                    </div>


                    <div class="control-group textarea">                    
                        <label for="short_description">Short Description</label> 
                        <textarea id="short_description" name="short_description"  class="control"></textarea>               
                    </div>

                    <div class="control-group textarea">                    
                        <label for="description" class="required">Description</label> 
                        <textarea id="description" name="description"  class="control"></textarea>
                    </div>

                    <div class="control-group">
                        <input type="hidden" value="simple" name="type" id="product_type"/>  <!-- Product Type simple/Configurable -->
                        <input type="hidden" value="1" name="attribute_family_id"/>     <!-- Attribute Type Default -->                
                    </div>

                    <div class="control-group text">
                        <label for="url_key" class="required">URL Key</label> 
                        <input type="text" id="url_key" name="url_key"  class="control"> 
                    </div>

                    <input type="hidden" value="1" name="visible_individually"/>
                    <input type="hidden" value="1" name="status"/>
                </div>
            </div> 

            <div class="wizard3">
                <div class="wizard-header"><h4>Sale Information</h4></div>
                <div class="wizard-content">
                    <div class="control-group">
                        <label for="sku" class="required">Product Type</label> 
                        <select name="prd_type" class="control prd_control">
                            <option value="1">Simple</option>
                            <option value="2">Variable</option>
                        </select>
                    </div>

                    <div class="product_configurable" id="simple">
                        <div class="control-group">
                            <label for="sku" class="required">SKU</label> 
                            <input type="text" id="sku" name="sku" class="control"> <span id="sku"></span>
                            <input type="hidden" id="sku_check" name="sku_check" value="">
                        </div>
                        <div class="control-group price">
                            <label for="price" class="required">Price<span class="currency-code">($)</span></label>
                            <input type="text" id="price" name="price" class="control">                     
                        </div>
                        <div class="control-group price">
                            <label for="price" class="required">Stock</label> 
                            <input type="number" id="qty" name="stock" class="control">                     
                        </div>

                        @php                
                        $count_cats=count($attr);                
                        @endphp
                        @if($count_cats!=0)                
                        <div class="control-group select">
                            <label for="color">Color</label> 
                            <select id="color" name="color" class="control attr_filter">
                                <option>Select Color</option>
                                @foreach($attr as $colorattr)
                                @if($colorattr->attribute_id==23)
                                <option value="{{$colorattr->id}}">{{$colorattr->admin_name}}</option>                        
                                @endif
                                @endforeach
                            </select> 
                            <input type="hidden" name="color_label"/>
                        </div>

                        <div class="control-group select">
                            <label for="size">Size</label> 
                            <select id="size" name="size" class="control attr_filter">
                                <option>Select Size</option>
                                @foreach($attr as $colorattr)
                                @if($colorattr->attribute_id==24)
                                <option value="{{$colorattr->id}}">{{$colorattr->admin_name}}</option>                        
                                @endif
                                @endforeach
                            </select>
                            <input type="hidden" name="size_label"/>
                        </div>
                        @endif
                    </div>

                    <div class="product_configurable" id="variable" style="display: none;">

                        @php                
                        $count_attr=count($show_attr);                
                        @endphp
                        @if($count_attr!=0)
                        <div class="left-section">
                            <ul class="left-side">
                                <li><a href="javascript:void(0)" id="1">Attribute</a></li>
                                <li><a href="javascript:void(0)" id="2">Variations</a></li>
                            </ul>
                        </div>
                        <div class="right-section">
                            <div class="attr_section variation_detail" id="show_1">
                                <input type="hidden" id="show_attr_id" value="-1"/>
                                <select class="control limitedNumbChosen" multiple="true">
                                    @foreach($show_attr as $myattr)
                                    <option value="{{$myattr->id}}">{{$myattr->code}}</option>
                                    @endforeach                                    
                                </select>
                                <a class="save_attr_data" href="javascript:void(0)" id="save_attr">Save Attribute</a>
                            </div>
                            <div class="variation_section variation_detail" id="show_2" style="display: none;">
                                <div id="get_variation"><!-- GET DATA From Ajax --></div>
                                <div class="new_variation">
                                    <a href="javascript:void(0)">Add New</a>
                                    <p style="color:red;">Please select attribute and manage variations</p>
                                </div>
                            </div>
                        </div>                        
                        @else
                        <label></label>
                        <p style="color:red">No Attribute found on the server</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="wizard3">
                <div class="wizard-header"><h4>Media Information</h4></div>
                <div class="wizard-content">                    
                    <div class="control-group price">
                        <div class="form_uploader">
                            <div class="upload_repeater">
                                <label for="1" class="image-item">                                    
                                    <input type="file" accept="image/*" name="images[1]" id="1">
                                    <img class="preview_image" id="preview_1" src="<?php echo url()->to('/') . '/storage/other_images/default.png'; ?>"/>                                
                                </label>
                            </div>
                            <label class="new_image">
                                <a href="javascript:void(0)" class="add_new">Upload Image</a>
                            </label>
                        </div>                    
                    </div>
                </div>
            </div>

            <!--<div class="wizard4">
                <div class="wizard-header"><h4>Meta Information</h4></div>
                <div class="wizard-content">
                    <div class="control-group textarea">
                        <label for="meta_title">Meta Title<span class="locale">[default - en]</span></label> 
                        <textarea id="meta_title" name="meta_title" class="control" aria-required="false" aria-invalid="false"></textarea>
                    </div>
                    <div class="control-group textarea">
                        <label for="meta_title">Meta Keywords<span class="locale">[default - en]</span></label> 
                        <textarea id="meta_title" name="meta_keywords" class="control" aria-required="false" aria-invalid="false"></textarea>
                    </div>
                    <div class="control-group textarea">
                        <label for="meta_title">Meta Description<span class="locale">[default - en]</span></label> 
                        <textarea id="meta_title" name="meta_description" class="control" aria-required="false" aria-invalid="false"></textarea>
                    </div>
                </div>
            </div>-->

            <div class="wizard4">
                <div class="wizard-header"><h4>Shipping Information</h4></div>
                <div class="wizard-content">
                    <div class="control-group text">
                        <label for="prd_weight" class="required">Weight Kg</label> 
                        <input type="text" id="prd_weight" name="prd_weight" class="control">
                    </div>

                    <div class="control-group textarea">
                        <div class="control-group text shiping-content">
                            <label for="weight" class="">Parcel Size</label> 
                            <input type="text" id="width" name="width" class="control" placeholder="W"><span>CM</span>
                            <input type="text" id="length" name="length" class="control" placeholder="L"><span>CM</span>
                            <input type="text" id="height" name="height" class="control" placeholder="H"><span>CM</span>
                        </div>
                    </div> 

                    <div class="control-group shipping_data">
                        <div class="control-group text shiping-content">
                            <label for="weight" class="">Shipping Fee</label>                             
                            <div class="shipping-fee">
                                @php
                                $ship_count=count($shiping_method);                                
                                @endphp

                                @if($ship_count!="")
                                @foreach($shiping_method as $getship)
                                <div class="shipping_item">
                                    <div class="add_shipping">
                                        <label>{{$getship->ship_name}}</label>
                                        <label class="switch">                                        
                                            <input type="checkbox" name="shipping_details[]" class="ship_payment_method" value="{{$getship->ship_id}}"/>
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <div class="expend_details" id="expend_{{$getship->ship_id}}" style="display: none;">
                                        <input type="text" name="ship_price_{{$getship->ship_id}}" placeholder="Shipping Price"/>
                                        <input type="hidden" name="shipname_{{$getship->ship_id}}" value="{{$getship->ship_name}}"/>
                                    </div>

                                </div>
                                @endforeach                                
                                @endif
                            </div>
                        </div>
                    </div>

                </div>

                <div class="submit_product">
                    <input class="btn btn-lg btn-primary save_btn" type="submit" name="save_product" value="Save and Publish"/>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

<style>    
    .limitedNumbChosen{
        width: 400px;
    }
    .chosen-container.chosen-container-multi {
        width: 100% !important;
    }
    span.price-error {
        position: absolute;
        right: 0;
        bottom: -15px;
        font-size: 12px;
        z-index: 10000;
        color: #ff5656;
        margin-top: 5px;
    }
    .submit_product input.btn.btn-primary.disable_btn {
        background: #ddd !important;
    }
</style>

@push('scripts')
<script src="<?php echo asset('/'); ?>/vendor/webkul/admin/assets/js/tinyMCE/tinymce.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.jquery.min.js"></script>
<script>
$(document).ready(function () {
    $("#myInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable li").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#myTable li .cats_selected").click(function () {
        var favorite = [];
        var text = [];
        if ($(this).prop("checked") == true) {
            $(this).parent().parent().addClass("cat_active");
        } else {
            $(this).parent().parent().removeClass("cat_active");
        }

        $.each($(".cats_selected:checked"), function () {
            favorite.push($(this).val());
            text.push($(this).attr('data-title'));
        });
        $("#selected_cats").val(favorite.join(","));
        $(".current_selected").show();
        $(".show_cats").html(text.join(", "));
    });

    //Shipping Box JS
    $(".shipping_item input").click(function () {
        var get_val = $(this).val();
        if ($(this).prop("checked") == true) {
            $("#expend_" + get_val).slideDown("slow");
        } else {
            $("#expend_" + get_val).slideUp("slow");
        }
    });

    //section show/hide
    $(".next_info a").click(function () {
        $(".wizard1").slideUp("slow");
        $(".other_info").slideDown("slow");
    });

    $(".current_selected .edit_cats").click(function () {
        $(".other_info").slideUp("slow");
        $(".wizard1").slideDown("slow");
    });

    $("#prdname").keyup(function () {
        var getprd_name = $(this).val();
        var prd_strurl = getprd_name.replace(/\s+/g, '-').toLowerCase();
        $("#other_prdname").val(getprd_name);
        $("#url_key").val(prd_strurl);
    });

    $("#other_prdname").keyup(function () {
        var getprd_name = $(this).val();
        var prd_strurl = getprd_name.replace(/\s+/g, '-').toLowerCase();
        $("#prdname").val(getprd_name);
        $("#url_key").val(prd_strurl);
    });



    //Product Configurable js
    $(".limitedNumbChosen").chosen();
    $(".prd_control").change(function () {
        var get_prdtype = $(this).val();
        if (get_prdtype == 1) {
            $("#variable").slideUp("slow");
            $("#simple").slideDown("slow");
            $("#product_type").val('simple');
        } else {
            $("#simple").slideUp("slow");
            $("#variable").slideDown("slow");
            $("#product_type").val('configurable');
        }
    });

    $(".left-side li a").click(function () {
        var get_id = $(this).attr('id');
        $(".left-side li a").removeClass("attr_active");
        $(this).addClass("attr_active");
        var blank_attr = $("#show_attr_id").val();
        $(".variation_detail").hide();
        $("#show_" + get_id).show();
        if (get_id == 2 && $("#get_variation").children().length == 0) {
            alert("Please select attribute and manage variations");
            $(".new_variation a").hide();
            $(".new_variation p").show();
        } else {
            $(".new_variation a").show();
            $(".new_variation p").hide();
        }
    });

    $(".save_attr_data").click(function () {
        var get_chosen_value = $(".limitedNumbChosen").val();
        var get_chosen_text = $(".limitedNumbChosen option:selected");
        var send_text = $.map(get_chosen_text, function (option) {
            return option.text;
        });
        //var total_attr = $(".limitedNumbChosen").val().toString().split(",").length;
        if (get_chosen_value != "") {
            $(this).attr("disabled", "disabled");
            $(this).html('<i class="fa fa-spinner fa-spin" style="font-size:24px; color: #0041ff;"></i> Please Wait');
            $.ajax({
                url: "<?php echo url('/marketplace/account/catalog/products/saveattr'); ?>",
                type: 'POST',
                data: {"_token": "{{ csrf_token() }}", "send_attr_val": get_chosen_value, "send_attr_text": send_text},
                success: function (resp) {
                    $(".save_attr_data").removeAttr("disabled");
                    $(".save_attr_data").html('Save Attribute');
                    $("#get_variation").html(resp);
                    $(".left-side li a").removeClass("attr_active");
                    $(".left-side li:last-child a").addClass('attr_active');
                    $(".new_variation p").hide();
                    $("#show_2").show();
                    $("#show_1").hide();
                }, error: function () {
                    $(".save_attr_data").removeAttr("disabled");
                    $(".save_attr_data").html('Save Attribute');
                    alert("Failed to save attributes");
                }
            })
        }
    });

    $(".new_variation a ").click(function () {
        var get_html = $("#get_variation #variation_1").html();
        $("#get_variation .variation-dynamic:nth-last-child(2)").after("<div class='variation-dynamic'>" + get_html + "<div class='remove_variations'><a href='javascript:void(0)'><i class='fa fa-minus'></i></a></div></div>");
    });

    $("body").on("click", "#get_variation .remove_variations a", function () {
        $(this).parent().parent().remove();
    });



    //Tinymce Editor JS
    tinymce.init({
        selector: 'textarea#description, textarea#short_description',
        height: 200,
        width: "100%",
        plugins: 'image imagetools media wordcount save fullscreen code',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent  | removeformat | code',
        image_advtab: true
    });

    $(".attr_filter").change(function () {
        var get_text = $(this).find("option:selected").text();
        $(this).next().val(get_text);
    });

    $("#sku").keyup(function () {
        var get_sku = $(this).val();
        $("span#sku").html('');
        if (get_sku.length > 2) {
            $("span#sku").html('<i class="fa fa-spinner fa-spin" style="font-size:24px; color: #0041ff;"></i>');
            $.ajax({
                url: "<?php echo url('/marketplace/account/catalog/products/checksku'); ?>",
                type: 'POST',
                data: {"_token": "{{ csrf_token() }}", "send_sku": get_sku},
                success: function (resp) {
                    if (resp == 0) {
                        $("span#sku").html('<i class="fa fa-check" style="font-size:24px; color:green;"></i>');
                        $("#sku_check").val("true");
                        $(".price-error").hide();
                        $(".save_btn").removeAttr("disabled").removeClass("disable_btn");
                    } else {
                        $("span#sku").html('<i class="fa fa-times" style="font-size:24px; color:red;"></i>');
                        $(".save_btn").attr("disabled", 'true').addClass("disable_btn");
                        $("#sku").after('<span class="error price-error">Please choose another sku</span>');
                    }
                }
            })
        } else {
            $("span#sku").html('');
        }
    });

    $(".arrow a").click(function () {
        $("ul.subcats").hide();
        $(this).parent().parent().next().toggle();
    });

    $("#product_form").validate({
        errorElement: "span",
        errorClass: 'error control-error',
        highlight: function (element) {
            $(element).parent().addClass("has-error");
            $(element).addClass('teset');
        },
        unhighlight: function (element) {
            $(element).parent().removeClass("has-error");
        },
        rules: {
            name: "required",
            sku: {
                required: true,
                minlength: 3
            },
            sku_check: "required",
            url_key: "required",
            price: {
                required: true,
                number: true
            },
            stock: {
                required: true,
                digits: true
            },
            weight: {
                required: true,
                number: true
            },
            description: "required"
        },

        messages: {
            name: 'The "Name" field is required.',
            sku: {
                required: 'The "Sku" field is required.',
                minlength: 'Sku required at least 3 disgits'
            },
            sku_check: '',
            url_key: 'The "url_key" field is required.',
            price: {
                required: 'The "Price" field is required.',
                number: 'The "Price" field accept only digits',
            },
            stock: {
                required: 'The "Stock" field is required.',
                digits: 'The "Stock" field accept only digits',
            },
            weight: {
                required: 'The "Weight" field is required.',
                number: 'The "Weight" field accept only digits',
            },
            description: 'The "Description" field is required.'
        },
        submitHandler: function (form) {
            form.submit();
        }
    });


    $(".new_image a").click(function () {
        var get_lastrep = $(".upload_repeater label:last-child input").attr('id');
        get_lastrep = parseInt(get_lastrep) + 1;
        var count_label = $(".upload_repeater .image-item").length;
        if (count_label < 5) {
            $(".upload_repeater").append('<label for="' + get_lastrep + '" class="image-item"><input type="file" accept="image/*" name="images[' + get_lastrep + ']" id="' + get_lastrep + '"><img class="preview_image" id="preview_' + get_lastrep + '" src="<?php echo url()->to('/') . '/storage/other_images/default.png'; ?>"/><label class="remove-image">Remove Image</label></label>');
        } else {
            alert("You can't add more media");
        }
    });

    $("body").on("change", ".upload_repeater label input", function () {
        var get_id = $(this).attr('id');
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $("#preview_" + get_id).attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $("body").on("click", ".upload_repeater label label.remove-image", function () {
        $(this).parent().remove();
    });

});
</script>
@endpush