<?php

namespace Webkul\Marketplace\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Event;
use Illuminate\Routing\Router;
use \Webkul\Marketplace\Models\Seller;
use Webkul\Core\Tree;

class MarketplaceServiceProvider extends ServiceProvider {

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router) {
        include __DIR__ . '/../Http/front-routes.php';

        include __DIR__ . '/../Http/admin-routes.php';

        $this->app->register(ModuleServiceProvider::class);

        $this->app->register(EventServiceProvider::class);

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'marketplace');

        $this->publishes([
            __DIR__ . '/../Resources/views/shop/customers/account/partials' => resource_path('views/vendor/shop/customers/account/partials'),
        ]);

        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('themes/default/assets'),
                ], 'public');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'marketplace');

        $this->composeView();
    }

    /**
     * Bind the the data to the views
     *
     * @return void
     */
    protected function composeView() {
        view()->composer(['marketplace::shop.sellers.account.catalog.products.create', 'marketplace::shop.sellers.account.catalog.products.edit'], function ($view) {
            $accordian = Tree::create();
            if (config('product_form_accordians') !== null) {
                foreach (config('product_form_accordians') as $item) {

                    if ($item['key'] == 'inventories') {
                        $item['view'] = 'marketplace::shop.sellers.account.catalog.products.accordians.inventories';
                    } else if ($item['key'] == 'variations') {
                        $item['view'] = 'marketplace::shop.sellers.account.catalog.products.accordians.variations';
                    }

                    $accordian->add($item);
                }
            }

            $accordian->items = core()->sortItems($accordian->items);

            $view->with('form_accordians', $accordian);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->registerConfig();
    }

    /**
     * Register package config.
     *
     * @return void
     */
    protected function registerConfig() {
        $this->mergeConfigFrom(
                dirname(__DIR__) . '/Config/system.php', 'core'
        );

        $this->mergeConfigFrom(
                dirname(__DIR__) . '/Config/menu.php', 'menu.customer'
        );

        $this->mergeConfigFrom(
                dirname(__DIR__) . '/Config/admin-menu.php', 'menu.admin'
        );

        $this->mergeConfigFrom(
                dirname(__DIR__) . '/Config/acl.php', 'acl'
        );
    }

}
