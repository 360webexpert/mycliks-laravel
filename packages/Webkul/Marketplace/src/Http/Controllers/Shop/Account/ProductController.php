<?php

namespace Webkul\Marketplace\Http\Controllers\Shop\Account;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Image;
use File;
use Illuminate\Http\Response;
use Webkul\Marketplace\Http\Controllers\Shop\Controller;
use Webkul\Product\Http\Requests\ProductForm;
use Webkul\Product\Repositories\ProductRepository as Product;
use Webkul\Attribute\Repositories\AttributeFamilyRepository as AttributeFamily;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Inventory\Repositories\InventorySourceRepository as InventorySource;
use Webkul\Marketplace\Repositories\ProductRepository as SellerProduct;
use Webkul\Marketplace\Repositories\SellerRepository as Seller;

/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductController extends Controller {

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * AttributeFamilyRepository object
     *
     * @var array
     */
    protected $attributeFamily;

    /**
     * CategoryRepository object
     *
     * @var array
     */
    protected $category;

    /**
     * InventorySourceRepository object
     *
     * @var array
     */
    protected $inventorySource;

    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $product;

    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $sellerProduct;

    /**
     * SellerRepository object
     *
     * @var array
     */
    protected $seller;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Attribute\Repositories\AttributeFamilyRepository $attributeFamily
     * @param  Webkul\Category\Repositories\CategoryRepository         $category
     * @param  Webkul\Inventory\Repositories\InventorySourceRepository $inventorySource
     * @param  Webkul\Product\Repositories\ProductRepository           $product
     * @param  Webkul\Marketplace\Repositories\ProductRepository       $sellerProduct
     * @param  Webkul\Marketplace\Repositories\SellerRepository        $seller
     * @return void
     */
    public function __construct(
            AttributeFamily $attributeFamily,
            Category $category,
            InventorySource $inventorySource,
            Product $product,
            SellerProduct $sellerProduct,
            Seller $seller
    ) {
        $this->attributeFamily = $attributeFamily;

        $this->category = $category;

        $this->inventorySource = $inventorySource;

        $this->product = $product;

        $this->sellerProduct = $sellerProduct;

        $this->seller = $seller;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $isSeller = $this->seller->isSeller(auth()->guard('customer')->user()->id);
        if (!$isSeller) {
            return redirect()->route('marketplace.account.seller.create');
        }

        $saller_id = auth()->guard('customer')->user()->id;
        $saller_product = DB::table("products")
                ->select("products.id", "products.type", "products_grid.name", "marketplace_products.is_approved")
                ->join('products_grid', 'products.id', '=', 'products_grid.product_id')
                ->join('marketplace_products', 'products.id', '=', 'marketplace_products.product_id')
                ->join('product_inventories', 'products.id', '=', 'product_inventories.product_id')
                ->where('product_inventories.vendor_id', $saller_id)
                ->orderby('products.id', 'DESC')
                ->get()
                ->toArray();
        
        return view($this->_config['view'], compact('saller_product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $families = $this->attributeFamily->all();

        if ($familyId = request()->get('family')) {
            $configurableFamily = $this->attributeFamily->find($familyId);
        }

        $cats = DB::table('categories')
                ->select('categories.id', 'category_translations.name', 'category_translations.slug')
                ->join('category_translations', 'categories.id', '=', 'category_translations.category_id')
                ->where('categories.status', '=', 1)
                ->WhereNull('categories.parent_id')
                ->orderby('position', 'asc')
                ->get()
                ->toArray();

        $attr = DB::table('attribute_options')
                ->select('attribute_options.id', 'attribute_options.admin_name', 'attribute_options.attribute_id')
                ->join('attributes', 'attribute_options.attribute_id', '=', 'attributes.id')
                ->orderby('attribute_options.sort_order', 'asc')
                ->get()
                ->toArray();

        $shiping_method = DB::table('shipping_name')
                ->orderby('ship_id', 'asc')
                ->get()
                ->toArray();

        $show_attr = DB::table('attributes')
                ->where('is_configurable', 1)
                ->get()
                ->toArray();

        return view($this->_config['view'], compact('families', 'configurableFamily', 'cats', 'attr', 'shiping_method', 'show_attr'));
    }

    public function checksku() {
        $send_sku = $_POST['send_sku'];
        $result = DB::table('products')
                ->where('sku', '=', $send_sku)
                ->get()
                ->toArray();
        $count_result = count($result);
        echo $count_result;
        die();
    }

    public function saveattr(Request $request) {
        $send_attr_val = ($request->input('send_attr_val') != '') ? $request->input('send_attr_val') : '';
        $send_attr_text = ($request->input('send_attr_text') != '') ? $request->input('send_attr_text') : '';

        $final_attr_val = implode(",", $send_attr_val);
        $final_attr_text = implode(',', $send_attr_text);

        $lastinsertid = DB::getPdo()->lastInsertId();
        $i = 0;
        echo "<div id='variation_1' class='variation-dynamic'>";
        foreach ($send_attr_text as $select_text):
            $attr_id = $send_attr_val[$i];
            $result_attr_val = DB::table('attribute_options')->where('attribute_id', $attr_id)->orderby('sort_order', 'asc')->get()->toArray();
            ?>
            <div class="variation_item">
                <select name="<?php echo strtolower($select_text); ?>[]">
                    <option value="-1">Select <?php echo ucfirst($select_text); ?></option>
                    <?php foreach ($result_attr_val as $attr_val): ?>
                        <option value="<?php echo $attr_val->id; ?>"><?php echo $attr_val->admin_name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <?php
            $i++;
        endforeach;
        ?>
        <div class="variation_item">
            <input type="text" name="variation_price[]" placeholder="Price"/>
        </div>
        <div class="variation_item">
            <input type="text" name="variation_stock[]" placeholder="Stock"/>
        </div>
        <div class="variation_item">
            <input type="text" name="variation_sku[]" placeholder="Sku"/>
        </div>
        </div>
        <div class='variation-dynamic'>
            <input type="hidden" name="selected_attr_id" value="<?php echo $final_attr_text; ?>"/>
            <input type="hidden" name="selected_attr_val" value="<?php echo $final_attr_val; ?>"/>
        </div>
        <?php
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $vendor_id = auth()->guard('customer')->user()->id;
            $prd_name = $request->post('name');
            $prd_cats = $request->post('selected_cats');
            $prd_short_description = $request->post('short_description');
            $prd_description = $request->post('description');
            $prd_type = $request->post('type');
            $prd_attribute_family_id = $request->post('attribute_family_id');
            $prd_sku = $request->post('sku');
            $prd_url_key = $request->post('url_key');
            $prd_color = $request->post('color');
            $prd_color_label = $request->post('color_label');
            $prd_size = $request->post('size');
            $prd_color_size = $request->post('size_label');
            $prd_visible_individually = $request->post('visible_individually');
            $prd_status = $request->post('status');
            $prd_price = $request->post('price');
            $prd_stock = $request->post('stock');
            $prd_meta_title = $request->post('meta_title');
            $prd_meta_keywords = $request->post('meta_keywords');
            $prd_meta_description = $request->post('meta_description');
            $prd_weight = $request->post('prd_weight');
            $prd_width = $request->post('width');
            $prd_length = $request->post('length');
            $prd_height = $request->post('height');
            $get_date = date('Y-m-d H:i:s');
            //Shipping Variables
            $prd_shipping_details = ($request->input('shipping_details') != '') ? $request->input('shipping_details') : '';
            
            if($prd_sku==""){
                $prd_conf_variation_sku = $request->post('variation_sku');                
                $prd_sku= $prd_conf_variation_sku[0];
            }
            
            $product_table = array(
                'sku' => $prd_sku,
                'type' => $prd_type,
                'created_at' => $get_date,
                'updated_at' => $get_date,
                'attribute_family_id' => $prd_attribute_family_id
            );
            $insertprd_tbl = DB::table('products')->insert([$product_table]);
            $lastinsertid = DB::getPdo()->lastInsertId();


            $market_place = DB::table('marketplace_sellers')
                    ->select("id")
                    ->where('is_approved', '=', '1')
                    ->where('customer_id', '=', $vendor_id)
                    ->first();

            $marketplace_seller_id = $market_place->id;

            //Variations Details saved into database
            if ($prd_type == "configurable") {
                $prd_conf_size = $request->post('size');
                $prd_conf_color = $request->post('color');
                $prd_conf_variation_price = $request->post('variation_price');
                $prd_conf_variation_stock = $request->post('variation_stock');
                $prd_conf_variation_sku = $request->post('variation_sku');
                $prd_conf_selected_attr_name = $request->post('selected_attr_id');
                $prd_conf_selected_attr_value = $request->post('selected_attr_val');
                $k = 0;
                foreach ($prd_conf_variation_price as $myconf) {
                    $conf_price = $myconf;   //Price

                    if ($prd_conf_color[$k] != "") {
                        $conf_color = $prd_conf_color[$k];
                        $final_conf = $conf_color;
                    }
                    if ($prd_conf_size[$k] != "") {
                        $conf_size = $prd_conf_size[$k];
                        $final_conf = $conf_size;
                    }

                    if ($prd_conf_color[$k] != "" && $prd_conf_size[$k] != "") {
                        $final_conf = $conf_color . ", " . $conf_size;
                    }


                    $conf_stock = $prd_conf_variation_stock[$k];
                    $conf_sku = $prd_conf_variation_sku[$k];
                    $conf_save = array(
                        'product_attr_data_price' => $conf_price,
                        'product_attr_data_stock' => $conf_stock,
                        'product_attr_data_sku' => $conf_sku,
                        'product_attr_data_detail' => $final_conf,
                        'selected_attr_text' => $prd_conf_selected_attr_name,
                        'selected_attr_val' => $prd_conf_selected_attr_value,
                        'product_id' => $lastinsertid
                    );

                    $cong_save = DB::table('product_attr_data')->insert([$conf_save]);
                    $k++;
                }

                $products_grid = array(
                    'product_id' => $lastinsertid,
                    'attribute_family_name' => 'Default',
                    'type' => $prd_type,
                    'name' => $prd_name,
                    'status' => 0
                );
                $insert_products_grid = DB::table('products_grid')->insert([$products_grid]);

                $product_flat = array(
                    'name' => $prd_name,
                    'description' => $prd_description,
                    'url_key' => $prd_url_key,
                    'weight' => $prd_weight,
                    'created_at' => $get_date,
                    'locale' => 'en',
                    'product_id' => $lastinsertid,
                    'updated_at' => $get_date,
                    'visible_individually' => $prd_visible_individually
                );
                $insert_product_flat = DB::table('product_flat')->insert([$product_flat]);

                $product_inventories = array(
                    'product_id' => $lastinsertid,
                    'inventory_source_id' => 1,
                    'vendor_id' => $vendor_id,
                );
                $insert_product_inventories = DB::table('product_inventories')->insert([$product_inventories]);

                $marketplace_products = array(
                    'description' => $prd_description,
                    'is_approved' => 0,
                    'is_owner' => 1,
                    'product_id' => $lastinsertid,
                    'marketplace_seller_id' => $marketplace_seller_id,
                    'created_at' => $get_date,
                    'updated_at' => $get_date
                );
                $insert_marketplace_products = DB::table('marketplace_products')->insert([$marketplace_products]);

                $a_16 = array(
                    'product_id' => $lastinsertid,
                    'attribute_id' => 11
                );
            } else {                                                                    //Simple Product      
                $products_grid = array(
                    'product_id' => $lastinsertid,
                    'attribute_family_name' => 'Default',
                    'sku' => $prd_sku,
                    'type' => $prd_type,
                    'name' => $prd_name,
                    'quantity' => $prd_stock,
                    'price' => $prd_price,
                    'status' => 0
                );
                $insert_products_grid = DB::table('products_grid')->insert([$products_grid]);

                $product_flat = array(
                    'sku' => $prd_sku,
                    'name' => $prd_name,
                    'description' => $prd_description,
                    'url_key' => $prd_url_key,
                    'price' => $prd_price,
                    'weight' => $prd_weight,
                    'color' => $prd_color,
                    'color_label' => $prd_color_label,
                    'size' => $prd_size,
                    'size_label' => $prd_color_size,
                    'created_at' => $get_date,
                    'locale' => 'en',
                    'product_id' => $lastinsertid,
                    'updated_at' => $get_date,
                    'visible_individually' => $prd_visible_individually
                );
                $insert_product_flat = DB::table('product_flat')->insert([$product_flat]);

                $product_inventories = array(
                    'qty' => $prd_stock,
                    'product_id' => $lastinsertid,
                    'inventory_source_id' => 1,
                    'vendor_id' => $vendor_id,
                );
                $insert_product_inventories = DB::table('product_inventories')->insert([$product_inventories]);

                $marketplace_products = array(
                    'price' => $prd_price,
                    'description' => $prd_description,
                    'is_approved' => 0,
                    'is_owner' => 1,
                    'product_id' => $lastinsertid,
                    'marketplace_seller_id' => $marketplace_seller_id,
                    'created_at' => $get_date,
                    'updated_at' => $get_date
                );
                $insert_marketplace_products = DB::table('marketplace_products')->insert([$marketplace_products]);

                $a_16 = array(
                    'float_value' => $prd_price,
                    'product_id' => $lastinsertid,
                    'attribute_id' => 11
                );
            }

            if ($prd_cats != "") {
                $selected_cat = explode(",", $prd_cats);
                foreach ($selected_cat as $cats) {
                    $product_categories = array(
                        'product_id' => $lastinsertid,
                        'category_id' => $cats,
                    );
                    $insert_products_grid = DB::table('product_categories')->insert([$product_categories]);
                }
            }




            //Save Shipping Data
            if ($prd_shipping_details != "") {
                foreach ($prd_shipping_details as $myship) {
                    $prd_ship_price = ($request->input('ship_price_' . $myship) != '') ? $request->input('ship_price_' . $myship) : '';
                    $prd_shipname = ($request->input('shipname_' . $myship) != '') ? $request->input('shipname_' . $myship) : '';
                    $insert_shipping_details = array(
                        'prd_ship_name' => $prd_shipname,
                        'prd_ship_price' => $prd_ship_price,
                        'product_id' => $lastinsertid,
                        'shipping_id' => $myship
                    );
                    DB::table('shipping_price')->insert([$insert_shipping_details]);
                }
            }

            //Meta Data Save
            $a_1 = array(
                'boolean_value' => 1,
                'product_id' => $lastinsertid,
                'attribute_id' => 8
            );

            $a_2 = array(
                'locale' => "en",
                'channel' => "default",
                'text_value' => $prd_short_description,
                'product_id' => $lastinsertid,
                'attribute_id' => 9
            );
            $a_3 = array(
                'locale' => "en",
                'channel' => "default",
                'text_value' => $prd_description,
                'product_id' => $lastinsertid,
                'attribute_id' => 10
            );
            $a_4 = array(
                'text_value' => $prd_sku,
                'product_id' => $lastinsertid,
                'attribute_id' => 1
            );

            $a_5 = array(
                'locale' => "en",
                'channel' => "default",
                'text_value' => $prd_name,
                'product_id' => $lastinsertid,
                'attribute_id' => 2
            );

            $a_6 = array(
                'text_value' => $prd_url_key,
                'product_id' => $lastinsertid,
                'attribute_id' => 3
            );
            $a_7 = array(
                'integer_value' => 0,
                'product_id' => $lastinsertid,
                'attribute_id' => 4
            );
            $a_8 = array(
                'boolean_value' => 0,
                'product_id' => $lastinsertid,
                'attribute_id' => 5
            );
            $a_9 = array(
                'boolean_value' => 0,
                'product_id' => $lastinsertid,
                'attribute_id' => 6
            );
            $a_10 = array(
                'boolean_value' => 1,
                'product_id' => $lastinsertid,
                'attribute_id' => 7
            );
            $a_11 = array(
                'integer_value' => 1,
                'product_id' => $lastinsertid,
                'attribute_id' => 23
            );

            $a_12 = array(
                'integer_value' => 6,
                'product_id' => $lastinsertid,
                'attribute_id' => 24
            );

            $a_13 = array(
                'locale' => "en",
                'text_value' => $prd_meta_title,
                'channel' => "default",
                'product_id' => $lastinsertid,
                'attribute_id' => 16
            );
            $a_14 = array(
                'locale' => "en",
                'text_value' => $prd_meta_keywords,
                'channel' => "default",
                'product_id' => $lastinsertid,
                'attribute_id' => 17
            );
            $a_15 = array(
                'locale' => "en",
                'text_value' => $prd_meta_description,
                'channel' => "default",
                'product_id' => $lastinsertid,
                'attribute_id' => 18
            );

            $a_17 = array(
                'float_value' => 0,
                'product_id' => $lastinsertid,
                'attribute_id' => 12
            );
            $a_18 = array(
                'float_value' => 0,
                'product_id' => $lastinsertid,
                'attribute_id' => 13
            );

            $a_19 = array(
                'text_value' => $prd_width,
                'product_id' => $lastinsertid,
                'attribute_id' => 19
            );
            $a_20 = array(
                'text_value' => $prd_height,
                'product_id' => $lastinsertid,
                'attribute_id' => 20
            );
            $a_21 = array(
                'text_value' => $prd_length,
                'product_id' => $lastinsertid,
                'attribute_id' => 21
            );
            $a_22 = array(
                'text_value' => $prd_weight,
                'product_id' => $lastinsertid,
                'attribute_id' => 22
            );
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_1]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_2]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_3]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_4]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_5]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_6]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_7]);
            //$insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_8]);
            //$insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_9]);
            //$insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_10]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_11]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_12]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_13]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_14]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_15]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_16]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_17]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_18]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_19]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_20]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_21]);
            $insert_product_attribute_values = DB::table('product_attribute_values')->insert([$a_22]);


            $check_folder = storage_path() . "/product/";
            $destinationPath = storage_path() . "/product/" . $lastinsertid . "/";
            if (!empty($request->file('images')) && count($request->file('images')) > 0) {
                if (!is_dir($check_folder . $lastinsertid . '/')) {
                    mkdir($check_folder . $lastinsertid . '/', 0777, true);
                }
                foreach ($request->file('images') as $key => $image) {
                    $image_real_name = sha1(@microtime()) . ".jpeg";
                    $image_namedb = "product/" . $lastinsertid . "/" . $image_real_name;
                    $image->move($destinationPath, $image_real_name);
                    $product_images = array(
                        'path' => $image_namedb,
                        'product_id' => $lastinsertid
                    );
                    $insert_product_images = DB::table('product_images')->insert([$product_images]);
                }
            }
            $message = "Product created successfully.";
        } catch (customException $e) {
            $message = $e->errorMessage();
        }

        session()->flash('success', $message);
        return redirect()->route($this->_config['redirect'], ['id' => $lastinsertid]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $seller = $this->seller->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $sellerProduct = $this->sellerProduct->findOneWhere([
            'product_id' => $id,
            'marketplace_seller_id' => $seller->id,
            'is_owner' => 0
        ]);

        if ($sellerProduct) {
            return redirect()->route('marketplace.account.products.edit-assign', ['id' => $sellerProduct->id]);
        }

        $cats = DB::table('categories')
                ->select('categories.id', 'category_translations.name', 'category_translations.slug')
                ->join('category_translations', 'categories.id', '=', 'category_translations.category_id')
                ->where('categories.status', '=', 1)
                ->WhereNull('categories.parent_id')
                ->orderby('position', 'asc')
                ->get()
                ->toArray();

        $attr = DB::table('attribute_options')
                ->select('attribute_options.id', 'attribute_options.admin_name', 'attribute_options.attribute_id')
                ->join('attributes', 'attribute_options.attribute_id', '=', 'attributes.id')
                ->orderby('attribute_options.sort_order', 'asc')
                ->get()
                ->toArray();

        $prdres = DB::table('products')
                ->select('products.*', 'products_grid.*', 'product_flat.*')
                ->join('products_grid', 'products.id', '=', 'products_grid.product_id')
                ->join('product_flat', 'products.id', '=', 'product_flat.product_id')
                ->where('products.id', '=', $id)
                ->get()
                ->toArray();


        $product_attribute_values = DB::table('product_attribute_values')
                ->where('product_attribute_values.product_id', '=', $id)
                ->get()
                ->toArray();

        $product_images = DB::table('product_images')
                ->where('product_images.product_id', '=', $id)
                ->get()
                ->toArray();

        $product_cats = DB::table('product_categories')
                ->select('product_categories.category_id', 'category_translations.name')
                ->join('category_translations', 'product_categories.category_id', '=', 'category_translations.id')
                ->where('product_categories.product_id', '=', $id)
                ->get()
                ->toArray();

        $shiping_method = DB::table('shipping_name')
                ->select('shipping_name.ship_id', 'shipping_name.ship_name', 'prd_ship_price')
                ->leftJoin('shipping_price', 'shipping_name.ship_id', '=', 'shipping_price.shipping_id')
                ->where('shipping_price.product_id', $id)
                ->orderby('shipping_name.ship_id', 'asc')
                ->get()
                ->toArray();

        $show_attr_variations = DB::table('attributes')
                ->where('is_configurable', 1)
                ->get()
                ->toArray();

        $all_variatio = $sel_variations = $sel_attr = $show_attr = array();

        if ($prdres[0]->type != "simple") {
            $sel_attr = DB::table('product_attr_data')
                    ->select(db::raw('DISTINCT(product_id)'), 'selected_attr_val', 'selected_attr_text')
                    ->where('product_id', $id)
                    ->get()
                    ->toArray();

            $selected_var = $sel_attr[0]->selected_attr_val;  //Selected Variations        
            $explode_var = explode(",", $selected_var);

            $all_variation = DB::table('attribute_options')
                    ->whereIn('attribute_id', $explode_var)
                    ->get()
                    ->toArray();

            $sel_variations = DB::table('product_attr_data')
                    ->where('product_id', $id)
                    ->get()
                    ->toArray();

            $show_attr = DB::table('attributes')
                    ->where('is_configurable', 1)
                    ->get()
                    ->toArray();

            return view($this->_config['view'], compact('cats', 'attr', 'prdres', 'product_attribute_values', 'product_attribute_values', 'product_images', 'product_cats', 'shiping_method', 'show_attr', 'sel_attr', 'sel_variations', 'all_variation'));
        } else {
            return view($this->_config['view'], compact('cats', 'attr', 'prdres', 'product_attribute_values', 'product_attribute_values', 'product_images', 'product_cats', 'shiping_method', 'show_attr', 'sel_attr', 'sel_variations', 'all_variation', 'show_attr_variations'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\Product\Http\Requests\ProductForm $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request) {
        try {
            $vendor_id = auth()->guard('customer')->user()->id;
            $prd_name = ($request->input('name') != '') ? $request->input('name') : '';
            $prd_cats = ($request->input('selected_cats') != '') ? $request->input('selected_cats') : '';
            $prd_short_description = ($request->input('short_description') != '') ? $request->input('short_description') : '';
            $prd_description = ($request->input('description') != '') ? $request->input('description') : '';
            $prd_type = ($request->input('type') != '') ? $request->input('type') : '';
            $prd_attribute_family_id = ($request->input('attribute_family_id') != '') ? $request->input('attribute_family_id') : '';
            $prd_sku = ($request->input('sku') != '') ? $request->input('sku') : '';
            $prd_url_key = ($request->input('url_key') != '') ? $request->input('url_key') : '';
            $prd_color = ($request->input('color') != '') ? $request->input('color') : '';
            $prd_color_label = ($request->input('color_label') != '') ? $request->input('color_label') : '';
            $prd_size = ($request->input('size') != '') ? $request->input('size') : '';
            $prd_color_size = ($request->input('size_label') != '') ? $request->input('size_label') : '';
            $prd_visible_individually = ($request->input('visible_individually') != '') ? $request->input('visible_individually') : '';
            $prd_status = ($request->input('status') != '') ? $request->input('status') : '';
            $prd_price = ($request->input('price') != '') ? $request->input('price') : '';
            $prd_stock = ($request->input('stock') != '') ? $request->input('stock') : '';
            $prd_meta_title = ($request->input('meta_title') != '') ? $request->input('meta_title') : '';
            $prd_meta_keywords = ($request->input('meta_keywords') != '') ? $request->input('meta_keywords') : '';
            $prd_meta_description = ($request->input('meta_keywords') != '') ? $request->input('meta_keywords') : '';
            $prd_weight = ($request->input('prd_weight') != '') ? $request->input('prd_weight') : '';
            $prd_width = ($request->input('width') != '') ? $request->input('width') : '';
            $prd_length = ($request->input('length') != '') ? $request->input('length') : '';
            $prd_height = ($request->input('height') != '') ? $request->input('height') : '';
            $prd_removed_images = ($request->input('removed_images') != '') ? $request->input('removed_images') : '';
            $get_date = date('Y-m-d H:i:s');


            //Shipping Variables
            $prd_shipping_details = ($request->input('shipping_details') != '') ? $request->input('shipping_details') : '';

            if ($prd_shipping_details != "") {
                DB::table('shipping_price')->where('product_id', $id)->delete();
                foreach ($prd_shipping_details as $myship) {
                    $prd_ship_price = ($request->input('ship_price_' . $myship) != '') ? $request->input('ship_price_' . $myship) : '';
                    $prd_shipname = ($request->input('shipname_' . $myship) != '') ? $request->input('shipname_' . $myship) : '';
                    $insert_shipping_details = array(
                        'prd_ship_name' => $prd_shipname,
                        'prd_ship_price' => $prd_ship_price,
                        'product_id' => $id,
                        'shipping_id' => $myship
                    );
                    DB::table('shipping_price')->insert([$insert_shipping_details]);
                }
            }

            $market_place = DB::table('marketplace_sellers')
                    ->select("id")
                    ->where('is_approved', '=', '1')
                    ->where('customer_id', '=', $vendor_id)
                    ->first();

            $marketplace_seller_id = $market_place->id;

            if ($prd_type == "configurable") {
                DB::table('product_attr_data')->where('product_id', $id)->delete();
                $prd_conf_size = $request->post('size');
                $prd_conf_color = $request->post('color');
                $prd_conf_variation_price = $request->post('variation_price');
                $prd_conf_variation_stock = $request->post('variation_stock');
                $prd_conf_variation_sku = $request->post('variation_sku');
                $prd_conf_selected_attr_name = $request->post('selected_attr_id');
                $prd_conf_selected_attr_value = $request->post('selected_attr_val');
                $k = 0;
                foreach ($prd_conf_variation_price as $myconf) {
                    $conf_price = $myconf;   //Price

                    if ($prd_conf_color[$k] != "") {
                        $conf_color = $prd_conf_color[$k];
                        $final_conf = $conf_color;
                    }
                    if ($prd_conf_size[$k] != "") {
                        $conf_size = $prd_conf_size[$k];
                        $final_conf = $conf_size;
                    }

                    if ($prd_conf_color[$k] != "" && $prd_conf_size[$k] != "") {
                        $final_conf = $conf_color . ", " . $conf_size;
                    }


                    $conf_stock = $prd_conf_variation_stock[$k];
                    $conf_sku = $prd_conf_variation_sku[$k];
                    $conf_save = array(
                        'product_attr_data_price' => $conf_price,
                        'product_attr_data_stock' => $conf_stock,
                        'product_attr_data_sku' => $conf_sku,
                        'product_attr_data_detail' => $final_conf,
                        'selected_attr_text' => $prd_conf_selected_attr_name,
                        'selected_attr_val' => $prd_conf_selected_attr_value,
                        'product_id' => $id
                    );

                    $cong_save = DB::table('product_attr_data')->insert([$conf_save]);
                    $k++;
                }


                $products_grid = array(
                    'attribute_family_name' => 'Default',
                    'type' => $prd_type,
                    'name' => $prd_name,
                    'status' => 0
                );
                $update_products_grid = DB::table('products_grid')->where('product_id', $id)->update($products_grid);

                $product_flat = array(
                    'name' => $prd_name,
                    'description' => $prd_description,
                    'weight' => $prd_weight,
                    'updated_at' => $get_date,
                    'visible_individually' => $prd_visible_individually
                );
                $update_product_flat = DB::table('product_flat')->where('product_id', '=', $id)->update($product_flat);


                $product_inventories = array(
                    'inventory_source_id' => 1,
                    'vendor_id' => $vendor_id,
                );
                $update_product_inventories = DB::table('product_inventories')->where('product_id', '=', $id)->update($product_inventories);

                $marketplace_products = array(
                    'description' => $prd_description,
                    'is_approved' => 0,
                    'is_owner' => 1,
                    'product_id' => $id,
                    'marketplace_seller_id' => $marketplace_seller_id,
                    'updated_at' => $get_date
                );
                $update_marketplace_products = DB::table('marketplace_products')->where('product_id', '=', $id)->update($marketplace_products);

                $a_16 = array(
                    'product_id' => $id,
                    'attribute_id' => 11
                );
            } else {

                $products_grid = array(
                    'attribute_family_name' => 'Default',
                    'sku' => $prd_sku,
                    'type' => $prd_type,
                    'name' => $prd_name,
                    'quantity' => $prd_stock,
                    'price' => $prd_price,
                    'status' => 0
                );
                $update_products_grid = DB::table('products_grid')->where('product_id', $id)->update($products_grid);


                $product_flat = array(
                    //'sku' => $prd_sku,
                    //'url_key' => $prd_url_key,
                    'name' => $prd_name,
                    'description' => $prd_description,
                    'price' => $prd_price,
                    'weight' => $prd_weight,
                    'color' => $prd_color,
                    'color_label' => $prd_color_label,
                    'size' => $prd_size,
                    'size_label' => $prd_color_size,
                    'locale' => 'en',
                    'updated_at' => $get_date,
                    'visible_individually' => $prd_visible_individually
                );
                $update_product_flat = DB::table('product_flat')->where('product_id', '=', $id)->update($product_flat);


                $product_inventories = array(
                    'qty' => $prd_stock,
                    'inventory_source_id' => 1,
                    'vendor_id' => $vendor_id,
                );
                $update_product_inventories = DB::table('product_inventories')->where('product_id', '=', $id)->update($product_inventories);




                $marketplace_products = array(
                    'price' => $prd_price,
                    'description' => $prd_description,
                    'is_approved' => 0,
                    'is_owner' => 1,
                    'product_id' => $id,
                    'marketplace_seller_id' => $marketplace_seller_id,
                    'updated_at' => $get_date
                );
                $update_marketplace_products = DB::table('marketplace_products')->where('product_id', '=', $id)->update($marketplace_products);

                $a_16 = array(
                    'float_value' => $prd_price,
                    'product_id' => $id,
                    'attribute_id' => 11
                );
            }


            $a_1 = array(
                'boolean_value' => 1
            );

            $a_2 = array(
                'locale' => "en",
                'channel' => "default",
                'text_value' => $prd_short_description
            );
            $a_3 = array(
                'locale' => "en",
                'channel' => "default",
                'text_value' => $prd_description
            );
            $a_4 = array(
                'text_value' => $prd_sku
            );

            $a_5 = array(
                'locale' => "en",
                'channel' => "default",
                'text_value' => $prd_name
            );

            $a_6 = array(
                'text_value' => $prd_url_key
            );
            $a_7 = array(
                'integer_value' => 0
            );
            $a_8 = array(
                'boolean_value' => 0
            );
            $a_9 = array(
                'boolean_value' => 0
            );
            $a_10 = array(
                'boolean_value' => 1
            );
            $a_11 = array(
                'integer_value' => 1
            );

            $a_12 = array(
                'integer_value' => 6
            );

            $a_13 = array(
                'locale' => "en",
                'text_value' => $prd_meta_title,
                'channel' => "default"
            );
            $a_14 = array(
                'locale' => "en",
                'text_value' => $prd_meta_keywords,
                'channel' => "default"
            );
            $a_15 = array(
                'locale' => "en",
                'text_value' => $prd_meta_description,
                'channel' => "default"
            );

            $a_17 = array(
                'float_value' => 0
            );
            $a_18 = array(
                'float_value' => 0
            );

            $a_19 = array(
                'text_value' => $prd_width
            );
            $a_20 = array(
                'text_value' => $prd_height
            );
            $a_21 = array(
                'text_value' => $prd_length
            );
            $a_22 = array(
                'text_value' => $prd_weight
            );
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 8)->update($a_1);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 9)->update($a_2);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 10)->update($a_3);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 1)->update($a_4);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 2)->update($a_5);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 3)->update($a_6);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 4)->update($a_7);
            //$update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 5)->update($a_8);
            //$update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 6)->update($a_9);
            //$update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 7)->update($a_10);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 23)->update($a_11);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 24)->update($a_12);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 16)->update($a_13);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 17)->update($a_14);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 18)->update($a_15);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 11)->update($a_16);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 12)->update($a_17);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 13)->update($a_18);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 19)->update($a_19);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 20)->update($a_20);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 21)->update($a_21);
            $update_product_attribute_values = DB::table('product_attribute_values')->where('product_id', '=', $id)->where('attribute_id', 22)->update($a_22);



            $check_folder = storage_path() . "/product/";
            $destinationPath = storage_path() . "/product/" . $id . "/";
            $rem_exp = explode(", ", $prd_removed_images);
            foreach ($rem_exp as $delimg) {
                $delqry = DB::table('product_images')->where('id', $delimg)->delete();
            }

            if (!empty($request->file('images')) && count($request->file('images')) > 0) {
                if (!is_dir($check_folder . $id . '/')) {
                    mkdir($check_folder . $id . '/', 0777, true);
                }
                foreach ($request->file('images') as $key => $image) {
                    $image_real_name = sha1(@microtime()) . ".jpeg";
                    $image_namedb = "product/" . $id . "/" . $image_real_name;
                    $image->move($destinationPath, $image_real_name);
                    $product_images = array(
                        'path' => $image_namedb,
                        'product_id' => $id
                    );
                    $update_product_images = DB::table('product_images')->insert([$product_images]);
                }
            }

            if ($prd_cats != "") {
                $delete_cats = DB::table('product_categories')->where('product_id', $id)->delete();
                $selected_cat = explode(",", $prd_cats);
                foreach ($selected_cat as $cats) {
                    $product_categories = array(
                        'product_id' => $id,
                        'category_id' => $cats,
                    );
                    $update_products_grid = DB::table('product_categories')->insert($product_categories);
                }
            }
            $message_update = "Product updated successfully.";
        } catch (customException $e) {
            $message_update = $e->errorMessage();
        }

        session()->flash('success', $message_update);
        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $sellerProduct = $this->sellerProduct->getMarketplaceProductByProduct($id);

        $this->sellerProduct->delete($sellerProduct->id);

        session()->flash('success', 'Product deleted successfully.');

        return redirect()->back();
    }

    /**
     * Mass Delete the products
     *
     * @return response
     */
    public function massDestroy() {
        $productIds = explode(',', request()->input('indexes'));

        foreach ($productIds as $productId) {
            $sellerProduct = $this->sellerProduct->getMarketplaceProductByProduct($productId);

            $this->sellerProduct->delete($sellerProduct->id);
        }

        session()->flash('success', trans('admin::app.catalog.products.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

}
