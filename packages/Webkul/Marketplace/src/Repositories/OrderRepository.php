<?php

namespace Webkul\Marketplace\Repositories;

use Illuminate\Container\Container as App;
use Webkul\Core\Eloquent\Repository;
use Illuminate\Support\Facades\Event;

/**
 * Seller Order Reposotory
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class OrderRepository extends Repository
{
    /**
     * SellerRepository object
     *
     * @var Object
     */
    protected $sellerRepository;

    /**
     * OrderItemRepository object
     *
     * @var Object
     */
    protected $orderItemRepository;

    /**
     * TransactionRepository object
     *
     * @var Object
     */
    protected $transactionRepository;

    /**
     * ProductRepository object
     *
     * @var Object
     */
    protected $productRepository;

    /**
     * Create a new repository instance.
     *
     * @param  Webkul\Product\Repositories\SellerRepository      $sellerRepository
     * @param  Webkul\Product\Repositories\OrderItemRepository   $orderItemRepository
     * @param  Webkul\Product\Repositories\TransactionRepository $transactionRepository
     * @param  Webkul\Product\Repositories\ProductRepository     $productRepository
     * @param  Illuminate\Container\Container                    $app
     * @return void
     */
    public function __construct(
        SellerRepository $sellerRepository,
        OrderItemRepository $orderItemRepository,
        TransactionRepository $transactionRepository,
        ProductRepository $productRepository,
        App $app
    )
    {
        $this->sellerRepository = $sellerRepository;

        $this->orderItemRepository = $orderItemRepository;

        $this->transactionRepository = $transactionRepository;

        $this->productRepository = $productRepository;

        parent::__construct($app);
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\Marketplace\Contracts\Order';
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $order = $data['order'];

        Event::fire('marketplace.sales.order.save.before', $data);

        $sellerOrders = [];

        $commissionPercentage = core()->getConfigData('marketplace.settings.general.commission_per_unit');

        foreach ($order->items()->get() as $item) {
            if (isset($item->additional['seller_info']) && !$item->additional['seller_info']['is_owner']) {
                $seller = $this->sellerRepository->find($item->additional['seller_info']['seller_id']);
            } else {
                $seller = $this->productRepository->getSellerByProductId($item->product_id);
            }

            if (! $seller)
                continue;

            $sellerProduct = $this->productRepository->findOneWhere([
                    'product_id' => $item->product->id,
                    'marketplace_seller_id' => $seller->id,
                ]);

            if (! $sellerProduct->is_approved)
                continue;

            $sellerOrder = $this->findOneWhere([
                    'order_id' => $order->id,
                    'marketplace_seller_id' => $seller->id,
                ]);

            if (! $sellerOrder) {
                $sellerOrders[] = $sellerOrder = parent::create([
                        'status' => 'pending',
                        'seller_payout_status' => 'pending',
                        'order_id' => $order->id,
                        'marketplace_seller_id' => $seller->id,
                        'commission_percentage' => $commissionPercentage,
                        'is_withdrawal_requested' => 0
                    ]);
            }

            $commission = $baseCommission = 0;
            $sellerTotal = $baseSellerTotal = 0;
            if ($commissionPercentage) {
                $commission = ($item->total * $commissionPercentage) / 100;
                $baseCommission = ($item->base_total * $commissionPercentage) / 100;

                $sellerTotal = $item->total - $commission;   
                $baseSellerTotal = $item->base_total - $baseCommission;   
            }

            $sellerOrderItem = $this->orderItemRepository->create([
                    'marketplace_product_id' => $sellerProduct->id,
                    'marketplace_order_id' => $sellerOrder->id,
                    'order_item_id' => $item->id,
                    'commission' => $commission,
                    'base_commission' => $baseCommission,
                    'seller_total' => $sellerTotal,
                    'base_seller_total' => $baseSellerTotal,
                ]);

            if ($childItem = $item->child) {
                $childSellerProduct = $this->productRepository->findOneWhere([
                        'product_id' => $childItem->product->id,
                        'marketplace_seller_id' => $seller->id,
                    ]);

                $childSellerOrderItem = $this->orderItemRepository->create([
                        'marketplace_product_id' => $childSellerProduct->id,
                        'marketplace_order_id' => $sellerOrder->id,
                        'order_item_id' => $childItem->id,
                        'parent_id' => $sellerOrderItem->id
                    ]);
            }
        }

        foreach ($sellerOrders as $order) {
            $this->collectTotals($order);

            Event::fire('marketplace.sales.order.save.after', $order);
        }
    }

    /**
     * @param int $order
     * @return mixed
     */
    public function cancel($order)
    {

        $sellerOrders = $this->findWhere(['order_id' => $order->id]);

        foreach ($sellerOrders as $sellerOrder) {
            if (! $sellerOrder->canCancel())
                return false;

            Event::fire('marketplace.sales.order.cancel.before', $sellerOrder);

            foreach ($sellerOrder->items as $item) {
                // $this->orderItemRepository->returnQtyToProductInventory($item);
            }

            $this->updateOrderStatus($sellerOrder);

            Event::fire('marketplace.sales.order.cancel.after', $sellerOrder);

            return true;
        }
    }

    /**
     * @param mixed $order
     * @return void
     */
    public function isInCompletedState($order)
    {
        $totalQtyOrdered = 0;
        $totalQtyInvoiced = 0;
        $totalQtyShipped = 0;
        $totalQtyRefunded = 0;
        $totalQtyCanceled = 0;

        foreach ($order->items  as $sellerOrderItem) {
            $totalQtyOrdered += $sellerOrderItem->item->qty_ordered;
            $totalQtyInvoiced += $sellerOrderItem->item->qty_invoiced;
            $totalQtyShipped += $sellerOrderItem->item->qty_shipped;
            $totalQtyRefunded += $sellerOrderItem->item->qty_refunded;
            $totalQtyCanceled += $sellerOrderItem->item->qty_canceled;
        }

        if ($totalQtyOrdered != ($totalQtyRefunded + $totalQtyCanceled) && 
            $totalQtyOrdered == $totalQtyInvoiced + $totalQtyRefunded + $totalQtyCanceled &&
            $totalQtyOrdered == $totalQtyShipped + $totalQtyRefunded + $totalQtyCanceled)
            return true;

        return false;
    }

    /**
     * @param mixed $order
     * @return void
     */
    public function isInCanceledState($order)
    {
        $totalQtyOrdered = 0;
        $totalQtyCanceled = 0;

        foreach ($order->items as $sellerOrderItem) {
            $totalQtyOrdered += $sellerOrderItem->item->qty_ordered;
            $totalQtyCanceled += $sellerOrderItem->item->qty_canceled;
        }

        if ($totalQtyOrdered == $totalQtyCanceled)
            return true;

        return false;
    }

    /**
     * @param mixed $order
     * @return void
     */
    public function isInClosedState($order)
    {
        $totalQtyOrdered = 0;
        $totalQtyRefunded = 0;
        $totalQtyCanceled = 0;

        foreach ($order->items  as $sellerOrderItem) {
            $totalQtyOrdered += $sellerOrderItem->item->qty_ordered;
            $totalQtyRefunded += $sellerOrderItem->item->qty_refunded;
            $totalQtyCanceled += $sellerOrderItem->item->qty_canceled;
        }

        if ($totalQtyOrdered == $totalQtyRefunded + $totalQtyCanceled)
            return true;

        return false;
    }

    /**
     * @param mixed $order
     * @return void
     */
    public function updateOrderStatus($order)
    {
        $status = 'processing';

        if ($this->isInCompletedState($order))
            $status = 'completed';

        if ($this->isInCanceledState($order))
            $status = 'canceled';
        elseif ($this->isInClosedState($order))
            $status = 'closed';

        $order->status = $status;
        $order->save();
    }

    /**
     * Updates marketplace order totals
     *
     * @param Order $order
     * @return void
     */
    public function collectTotals($order)
    {
        $order->grand_total = $order->base_grand_total = 0;
        $order->sub_total = $order->base_sub_total = 0;
        $order->tax_amount = $order->base_tax_amount = 0;
        $order->commission = $order->base_commission = 0;
        $order->seller_total = $order->base_seller_total = 0;
        $order->total_item_count = $order->total_qty_ordered = 0;

        foreach ($order->items()->get() as $sellerOrderItem) {
            $item = $sellerOrderItem->item;
            $order->grand_total += $item->total + $item->tax_amount;
            $order->base_grand_total += $item->base_total + $item->base_tax_amount;

            $order->sub_total += $item->total;
            $order->base_sub_total += $item->base_total;

            $order->tax_amount += $item->tax_amount;
            $order->base_tax_amount += $item->base_tax_amount;

            $order->commission += $sellerOrderItem->commission;
            $order->base_commission += $sellerOrderItem->base_commission;

            $order->seller_total += $sellerOrderItem->seller_total;
            $order->base_seller_total += $sellerOrderItem->base_seller_total;

            $order->total_qty_ordered += $item->qty_ordered;

            $order->total_item_count += 1;
        }

        $order->sub_total_invoiced = $order->base_sub_total_invoiced = 0;
        $order->shipping_invoiced = $order->base_shipping_invoiced = 0;
        $order->commission_invoiced = $order->base_commission_invoiced = 0;
        $order->seller_total_invoiced = $order->base_seller_total_invoiced = 0;

        foreach ($order->invoices as $invoice) {
            $order->sub_total_invoiced += $invoice->sub_total;
            $order->base_sub_total_invoiced += $invoice->base_sub_total;

            $order->shipping_invoiced += $invoice->shipping_amount;
            $order->base_shipping_invoiced += $invoice->base_shipping_amount;

            $order->tax_amount_invoiced += $invoice->tax_amount;
            $order->base_tax_amount_invoiced += $invoice->base_tax_amount;


            $order->commission_invoiced += $commissionInvoiced = ($invoice->sub_total * $order->commission_percentage) / 100;
            $order->base_commission_invoiced += $baseCommissionInvoiced = ($invoice->base_sub_total * $order->commission_percentage) / 100;

            $order->seller_total_invoiced += $invoice->sub_total - $commissionInvoiced;   
            $order->base_seller_total_invoiced += $invoice->base_sub_total - $baseCommissionInvoiced;   
        }

        $order->grand_total_invoiced = $order->sub_total_invoiced + $order->shipping_invoiced + $order->tax_amount_invoiced;
        $order->base_grand_total_invoiced = $order->base_sub_total_invoiced + $order->base_shipping_invoiced + $order->base_tax_amount_invoiced;

        $order->save();

        return $order;
    }
}