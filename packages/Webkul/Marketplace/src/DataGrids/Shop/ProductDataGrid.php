<?php

namespace Webkul\Marketplace\DataGrids\Shop;

use DB;
use Webkul\Ui\DataGrid\DataGrid;
use Webkul\Marketplace\Repositories\SellerRepository;

/**
 * Product Data Grid class
 *
 * @author Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductDataGrid extends DataGrid {

    /**
     * @var integer
     */
    protected $index = 'product_id';

    /**
     * SellerRepository object
     *
     * @var Object
     */
    protected $sellerRepository;

    /**
     * Create a new repository instance.
     *
     * @param  Webkul\Marketplace\Repositories\SellerRepository $sellerRepository
     * @return void
     */
    public function __construct(SellerRepository $sellerRepository) {
        $this->sellerRepository = $sellerRepository;
    }

    public function prepareQueryBuilder() {
        $seller = $this->sellerRepository->findOneByField('customer_id', auth()->guard('customer')->user()->id);

        $queryBuilder = DB::table('products_grid')
                ->leftJoin('products', 'products_grid.product_id', '=', 'products.id')
                ->leftJoin('marketplace_products', 'products.id', '=', 'marketplace_products.product_id')
                ->addSelect('products_grid.product_id', 'products_grid.sku', 'products_grid.name', 'products_grid.price', 'marketplace_products.is_approved')
                ->where('marketplace_products.marketplace_seller_id', $seller->id)
                ->distinct();

        $queryBuilder = $queryBuilder->leftJoin('product_inventories', function($qb) use($seller) {
            $qb->on('products_grid.product_id', 'product_inventories.product_id')
                    ->where('product_inventories.vendor_id', $seller->id);
        });


        $queryBuilder
                ->groupBy('products_grid.product_id')
                ->addSelect(DB::raw('SUM(product_inventories.qty) as quantity'));

        $this->addFilter('sku', 'products_grid.sku');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns() {
        $this->addColumn([
            'index' => 'product_id',
            'label' => trans('marketplace::app.shop.sellers.account.catalog.products.id'),
            'type' => 'number',
            'searchable' => false,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => trans('marketplace::app.shop.sellers.account.catalog.products.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);

        
        $this->addColumn([
            'index' => 'is_approved',
            'label' => trans('marketplace::app.shop.sellers.account.catalog.products.is-approved'),
            'type' => 'boolean',
            'sortable' => true,
            'searchable' => false,
            'wrapper' => function($row) {
                if ($row->is_approved == 1)
                    return trans('marketplace::app.shop.sellers.account.catalog.products.yes');
                else
                    return trans('marketplace::app.shop.sellers.account.catalog.products.no');
            }
        ]);
    }

    public function prepareActions() {
        $this->addAction([
            'type' => 'Edit',
            'route' => 'marketplace.account.products.edit',
            'icon' => 'icon pencil-lg-icon'
        ]);

        $this->addAction([
            'type' => 'Delete',
            'route' => 'marketplace.account.products.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'product']),
            'icon' => 'icon trash-icon'
        ]);
    }

    public function prepareMassActions() {
        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('marketplace::app.shop.sellers.account.catalog.products.delete'),
            'action' => route('marketplace.account.products.massdelete'),
            'method' => 'DELETE'
        ]);
    }

}
