<?php

namespace Webkul\Marketplace\DataGrids\Admin;

use DB;
use Webkul\Ui\DataGrid\DataGrid;

/**
 * Seller Data Grid class
 *
 * @author Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SellerDataGrid extends DataGrid
{
    /**
     *
     * @var integer
     */
    public $index = 'id';

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('marketplace_sellers')
                ->leftJoin('customers', 'marketplace_sellers.customer_id', '=', 'customers.id')
                ->select('marketplace_sellers.id', 'marketplace_sellers.created_at', 'customers.email', 'marketplace_sellers.is_approved', DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as customer_name'));

        $this->addFilter('customer_name', DB::raw('CONCAT(customers.first_name, " ", customers.last_name)'));

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'id',
            'label' => trans('marketplace::app.admin.sellers.id'),
            'type' => 'number',
            'searchable' => false,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'customer_name',
            'label' => trans('marketplace::app.admin.sellers.customer-name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'email',
            'label' => trans('marketplace::app.admin.sellers.customer-email'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'created_at',
            'label' => trans('marketplace::app.admin.sellers.created-at'),
            'type' => 'datetime',
            'sortable' => true,
            'searchable' => false
        ]);

        $this->addColumn([
            'index' => 'is_approved',
            'label' => trans('marketplace::app.admin.sellers.is-approved'),
            'type' => 'boolean',
            'sortable' => true,
            'searchable' => false,
            'wrapper' => function($row) {
                if ($row->is_approved == 1)
                    return trans('marketplace::app.admin.sellers.approved');
                else
                    return trans('marketplace::app.admin.sellers.un-approved');
            }
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.marketplace.sellers.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'product']),
            'icon' => 'icon trash-icon'
        ]);
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('marketplace::app.admin.sellers.delete'),
            'action' => route('admin.marketplace.sellers.massdelete'),
            'method' => 'DELETE'
        ]);

        $this->addMassAction([
            'type' => 'update',
            'label' => trans('marketplace::app.admin.sellers.update'),
            'action' => route('admin.marketplace.sellers.massupdate'),
            'method' => 'PUT',
            'options' => [
                trans('marketplace::app.admin.sellers.approve') => 1,
                trans('marketplace::app.admin.sellers.unapprove') => 0
            ]
        ]);
    }
}