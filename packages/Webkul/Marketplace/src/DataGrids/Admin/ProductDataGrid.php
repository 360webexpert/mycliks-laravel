<?php

namespace Webkul\Marketplace\DataGrids\Admin;

use DB;
use Webkul\Ui\DataGrid\DataGrid;

/**
 * Product Data Grid class
 *
 * @author Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductDataGrid extends DataGrid
{
    /**
     *
     * @var integer
     */
    public $index = 'marketplace_product_id';

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('products_grid')
                ->leftJoin('products', 'products_grid.product_id', '=', 'products.id')
                ->join('marketplace_products', 'products_grid.product_id', '=', 'marketplace_products.product_id')
                ->leftJoin('marketplace_sellers', 'marketplace_products.marketplace_seller_id', '=', 'marketplace_sellers.id')
                ->leftJoin('customers', 'marketplace_sellers.customer_id', '=', 'customers.id')

                ->addSelect('marketplace_products.id as marketplace_product_id', 'products_grid.product_id', 'products_grid.sku', 'products_grid.name', 'products_grid.price', 'products_grid.quantity', 'marketplace_products.is_approved', DB::raw('CONCAT(customers.first_name, " ", customers.last_name) as seller_name'));
        
        $queryBuilder = $queryBuilder->leftJoin('product_inventories', function($qb) {
            $qb->on('products_grid.product_id', 'product_inventories.product_id')
                ->where('product_inventories.vendor_id', '<>', 0);
        });


        $queryBuilder
            ->groupBy('marketplace_products.id')
            ->addSelect(DB::raw('SUM(product_inventories.qty) as quantity'));

        $this->addFilter('seller_name', DB::raw('CONCAT(customers.first_name, " ", customers.last_name)'));
        $this->addFilter('sku', 'products_grid.sku');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'product_id',
            'label' => trans('marketplace::app.admin.products.product-id'),
            'type' => 'number',
            'searchable' => false,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'seller_name',
            'label' => trans('marketplace::app.admin.products.seller-name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'sku',
            'label' => trans('marketplace::app.admin.products.sku'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'name',
            'label' => trans('marketplace::app.admin.products.name'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true
        ]);

        $this->addColumn([
            'index' => 'price',
            'label' => trans('marketplace::app.admin.products.price'),
            'type' => 'price',
            'sortable' => true,
            'searchable' => false
        ]);

        $this->addColumn([
            'index' => 'quantity',
            'label' => trans('marketplace::app.admin.products.quantity'),
            'type' => 'number',
            'sortable' => true,
            'searchable' => false,
        ]);

        $this->addColumn([
            'index' => 'is_approved',
            'label' => trans('marketplace::app.admin.products.status'),
            'type' => 'boolean',
            'sortable' => true,
            'searchable' => false,
            'wrapper' => function($row) {
                if ($row->is_approved == 1)
                    return trans('marketplace::app.admin.products.approved');
                else
                    return trans('marketplace::app.admin.products.un-approved');
            }
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'type' => 'Delete',
            'route' => 'admin.marketplace.products.delete',
            'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'product']),
            'icon' => 'icon trash-icon'
        ]);
    }

    public function prepareMassActions()
    {
        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('marketplace::app.admin.products.delete'),
            'action' => route('admin.marketplace.products.massdelete'),
            'method' => 'DELETE'
        ]);

        $this->addMassAction([
            'type' => 'update',
            'label' => trans('marketplace::app.admin.products.update'),
            'action' => route('admin.marketplace.products.massupdate'),
            'method' => 'PUT',
            'options' => [
                trans('marketplace::app.admin.sellers.approve') => 1,
                trans('marketplace::app.admin.sellers.unapprove') => 0
            ]
        ]);
    }
}