<?php

namespace Webkul\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Webkul\Customer\Repositories\CustomerRepository;
use Webkul\Product\Repositories\ProductReviewRepository as ProductReview;
use Webkul\Customer\Models\Customer;
use Auth;
use DB;
use Session;
use Hash;

/**
 * Customer controlller for the customer basically for the tasks of customers which will be done after customer authentication.
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class CustomerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * CustomerRepository object
     *
     * @var array
     */
    protected $customer;

    /**
     * ProductReviewRepository object
     *
     * @var array
     */
    protected $productReview;

    /**
     * Create a new Repository instance.
     *
     * @param  Webkul\Customer\Repositories\CustomerRepository     $customer
     * @param  Webkul\Product\Repositories\ProductReviewRepository $productReview
     * @return void
     */
    public function __construct(
            CustomerRepository $customer,
            ProductReview $productReview
    ) {
        $this->middleware('customer');

        $this->_config = request('_config');

        $this->customer = $customer;

        $this->productReview = $productReview;
    }

    /**
     * Taking the customer to profile details page
     *
     * @return View
     */
    public function index() {        
        if (session('cart_data')) {
            $customer_id = auth()->guard('customer')->user()->id;
            $customer_fname = auth()->guard('customer')->user()->first_name;
            $customer_lname = auth()->guard('customer')->user()->last_name;
            $customer_email = auth()->guard('customer')->user()->email;
            $cart_details = session('cart_data');
            $delete_beforecart = DB::table("cart_detail")->where('customer_id', $customer_id)->delete();
            foreach ($cart_details as $session_cart):
                $save_cart = array(
                    'customer_email' => $customer_email,
                    'customer_first_name' => $customer_fname,
                    'customer_last_name' => $customer_lname,
                    'items_qty' => $session_cart['quantity'],
                    'global_currency_code' => $session_cart['store_currency'],
                    'channel_currency_code' => $session_cart['store_currency'],
                    'cart_currency_code' => $session_cart['store_currency'],
                    'customer_id' => $customer_id,
                    'channel_id' => 1,
                    'prd_id' => $session_cart['product_id'],
                    'prd_type' => $session_cart['product_type'],
                    'prd_shipping_id' => $session_cart['shipping_charge_id'],
                    'prd_shipping_name' => $session_cart['shipping_charged_name'],
                    'customer_address' => $session_cart['customer_ip'],
                    'prd_shipping_charge' => $session_cart['shipping_charge'],
                    'sub_total' => $session_cart['sub_total'],
                    'seller_id' => $session_cart['seller_id'],
                    'prd_sku' => $session_cart['prd_sku'],
                    'grand_total' => $session_cart['grand_total'],
                    'created_at' => $session_cart['created_date'],
                    'updated_at' => $session_cart['created_date']
                );

                if ($session_cart['product_type'] == "configurable") {
                    $variation_array = array(
                        'variation_label' => $session_cart['customer_variationlabel'],
                        'variation_value' => $session_cart['customer_variationvalue']
                    );
                    $save_cart = merge_array($save_cart, $variation_array);
                }
                $insert_session = DB::table('cart_detail')->insert([$save_cart]);
            endforeach;
            if ($insert_session == 1) {
                Session::forget('cart_data');
                return redirect('checkout/onepage');
            }
        }        
        
        $customer = $this->customer->find(auth()->guard('customer')->user()->id);
        return view($this->_config['view'], compact('customer'));
    }

    /**
     * For loading the edit form page.
     *
     * @return View
     */
    public function editIndex() {
        $customer = $this->customer->find(auth()->guard('customer')->user()->id);

        return view($this->_config['view'], compact('customer'));
    }

    /**
     * Edit function for editing customer profile.
     *
     * @return Redirect.
     */
    public function edit() {
        $id = auth()->guard('customer')->user()->id;

        $this->validate(request(), [
            'first_name' => 'string',
            'last_name' => 'string',
            'gender' => 'required',
            'date_of_birth' => 'date|before:today',
            'email' => 'email|unique:customers,email,' . $id,
            'oldpassword' => 'required_with:password',
            'password' => 'confirmed|min:6'
        ]);

        $data = collect(request()->input())->except('_token')->toArray();

        if ($data['date_of_birth'] == "") {
            unset($data['date_of_birth']);
        }

        if ($data['oldpassword'] != "" || $data['oldpassword'] != null) {
            if (Hash::check($data['oldpassword'], auth()->guard('customer')->user()->password)) {
                $data['password'] = bcrypt($data['password']);
            } else {
                session()->flash('warning', trans('shop::app.customer.account.profile.unmatch'));

                return redirect()->back();
            }
        } else {
            unset($data['password']);
        }

        if ($this->customer->update($data, $id)) {
            Session()->flash('success', trans('shop::app.customer.account.profile.edit-success'));

            return redirect()->back();
        } else {
            Session()->flash('success', trans('shop::app.customer.account.profile.edit-fail'));

            return redirect()->back();
        }
    }

    /**
     * Load the view for the customer account panel, showing orders in a table.
     *
     * @return Mixed
     */
    public function orders() {
        return view($this->_config['view']);
    }

    /**
     * Load the view for the customer account panel, showing wishlist items.
     *
     * @return Mixed
     */
    public function wishlist() {
        return view($this->_config['view']);
    }

    /**
     * Load the view for the customer account panel, showing approved reviews.
     *
     * @return Mixed
     */
    public function reviews() {
        $reviews = $this->productReview->getCustomerReview();

        return view($this->_config['view'], compact('reviews'));
    }

    /**
     * Load the view for the customer account panel, shows the customer address.
     *
     * @return Mixed
     */
    public function address() {
        return view($this->_config['view']);
    }

}
