$(document).ready(function () {
    $(".right-content-menu li.dropdown-open.prf").click(function (e) {
        e.stopPropagation();
        $(this).children('span').addClass('active');
        $(this).children('ul').toggle();
    });
    $(".right-navigation-top .cart-dropdown-container.dropdown-open").click(function (e) {
        e.stopPropagation();
        $(this).children('div.dropdown-toggle').addClass('active');
        $(this).children('div.dropdown-list').toggle();
    });
    $('body').click(function () {
        $(".right-content-menu li.dropdown-open.prf span").removeClass('active');
        $(".right-content-menu li.dropdown-open.prf ul").hide();
        $(".right-content-menu li.cart-dropdown-container.dropdown-open div.dropdown-toggle").removeClass("active");
        $(".right-content-menu li.cart-dropdown-container.dropdown-open div.dropdown-list").hide();
    });
});